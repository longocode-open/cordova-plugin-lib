(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de descargar y abrir un pass
*/
function clickDownloadAndOpenPass () { document.addEventListener("deviceready", downloadAndOpenPass, false); }

/*
Click en la opción de desacargar un pass con Passbook
*/
function clickDownloadPassWithPassbook () { document.addEventListener("deviceready", downloadPassWithPassbook, false); }

/*
Click en la opción de comprobar si Passbook esta disponible
*/
function clickCheckIfPassbookAvailable () { document.addEventListener("deviceready", checkIfPassbookAvailable, false); }

/*
Click en la opción de añadir un pass local
*/
function clickAddPassWithPassbook () { document.addEventListener("deviceready", addPassWithPassbook, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de descargar y abrir un pass
*/
function downloadAndOpenPass () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (pass, added) {

    // Se hace stringify a la variable pass por que en iOS vuelve un JSON con información del pass y en Android un string con un OK.
    alert("pass: " + JSON.stringify(pass));

    // En iOS el valor de la variable added es un booleano en iOS y no existe en Android
    alert("added: " + added);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Url en la que se encuentra el pass
  var url = 'https://gestiona3.madrid.org/portalapps/webapps/cordova_plugin_lib/pass/PawPlanet.pkpass';
  //config.headers = {"authorization": "Bearer <token>"}; // --8<-- Plugin Passbook de iOS, esta opción no parece funcionar

  CPLUGIN.pass.downloadPass(url, config);
}

/*
Click en la opción de descargar un pass con Passbook
*/
function downloadPassWithPassbook () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (pass, added) {
    alert(JSON.stringify(pass));
    alert(added);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Url en la que se encuentra el pass
  var url = 'https://gestiona3.madrid.org/portalapps/webapps/cordova_plugin_lib/pass/PawPlanet.pkpass';
  //config.headers = {"authorization": "Bearer <token>"}; // --8<-- Plugin Passbook de iOS, esta opción no parece funcionar

  CPLUGIN.passbook.downloadPass(url, config);
}

/*
Click en la opción de comprobar si Passbook esta disponible
*/
function checkIfPassbookAvailable () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Passbook disponible');
  }

  // Función a ejecutar en caso de error
  function errorCallback () {
    alert('Passbook no disponible');
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  CPLUGIN.passbook.available(config);
}

/*
Click en la opción de añadir un pass local
*/
function addPassWithPassbook () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (pass, added) {
    alert(JSON.stringify(pass));
    alert(added);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.file = 'www/misc/pass/PawPlanet.pkpass';

  CPLUGIN.passbook.addPass(config);
}