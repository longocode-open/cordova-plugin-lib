(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de escanear un código
*/
function clickScan () { document.addEventListener("deviceready", barcodeScannerScan, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de escanear un código
*/
function barcodeScannerScan () {

  // Función a ejecutar cuando se obtiene un código correctamente
  function successCallback (result) {
    alert(
      'Se ha encontrado un código\n' +
      'Resultado: ' + result.text + '\n' +
      'Formato: ' + result.format + '\n' +
      'Cancelado: ' + result.cancelled);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Error: ' + error);
  }

  /*
  Definir la configuración para el plugin
  Todos los formatos posibles y más información en https://github.com/phonegap/phonegap-plugin-barcodescanner
  */
  var config = {};
  // Obligatorio
  config.successCallback = successCallback;
  config.formats = 'QR_CODE, PDF_417, CODE_39, CODE_93'; // por defecto solo QR
  config.errorCallback = errorCallback;
  config.preferFrontCamera = false; // iOS y Android
  config.showFlipCameraButton = true; // iOS y Android
  config.showTorchButton = true; // iOS y Android
  config.torchOn = false; // Android, si puede, es capaz de encender el flash de forma inicial.
  config.saveHistory = false; // Android, puede salvar un historial por defecto desactivado
  config.prompt = 'Encuadre el código en el area.'; // Android
  config.resultDisplayDuration = 1500; // Android, puede mostrar el resultado conseguido en un TOAST, por defecto no lo hace
  config.orientation = 'portrait'; // Android, (portrait|landscape), por defecto el valor es portrait
  config.disableAnimations = true; // iOS
  config.disableSuccessBeep = false; // iOS y Android

  // Escanear código
  CPLUGIN.barcodeScanner.scan(config);
};