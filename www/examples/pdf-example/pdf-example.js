(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de abrir una URL con un PDF
*/
function clickOpenBrowserWithPdfSystem () { document.addEventListener("deviceready", clickOpenBrowserWithPdfSystem, false); }

/*
Click en la opción de abrir una URL con un PDF
*/
function clickOpenBrowserWithPdfDrive () { document.addEventListener("deviceready", clickOpenBrowserWithPdfDrive, false); }

/*
Click en la opción de abrir pdf
*/
function clickOpenPdf () { document.addEventListener("deviceready", pdfViewerOpenPdf, false); }

/*
Click en la opción de abrir pdf con un diálogo de elección
*/
function clickOpenPdfWithDialog () { document.addEventListener("deviceready", pdfViewerOpenPdfWithDialog, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de abrir una URL de un PDF
*/
function clickOpenBrowserWithPdfSystem () {

  // Url que se quiere abrir
  var url = 'https://gestiona3.madrid.org/portalapps/webapps/cordova_plugin_lib/pdf/demo_titans.pdf';

  // Definir la configuración para el plugin
  var config = {};

  config.name = '_system';

  // Abrir la URL en el navegador
  CPLUGIN.inAppBrowser.open(url, config);

}


/*
Click en la opción de abrir una URL de un PDF
*/
function clickOpenBrowserWithPdfDrive () {

  // Url que se quiere abrir
  var url = 'https://gestiona3.madrid.org/portalapps/webapps/cordova_plugin_lib/pdf/demo_titans.pdf';

  // Función a ejecutar cuando empieza la carga de la URL
  function loadStart (event) {
    alert('start: ' + event.url);
  }

  // Función a ejecutar cuando termina la carga de la URL
  function loadStop (event) {
    alert('stop: ' + event.url);
  }

  // Función a ejecutar cuando da error la carga de la URL
  function loadError (event) {
    alert('error: ' + event.message);
  }

  // Función a ejecutar cuando se cierra el navegador
  function exit (event) {
    alert(event.type);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.pdfDrive = true;
  config.loadStart = loadStart;
  config.loadStop = loadStop;
  config.loadError = loadError;
  config.exit = exit;

  // Abrir la URL en el navegador
  CPLUGIN.inAppBrowser.open(url, config);

}

/*
Click en la opción de abrir pdf
*/
function pdfViewerOpenPdf () {
  
  // Url que se quiere abrir
  var url = 'https://gestiona3.madrid.org/portalapps/webapps/cordova_plugin_lib/pdf/demo_titans.pdf';

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert('Abrir PDF finalizado');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Error status: ' + error.status + ' - Error message: ' + error.message);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  CPLUGIN.pdfViewer.openPdf(url, config);
}

/*
Click en la opción de abrir pdf con opción de elegir como abrirlo mediante un diálogo
*/
function pdfViewerOpenPdfWithDialog () {
  
  // Url que se quiere abrir
  var url = 'https://gestiona3.madrid.org/portalapps/webapps/cordova_plugin_lib/pdf/demo_titans.pdf';

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert('Abrir PDF finalizado');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Error status: ' + error.status + ' - Error message: ' + error.message);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.showOpenDialog = true;

  CPLUGIN.pdfViewer.openPdf(url, config)
}