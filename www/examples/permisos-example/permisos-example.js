(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de comprobar el permiso para la cámara
*/
function clickCheckCameraAndContactsPermissions () { document.addEventListener("deviceready", permissionsCheckPermissions, false); }

/*
Click en la opción de solicitar el permiso para la cámara
*/
function clickRequestCameraAndContactsPermisions () { document.addEventListener("deviceready", permissionsRequestPermissions, false); }

/*
Click en la opción de comprobar si la cámara esta presente
*/
function clickCheckIfCameraPresent () { document.addEventListener("deviceready", checkIfCameraPresent, false); }

/*
Click en la opción de comprobar si la cámara esta disponible
*/
function clickCheckIfCameraAvailable () { document.addEventListener("deviceready", checkIfCameraAvailable, false); }

/*
Click en la opción de comprobar si la cámara esta disponible
*/
function clickCheckIfCameraAuthorized () { document.addEventListener("deviceready", checkIfCameraAuthorized, false); }

/*
Click en la opción de comprobar el estado actual de la autorización de la cámara
*/
function clickCheckCameraAuthorizationStatus () { document.addEventListener("deviceready", checkCameraAuthorizationStatus, false); }

/*
Click en la opción de requerir al usuario permiso para la cámara
*/
function clickRequestCameraAuthorization () { document.addEventListener("deviceready", requestCameraAuthorization, false); }

/*
Click en la opción de comprobar si la localizacion es accesible por la aplicación
*/
function clickCheckIfLocationAvailable () { document.addEventListener("deviceready", checkIfLocationAvailable, false); }

/*
Click en la opción de comprobar la configuración de la localización esta activada
*/
function clickCheckIfLocationEnabled () { document.addEventListener("deviceready", checkIfLocationEnabled, false); }

/*
Click en la opción de comprobar si la aplicación esta autorizada para usar la localización
*/
function clickCheckIfLocationAuthorized () { document.addEventListener("deviceready", checkIfLocationAuthorized, false); }

/*
Click en la opción de comprobar el estado actual de la autorización de la localización
*/
function clickCheckLocationAuthorizationStatus () { document.addEventListener("deviceready", checkLocationAuthorizationStatus, false); }

/*
Click en la opción de requerir al usuario permiso para la localización
*/
function clickRequestLocationAuthorization () { document.addEventListener("deviceready", requestLocationAuthorization, false); }

/*
Click en la opción de comprobar si las notificaciones Push están activadas
*/
function clickCheckIfRemoteNotificationsEnabled () { document.addEventListener("deviceready", checkIfRemoteNotificationsEnabled, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de comprobar el permiso para la cámara
*/
function permissionsCheckPermissions () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (statuses) {
    alert(JSON.stringify(statuses));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.CAMERA = true;
  config.READ_CONTACTS = true;
  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar permisos
  CPLUGIN.permissions.getPermissionsAuthorizationStatus(config);
}

/*
Click en la opción de solicitar permiso para la cámara
*/
function permissionsRequestPermissions () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (statuses) {
    alert(JSON.stringify(statuses));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.CAMERA = true;
  config.READ_CONTACTS = true;
  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar permisos
  CPLUGIN.permissions.requestRuntimePermissions(config);
}

/*
Click en la opción de comprobar si la cámara esta presente
*/
function checkIfCameraPresent () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (present) {
    alert(present);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.isCameraPresent(config);
}

/*
Click en la opción de comprobar si la cámara esta disponible
*/
function checkIfCameraAvailable () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (available) {
    alert(available);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.isCameraAvailable(config);
}

/*
Click en la opción de comprobar si la cámara esta autorizada
*/
function checkIfCameraAuthorized () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (authorized) {
    alert(authorized);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.isCameraAuthorized(config);
}

/*
Click en la opción de comprobar el estado actual de la autorización de la cámara
*/
function checkCameraAuthorizationStatus () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (status) {
    alert(status);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.getCameraAuthorizationStatus(config);
}

/*
Click en la opción de requerir al usuario autorización para la cámara
*/
function requestCameraAuthorization () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (status) {
    alert(status);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.requestCameraAuthorization(config); 
}

/*
Click en la opción de comprobar si la localizacion es accesible por la App
*/
function checkIfLocationAvailable () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (available) {
    alert(available);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.isLocationAvailable(config);
}

/*
Click en la opción de comprobar la configuración de la localización esta activada
*/
function checkIfLocationEnabled () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (enabled) {
    alert(enabled);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.isLocationEnabled(config);
}

/*
Click en la opción de comprobar si la aplicación esta autorizada para usar la localización
*/
function checkIfLocationAuthorized () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (authorized) {
    alert(authorized);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.isLocationAuthorized(config);  
}

/*
Click en la opción de comprobar el estado actual de la autorización de la localización
*/
function checkLocationAuthorizationStatus () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (status) {
    alert(status);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.getLocationAuthorizationStatus(config);
}

/*
Click en la opción de requerir al usuario permiso para la localización
*/
function requestLocationAuthorization () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (status) {
    alert(status);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.requestLocationAuthorization(config); 
}

/*
Click en la opción de comprobar si las notificaciones Push están activadas
*/
function checkIfRemoteNotificationsEnabled () {
  
  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (enabled) {
    alert(enabled);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.permissions.isRemoteNotificationsEnabled(config);
}