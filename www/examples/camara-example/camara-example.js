(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción para conseguir una imagen mediante la cámara
*/
function clickCameraGetPicture () { document.addEventListener("deviceready", cameraGetPicture, false); }

/*
Click en la opción para conseguir una imagen mediante la cámara
*/
function clickCameraGetPictureAndEdit () { document.addEventListener("deviceready", cameraGetPictureAndEdit, false); }

/*
Click en la opción para conseguir una imagen mediante la cámara
*/
function clickCameraGetPicturePhotolibrary () { document.addEventListener("deviceready", cameraGetPicturePhotolibrary, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de conseguir una imagen mediante la cámara
*/
function cameraGetPicture () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (imageData) {
    alert(imageData);
    var image = document.getElementById('getPictureExample001');
    image.src = "data:image/jpeg;base64," + imageData;
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.targetWidth = 200;
  config.targetHeight = 200;
  config.jpg = true;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.camera.getPicture(config);
}

/*
Click en la opción de conseguir una imagen mediante la cámara y editarla antes de terminar
*/
function cameraGetPictureAndEdit () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (imageData) {
    alert(imageData);
    var image = document.getElementById('getPictureExample002');
    image.src = "data:image/jpeg;base64," + imageData;
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.targetWidth = 200;
  config.targetHeight = 200;
  config.jpg = true;
  config.allowEdit = true;

  // Abrir la cámara para conseguir una imagen y editarla antes de finalizar
  CPLUGIN.camera.getPicture(config);
}

/*
Click en la opción de conseguir una imagen mediante la galería de imágenes
*/
function cameraGetPicturePhotolibrary () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (imageData) {
    alert(imageData);
    var image = document.getElementById('getPictureExample003');
    image.src = "data:image/jpeg;base64," + imageData;
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.targetWidth = 200;
  config.targetHeight = 200;
  config.jpg = true;
  config.photolibrary = true;

  // // Abrir la cámara para conseguir una imagen de la librería de imágenes
  CPLUGIN.camera.getPicture(config);
}