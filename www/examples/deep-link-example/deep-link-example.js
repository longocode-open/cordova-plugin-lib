(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de configurar el retorno por Deep Link
*/
function clickConfigureDeepLink () { document.addEventListener("deviceready", configureDeepLink, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de configurar el retorno por Deep Link
*/
function configureDeepLink () {

  // Función a ejecutar cuando se viene a la App desde un Deep Link
  function deepLinkCallback (url) {
    alert(JSON.stringify(url));
  }

  CPLUGIN.customUrlScheme.handleOpenURL = deepLinkCallback;
}