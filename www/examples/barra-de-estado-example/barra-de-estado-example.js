(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de cambiar color de la barra de estado
*/
function clickChangeColorStatusBar () { document.addEventListener("deviceready", statusBarChangeColor, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de cambiar el color de la barra de estado
*/
function statusBarChangeColor () {

  // Definir la configuración para el plugin
  var config = {};

  // Mostrar la barra de estado ya que la App esta configurada a pantalla completa
  config.show = true;
  // Necesario para iOS, de esta forma la barra de estado responderá a los cambios de color
  config.overlaysWebView = false;
  // Cambiar el color de la barra de estado
  config.backgroundColorByHexString = '#'+Math.floor(Math.random()*16777215).toString(16);

  // Modificar la barra de estado
  CPLUGIN.statusBar.setConfig(config);
}