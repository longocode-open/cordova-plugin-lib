(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de mostrar una alerta nativa
*/
function clickShowAlert () { document.addEventListener("deviceready", showAlert, false); }

/*
Click en la opción de mostrar un diálogo de confirmación nativo
*/
function clickShowConfirmation () { document.addEventListener("deviceready", showConfirmation, false); }

/*
Click en la opción de mostrar prompt
*/
function clickShowPrompt () { document.addEventListener("deviceready", showPrompt, false); }

/*
Click en la opción de reproducir un sonido
*/
function clickPlayBeep () { document.addEventListener("deviceready", playBeep, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de mostrar una alerta nativa
*/
function showAlert () {

  // Función a ejecutar cuando se ejecuta la alerta
  function callback () {
    alert('Función ejecutada después de mostrar la alerta.');
  }

  // Definir la configuración para el plugin
  var config = {};

  config.callback = callback;
  config.message = 'Este es un ejemplo de alerta.';
  config.title = 'Alerta';
  config.buttonText = 'Entendido';

  CPLUGIN.dialog.alert(config);
}

/*
Click en la opción de mostrar un diálogo de confirmación nativo
*/
function showConfirmation () {

  // Función a ejecutar cuando se ejecuta la confirmación
  function callback (button) {
    alert('Has pulsado el botón ' + button);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.callback = callback;
  config.message = 'Este es un ejemplo de diálogo de confirmación.';
  config.title = 'Confirmación';
  config.buttonOkText = 'Aceptar';
  config.buttonCancelText = 'Cancelar';

  CPLUGIN.dialog.confirm(config);
}

/*
Click en la opción de mostrar prompt
*/
function showPrompt () {

  // Función a ejecutar cuando se ejecuta la confirmación
  function callback (results) {
    alert("Has pulsado el botón " + results.buttonIndex + " y has introducido el texto " + results.input1);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.callback = callback;
  config.message = 'Este es un ejemplo de diálogo de confirmación.';
  config.title = 'Confirmación';
  config.inputText = 'Introduzca un texto';
  config.buttonOkText = 'Aceptar';
  config.buttonCancelText = 'Cancelar';

  CPLUGIN.dialog.prompt(config);
}

/*
Click en la opción de reproducir un sonido
*/
function playBeep () {

  CPLUGIN.dialog.beep(3);
}