(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de conseguir la posición del dispositivo
*/
function clickGetCurrentPosition () { document.addEventListener("deviceready", getCurrentPosition, false); }

/*
Click en la opción de comenzar la escucha de la posición del dispositivo
*/
function clickWatchPosition () { document.addEventListener("deviceready", watchPosition, false); }

/*
Click en la opción de detener la escucha de la posición del dispositivo
*/
function clickClearWatch () { document.addEventListener("deviceready", clearWatch, false); }

/*
Click en la opción de mostrar un mensaje con el estado de la funcionalidad de localización cada vez que cambie
*/
function clickShowAlertOnLocationStateChange () { document.addEventListener("deviceready", showAlertOnLocationStateChange, false); }

/*
Click en la opción de cancelar el mostrar un mensaje con el estado de la funcionalidad de localización cada vez que cambie
*/
function clickCancelShowAlertOnLocationStateChange () { document.addEventListener("deviceready", cancelShowAlertOnLocationStateChange, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de conseguir la posición del dispositivo
*/
function getCurrentPosition () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (position) {
    alert('Latitude: ' + position.coords.latitude + '\n' +
          'Longitude: '+ position.coords.longitude + '\n' +
          'Altitude: '  + position.coords.altitude + '\n' +
          'Accuracy: ' + position.coords.accuracy + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
          'Heading: ' + position.coords.heading + '\n' +
          'Speed: ' + position.coords.speed + '\n' +
          'Timestamp: ' + position.timestamp + '\n');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  CPLUGIN.geolocation.getCurrentPosition(config);
}

/*
Click en la opción de comenzar la escucha de la posición del dispositivo
*/
function watchPosition () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (position) {
    alert('Latitude: ' + position.coords.latitude + '\n' +
          'Longitude: '+ position.coords.longitude + '\n' +
          'Altitude: '  + position.coords.altitude + '\n' +
          'Accuracy: ' + position.coords.accuracy + '\n' +
          'Altitude Accuracy: ' + position.coords.altitudeAccuracy + '\n' +
          'Heading: ' + position.coords.heading + '\n' +
          'Speed: ' + position.coords.speed + '\n' +
          'Timestamp: ' + position.timestamp + '\n');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Función a ejecutar cuando se dentenga la escucha
  function endTimeCallback (error) {
    alert('Escucha detenida de forma automática');
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.endTimeCallback = endTimeCallback;

  config.cacheTime = 5000;
  config.endTime = 30000;

  // Lanzar la escucha
  var watchID = CPLUGIN.geolocation.watchPosition(config);

  // Guardar el identificador de la escucha
  localStorage.setItem('geolocationWatchId', watchID);

  alert(watchID);
}

/*
Click en la opción de detener la escucha de la posición del dispositivo
*/
function clearWatch () {

  var watchID = localStorage.getItem('geolocationWatchId');

  CPLUGIN.geolocation.clearWatch(watchID);
}

/*
Click en la opción de mostrar un mensaje con el estado de la funcionalidad de localización cada vez que cambie
*/
function showAlertOnLocationStateChange () {

  // Función a ejecutar
  function callback (state) {
    var mensaje = 'El estado de la funcionalidad de localización ha cambiado a: ' + state;
    alert(mensaje);
  }

  CPLUGIN.geolocation.doOnLocationStateChange = callback;
}

/*
Click en la opción de cancelar el mostrar un mensaje con el estado de la funcionalidad de localización cada vez que cambie
*/
function cancelShowAlertOnLocationStateChange () {

  CPLUGIN.geolocation.doOnLocationStateChange = undefined;
}