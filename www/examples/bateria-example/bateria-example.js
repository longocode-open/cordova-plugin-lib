(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de mostrar si la batería esta cargandose
*/
function clickShowChargingStatus () { document.addEventListener("deviceready", showChargingStatus, false); }

/*
Click en la opción de mostrar el tiempo necesario para cargar completamente la batería
*/
function clickShowChargingTimeStatus () { document.addEventListener("deviceready", showChargingTimeStatus, false); }

/*
Click en la opción de mostrar el tiempo necesario para descargar completamente la batería
*/
function clickShowDischargingTimeStatus () { document.addEventListener("deviceready", showDischargingTimeStatus, false); }

/*
Click en la opción de mostrar el nivel de la batería
*/
function clickShowLevelStatus () { document.addEventListener("deviceready", showLevelStatus, false); }

/*
Click en la opción de mostrar un mensaje cada vez que el usuario comience a cargarse o a descargarse
*/
function clickShowAlertOnChargingChange () { document.addEventListener("deviceready", showAlertOnChargingChange, false); }

/*
Click en la opción de mostrar un mensaje cada vez que el tiempo de carga cambie
*/
function clickShowAlertOnChargingTimeChange () { document.addEventListener("deviceready", showAlertOnChargingTimeChange, false); }

/*
Click en la opción de mostrar un mensaje cada vez que el tiempo de descarga cambie
*/
function clickShowAlertOnDischargingTimeChange () { document.addEventListener("deviceready", showAlertOnDischargingTimeChange, false); }

/*
Click en la opción de mostrar un mensaje cada vez que el nivel de carga cambie
*/
function clickShowAlertOnLevelChange () { document.addEventListener("deviceready", showAlertOnLevelChange, false); }

/*
Click en la opción de cancelar el mostrar un mensaje cada vez que el usuario comience a cargarse o a descargarse
*/
function clickCancelShowAlertOnChargingChange () { document.addEventListener("deviceready", cancelShowAlertOnChargingChange, false); }

/*
Click en la opción de cancelar el mostrar un mensaje cada vez que el tiempo de carga cambie
*/
function clickCancelShowAlertOnChargingTimeChange () { document.addEventListener("deviceready", cancelShowAlertOnChargingTimeChange, false); }

/*
Click en la opción de cancelar el mostrar un mensaje cada vez que el tiempo de descarga cambie
*/
function clickCancelShowAlertOnDischargingTimeChange () { document.addEventListener("deviceready", cancelShowAlertOnDischargingTimeChange, false); }

/*
Click en la opción de cancelar el mostrar un mensaje cada vez que el nivel de carga cambie
*/
function clickCancelShowAlertOnLevelChange () { document.addEventListener("deviceready", cancelShowAlertOnLevelChange, false); }

/*
Click en la opción de mostrar si el dispositivo está en estado warning
*/
function clickCheckWarningStatus () { document.addEventListener("deviceready", checkWarningStatus, false); }

/*
Click en la opción de mostrar si el dispositivo está en estado critico
*/
function clickCheckCriticalStatus () { document.addEventListener("deviceready", checkCriticalStatus, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de mostrar si la batería esta cargandose
*/
function showChargingStatus () {

  alert(CPLUGIN.battery.charging);
}

/*
Click en la opción de mostrar el tiempo necesario para cargar completamente la batería
*/
function showChargingTimeStatus () {

  alert(CPLUGIN.battery.chargingTime);
}

/*
Click en la opción de mostrar el tiempo necesario para descargar completamente la batería
*/
function showDischargingTimeStatus () {

  alert(CPLUGIN.battery.dischargingTime);
}

/*
Click en la opción de mostrar el nivel de la batería
*/
function showLevelStatus () {

  alert(CPLUGIN.battery.level);
}

/*
Click en la opción de mostrar un mensaje cada vez que el usuario comience a cargarse o a descargarse
*/
function showAlertOnChargingChange () {

  // Función a ejecutar
  function callback () {
    var mensaje = (CPLUGIN.battery.charging) ? 'Comienzo de carga' : 'Comienzo de descarga';
    alert(mensaje);
  }

  CPLUGIN.battery.doOnChargingChange = callback;
}

/*
Click en la opción de mostrar un mensaje cada vez que el tiempo de carga cambie
*/
function showAlertOnChargingTimeChange () {

  // Función a ejecutar
  function callback () {
    var mensaje = 'El tiempo para la carga total ha cambiado a ' + CPLUGIN.battery.chargingTime;
    alert(mensaje);
  }

  CPLUGIN.battery.doOnChargingTimeChange = callback;
}

/*
Click en la opción de mostrar un mensaje cada vez que el tiempo de descarga cambie
*/
function showAlertOnDischargingTimeChange () {

  // Función a ejecutar
  function callback () {
    var mensaje = 'El tiempo para la descarga total ha cambiado a ' + CPLUGIN.battery.dischargingTime;
    alert(mensaje);
  }

  CPLUGIN.battery.doOnDischargingTimeChange = callback;  
}

/*
Click en la opción de mostrar un mensaje cada vez que el nivel de carga cambie
*/
function showAlertOnLevelChange () {

  // Función a ejecutar
  function callback () {
    var mensaje = 'El nivel de carga ha cambiado a ' + CPLUGIN.battery.level;
    alert(mensaje);
  }

  CPLUGIN.battery.doOnLevelChange = callback;    
}

/*
Click en la opción de cancelar el mostrar un mensaje cada vez que el usuario comience a cargarse o a descargarse
*/
function cancelShowAlertOnChargingChange () {

  CPLUGIN.battery.doOnChargingChange = undefined;  
}

/*
Click en la opción de cancelar el mostrar un mensaje cada vez que el tiempo de carga cambie
*/
function cancelShowAlertOnChargingTimeChange () {

  CPLUGIN.battery.doOnChargingTimeChange = undefined;  
}

/*
Click en la opción de cancelar el mostrar un mensaje cada vez que el tiempo de descarga cambie
*/
function cancelShowAlertOnDischargingTimeChange () {

  CPLUGIN.battery.doOnDischargingTimeChange = undefined;  
}

/*
Click en la opción de cancelar el mostrar un mensaje cada vez que el nivel de carga cambie
*/
function cancelShowAlertOnLevelChange () {

  CPLUGIN.battery.doOnLevelChange = undefined;  
}

/*
Click en la opción de mostrar si el dispositivo está en estado warning
*/
function checkWarningStatus () {

  alert(CPLUGIN.battery.isBatteryWarningStatus());
}

/*
Click en la opción de mostrar si el dispositivo está en estado crítico
*/
function checkCriticalStatus () {

  alert(CPLUGIN.battery.isBatteryCriticalStatus());
}
