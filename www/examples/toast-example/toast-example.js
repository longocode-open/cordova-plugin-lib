(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de mostrar un Toast
*/
function clickShowToast () { document.addEventListener("deviceready", showToast, false); }

/*
Click en la opción de mostrar un Toast con estilo personalizado
*/
function clickShowToastWithStyle () { document.addEventListener("deviceready", showToastWithStyle, false); }

/*
Click en la opción de ocultar un Toast
*/
function clickHideToast () { document.addEventListener("deviceready", hideToast, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de mostrar un Toast
*/
function showToast () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (args) {
    alert(JSON.stringify(args));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.message = 'Mensaje de prueba';

  // Mostrar el Toast
  CPLUGIN.toast.show(config);
}

/*
Click en la opción de mostrar un Toast
*/
function showToastWithStyle () {

  // Definir la configuración para el plugin
  var config = {};
  
  config.message = 'Mensaje de prueba';
  config.styling = {};
  config.styling.opacity = 0.75;
  config.styling.backgroundColor = '#FF0000';
  config.styling.textColor = '#FFFF00';
  config.styling.textSize = 20.5;
  config.styling.cornerRadius = 16;
  config.styling.horizontalPadding = 20;
  config.styling.verticalPadding = 16;

  // Mostrar el Toast
  CPLUGIN.toast.show(config);
}

/*
Click en la opción de ocultar un Toast
*/
function hideToast () {

  // Ocultar el Toast activo
  CPLUGIN.toast.hide();
}