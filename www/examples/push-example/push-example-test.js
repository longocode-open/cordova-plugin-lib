var firstLoad = true;

(function() {

  // Ejecutar cuando el DOM sea creado

  document.addEventListener("deviceready", pushInitAndRegisterTest, false);

})()

/*
Click en la opción de iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
*/
function pushInitAndRegisterTest () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function registrationCallback (data) {
    // Mostrar el objeto de respuesta
    //alert(JSON.stringify(data));
    // Mostrar el identificador de dispositivo
    var elem = document.getElementById('getDeviceId');
    elem.innerHTML = data.idDispositivo;
    // Guardar el identificador del dispositivo en la memoria local
    localStorage.setItem('pushDeviceId', data.idDispositivo);
    // Registrar el dispositivo en el topic 001
    pushAddDeviceTopic001();
  }

  // Función a ejecutar cuando el dispositivo se ha registrado en la platforma nativa de Google o Apple
  function registrationNativeCallback (data) {
    // Mostrar el objeto de respuesta
    //alert(JSON.stringify(data))
    // Mostrar el token nativo del dispositivo
    var elem = document.getElementById('getDeviceNativeToken');
    elem.innerHTML = data.registrationId;
    // Guardar el token nativo del dispositivo en la memoria local
    localStorage.setItem('pushNativeToken', data.registrationId);
  }

  function getNotificationCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data))
    // Mostrar la información de la notificación
    var elem = document.getElementById('getNotificationData');
    elem.innerHTML = JSON.stringify(data);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configuración para el plugin
  config.pluginInitConfig = {};
  
  // Configuración para la plataforma Android
  config.pluginInitConfig.android = {};
  config.pluginInitConfig.android.forceShow = true; // Mostrar la notificación si la App esta en foreground

  // Configurar la función a ejecutar al registrar el dispositivo de forma nativa en Google o Apple
  config.registrationNativeCallback = registrationNativeCallback;
  // Configurar la función a ejecutar al registrar el dispositivo con la API de Madrid Digital
  config.registrationCallback = registrationCallback;
  // Configurar la función a ejecutar al recibir una notificación
  config.getNotificationCallback = getNotificationCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.init(config);
}

/*
Click en la opción de crear un topic
*/
function pushAddDeviceTopic001 () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {

    // Mostrar el estado del topic 001
    var elem = document.getElementById('topic_001_status');
    elem.innerHTML = true;
    
    // Al iniciar solo se esta suscrito al topic 1
    if (firstLoad) {
      firstLoad = false;
      pushRemoveDeviceTopic002();
    }
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el identificador de dispositivo
  config.topicName = 'MOVA_MOV_MOVATESTPLUGINS_PRO_TOPIC_001';

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.addDeviceTopic(config);
}

/*
Click en la opción de crear un topic
*/
function pushAddDeviceTopic002 () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {

    // Mostrar el estado del topic 001
    var elem = document.getElementById('topic_002_status');
    elem.innerHTML = true;
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el identificador de dispositivo
  config.topicName = 'MOVA_MOV_MOVATESTPLUGINS_PRO_TOPIC_002';

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.addDeviceTopic(config);
}

/*
Click en la opción de eliminar la subscripción entre un dispositivo y un topic
*/
function pushRemoveDeviceTopic002 () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {

    // Mostrar el estado del topic 001
    var elem = document.getElementById('topic_002_status');
    elem.innerHTML = false;
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el identificador de dispositivo
  config.topicName = 'MOVA_MOV_MOVATESTPLUGINS_PRO_TOPIC_002';

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.removeDeviceTopic(config);
}

/*
Click en el checkbox de suscribir topic 002
*/
function checkboxTopic002 () {

  if (document.getElementById('topic_002_suscribe').checked) 
  {
    pushAddDeviceTopic002();
  } else {
    pushRemoveDeviceTopic002();
  }
}