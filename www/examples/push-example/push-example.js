(function() {

  // Ejecutar cuando el DOM sea creado

  // Configuración de la API de Madrid Digital
  CPLUGIN.pushMD.application = 'MOVA_MOV_MOVATESTPLUGINS';
  CPLUGIN.pushMD.client = 'MOVA_TEST_CLIENT';
  CPLUGIN.pushMD.desarrolloPassword = '63D4C088809BED822C45124586507E4F';
  CPLUGIN.pushMD.validacionPassword = '63D4C088809BED822C45124586507E4F';
  CPLUGIN.pushMD.produccionPassword = '63D4C088809BED822C45124586507E4F';
  CPLUGIN.pushMD.version = 'v4';

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
*/
function clickPushInitAndRegister () { document.addEventListener("deviceready", pushInitAndRegister, false); }

/*
Click en la opción de actualizar el token nativo de un dispositivo
*/
function clickPushSetDevice () { document.addEventListener("deviceready", pushSetDevice, false); }

/*
Click en la opción de recuperar la información de un dispositivo
*/
function clickPushGetDevice () { document.addEventListener("deviceready", pushGetDevice, false); }

/*
Click en la opción de recuperar los mensajes entregados a un dispositivo
*/
function clickPushGetDeviceDeliveries () { document.addEventListener("deviceready", pushGetDeviceDeliveries, false); }

/*
Click en la opción de enviar un push a un dispositivo
*/
function clickPushSendDevicePush () { document.addEventListener("deviceready", pushSendDevicePush, false); }

/*
Click en la opción de conseguir el identificador del dispositivo para la API de Madrid Digital mediante el token nativo de la plataforma
*/
function clickPushGetDeviceId () { document.addEventListener("deviceready", pushGetDeviceId, false); }

/*
Click en la opción de asociar un usuario a un dispositivo
*/
function clickPushAddDeviceUsers () { document.addEventListener("deviceready", pushAddDeviceUsers, false); }

/*
Click en la opción de recuperar los usuarios asociados a un dispositivo
*/
function clickPushGetDeviceUsers () { document.addEventListener("deviceready", pushGetDeviceUsers, false); }

/*
Click en la opción de eliminar un usuario a un dispositivo
*/
function clickPushRemoveDeviceUser () { document.addEventListener("deviceready", pushRemoveDeviceUser, false); }

/*
Click en la opción de eliminar todos los usuarios de un dispositivo
*/
function clickPushRemoveAllDeviceUsers () { document.addEventListener("deviceready", pushRemoveAllDeviceUsers, false); }

/*
Click en la opción de enviar un push a un usuario
*/
function clickPushSendDeviceUserPush () { document.addEventListener("deviceready", pushSendDeviceUserPush, false); }

/*
Click en la opción de crear un topic
*/
function clickPushAddDeviceTopic () { document.addEventListener("deviceready", pushAddDeviceTopic, false); }

/*
Click en la opción de recuperar los topics de un dispositivo
*/
function clickPushGetDeviceAllTopics () { document.addEventListener("deviceready", pushGetDeviceAllTopics, false); }

/*
Click en la opción de recuperar el estado de la subscripción entre un dispositivo y un topic
*/
function clickPushGetDeviceTopic () { document.addEventListener("deviceready", pushGetDeviceTopic, false); }

/*
Click en la opción de eliminar la subscripción entre un dispositivo y un topic
*/
function clickPushRemoveDeviceTopic () { document.addEventListener("deviceready", pushRemoveDeviceTopic, false); }

/*
Click en la opción de enviar un push a un topic
*/
function clickPushSendDeviceTopicPush () { document.addEventListener("deviceready", pushSendDeviceTopicPush, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
*/
function pushInitAndRegister () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function registrationCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
    // Mostrar el identificador de dispositivo
    var elem = document.getElementById('getDeviceId');
    elem.innerHTML = data.idDispositivo;
    // Guardar el identificador del dispositivo en la memoria local
    localStorage.setItem('pushDeviceId', data.idDispositivo);
  }

  // Función a ejecutar cuando el dispositivo se ha registrado en la platforma nativa de Google o Apple
  function registrationNativeCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data))
    // Mostrar el token nativo del dispositivo
    var elem = document.getElementById('getDeviceNativeToken');
    elem.innerHTML = data.registrationId;
    // Guardar el token nativo del dispositivo en la memoria local
    localStorage.setItem('pushNativeToken', data.registrationId);
  }

  function getNotificationCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data))
    // Mostrar la información de la notificación
    var elem = document.getElementById('getNotificationData');
    elem.innerHTML = JSON.stringify(data);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configuración para el plugin
  config.pluginInitConfig = {};
  
  // Configuración para la plataforma Android
  config.pluginInitConfig.android = {};
  config.pluginInitConfig.android.forceShow = true; // Mostrar la notificación si la App esta en foreground

  // Configurar la función a ejecutar al registrar el dispositivo de forma nativa en Google o Apple
  config.registrationNativeCallback = registrationNativeCallback;
  // Configurar la función a ejecutar al registrar el dispositivo con la API de Madrid Digital
  config.registrationCallback = registrationCallback;
  // Configurar la función a ejecutar al recibir una notificación
  config.getNotificationCallback = getNotificationCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.init(config);
}

/*
Click en la opción de actualizar el token nativo de un dispositivo
*/
function pushSetDevice () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el nuevo token nativo para el dispositivo
  config.deviceNativeToken = localStorage.getItem('pushNativeToken');
  // Modifica el valor de itNotifica para el dispositivo
  config.deviceAllowNotifications = 0;

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.setDevice(config);
}

/*
Click en la opción de recuperar la información de un dispositivo
*/
function pushGetDevice () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.getDevice(config);
}

/*
Click en la opción de recuperar los mensajes entregados a un dispositivo
*/
function pushGetDeviceDeliveries () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Límite de las últimas 10 notificaciones entregadas
  config.limit = 10;

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.getDeviceDeliveries(config);
}

/*
Click en la opción de enviar un push a un dispositivo
*/
function pushSendDevicePush () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el mensaje
  config.message = 'Mensaje de prueba para el dispositivo ' + config.deviceId;
  // Configurar el título
  config.title = 'Prueba para el dispositivo ' + config.deviceId;
  // Configurar la información adicional
  config.info = "{\\\"data\\\":\\\"some_data\\\"}";

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.sendDevicePush(config);
}

/*
Click en la opción de conseguir el identificador del dispositivo para la API de Madrid Digital mediante el token nativo de la plataforma
*/
function pushGetDeviceId () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el token nativo del dispositivo
  config.deviceNativeToken = localStorage.getItem('pushNativeToken');
  // Plataforma del dispositivo
  config.platform = device.platform;

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.getDeviceId(config);
}

/*
Click en la opción de asociar un usuario a un dispositivo
*/
function pushAddDeviceUsers () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el identificador de usuario
  config.userId = 'NOMBRE_USUARIO';

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.addDeviceUser(config);
}

/*
Click en la opción de recuperar los usuarios asociados a un dispositivo
*/
function pushGetDeviceUsers () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.getDeviceUsers(config);
}

/*
Click en la opción de eliminar un usuario a un dispositivo
*/
function pushRemoveDeviceUser () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el identificador de usuario
  config.userId = 'NOMBRE_USUARIO';

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.removeDeviceUser(config);
}

/*
Click en la opción de eliminar todos los usuarios de un dispositivo
*/
function pushRemoveAllDeviceUsers () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.removeDeviceAllUsers(config);
}

/*
Click en la opción de enviar un push a un dispositivo
*/
function pushSendDeviceUserPush () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de usuario
  config.userId = 'NOMBRE_USUARIO';
  // Configurar el mensaje
  config.message = 'Mensaje de prueba para el usuario ' + config.userId;
  // Configurar el título
  config.title = 'Prueba para el dispositivo ' + config.deviceId;
  // Configurar la información adicional
  config.info = "{\\\"data\\\":\\\"some_data\\\"}";

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.sendDeviceUserPush(config);
}

/*
Click en la opción de crear un topic
*/
function pushAddDeviceTopic () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el identificador de dispositivo
  config.topicName = 'MOVA_MOV_MOVATESTPLUGINS_PRO_TOPIC_002';

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.addDeviceTopic(config);
}

/*
Click en la opción de recuperar el estado de la subscripción entre un dispositivo y un topic
*/
function pushGetDeviceTopic () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el identificador de dispositivo
  config.topicName = 'MOVA_MOV_MOVATESTPLUGINS_PRO_TOPIC_002';

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.getDeviceTopic(config);
}

/*
Click en la opción de recuperar los topics de un dispositivo
*/
function pushGetDeviceAllTopics () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.getDeviceAllTopics(config);
}

/*
Click en la opción de eliminar la subscripción entre un dispositivo y un topic
*/
function pushRemoveDeviceTopic () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.deviceId = localStorage.getItem('pushDeviceId');
  // Configurar el identificador de dispositivo
  config.topicName = 'MOVA_MOV_MOVATESTPLUGINS_PRO_TOPIC_002';

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.removeDeviceTopic(config);
}

/*
Click en la opción de enviar un push a un topic
*/
function pushSendDeviceTopicPush () {

  // Función a ejecutar cuando el dispositivo se ha registrado mediante la API de Madrid Digital
  function successCallback (data) {
    // Mostrar el objeto de respuesta
    alert(JSON.stringify(data));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  // Configurar la función a ejecutar en caso correcto
  config.successCallback = successCallback;
  // Configurar la función a ejecutar en caso de error
  config.errorCallback = errorCallback;
  // Configurar el identificador de dispositivo
  config.topicName = 'MOVA_MOV_MOVATESTPLUGINS_PRO_TOPIC_002';
  // Configurar el mensaje
  config.message = 'Mensaje de prueba para el topic ' + config.topicName;
  // Configurar el título
  config.title = 'Prueba para el dispositivo ' + config.deviceId;
  // Configurar la información adicional
  config.info = "{\\\"data\\\":\\\"some_data\\\"}";

  // Elegir el entorno contra el que trabajar
  config.environment = CPLUGIN.pushMD.PRODUCCION; // Servicio contra producción

  // Iniciar el plugin de forma nativa y registrar el dispositivo mediante la API de Madrid Digital
  CPLUGIN.pushMD.sendDeviceTopicPush(config);
}