(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de abrir una URL
*/
function clickOpenBrowser () { document.addEventListener("deviceready", inAppBrowserOpen, false); }

/*
Click en la opción de abrir una URL en el navegador del sistema
*/
function clickOpenBrowserSystem () { document.addEventListener("deviceready", inAppBrowserOpenSystem, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de abrir una URL
*/
function inAppBrowserOpen () {

  // Url que se quiere abrir
  var url = 'https://www.google.com/';

  // Función a ejecutar cuando empieza la carga de la URL
  function loadStart (event) {
    alert('start: ' + event.url);
  }

  // Función a ejecutar cuando termina la carga de la URL
  function loadStop (event) {
    alert('stop: ' + event.url);
  }

  // Función a ejecutar cuando da error la carga de la URL
  function loadError (event) {
    alert('error: ' + event.message);
  }

  // Función a ejecutar cuando se cierra el navegador
  function exit (event) {
    alert(event.type);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.target = '_blank'; // Por defecto es '', más información https://www.w3schools.com/jsref/met_win_open.asp
  config.params = 'location=YES'; // Por defecto es 'location=YES', incluir más separados por coma sin espacios, más información https://www.w3schools.com/jsref/met_win_open.asp
  config.loadStart = loadStart;
  config.loadStop = loadStop;
  config.loadError = loadError;
  config.exit = exit;

  // Abrir la URL en el navegador
  CPLUGIN.inAppBrowser.open(url, config);
};

/*
Click en la opción de abrir una URL en el navegador del sistema
*/
function inAppBrowserOpenSystem () {

  // Url que se quiere abrir
  var url = 'https://www.google.com/';

  // Definir la configuración para el plugin
  var config = {};

  config.target = '_system';

  // Abrir la URL en el navegador
  CPLUGIN.inAppBrowser.open(url, config);

}