(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de mostrar una instancia de la cámara
*/
function clickCameraPreviewStart () { document.addEventListener("deviceready", cameraPreviewStart, false); }

/*
Click en la opción de detener la instancia de la cámara
*/
function clickCameraPreviewStop () { document.addEventListener("deviceready", cameraPreviewStop, false); }

/*
Click en la opción de cambiar la cámara de frontal a trasera y viceversa
*/
function clickCameraPreviewSwitchCamera () { document.addEventListener("deviceready", cameraPreviewSwitchCamera, false); }
/*
Click en la opción de ocultar la cámara
*/
function clickCameraPreviewHide () { document.addEventListener("deviceready", cameraPreviewHide, false); }
/*
Click en la opción de mostrar la cámara
*/
function clickCameraPreviewShow () { document.addEventListener("deviceready", cameraPreviewShow, false); }
/*
Click en la opción de capturar una imagen
*/
function clickCameraPreviewTakePicture () { document.addEventListener("deviceready", cameraPreviewTakePicture, false); }
/*
Click en la opción de capturar una instantánea
*/
function clickCameraPreviewTakeSnapshot () { document.addEventListener("deviceready", cameraPreviewTakeSnapshot, false); }
/*
Click en la opción de conseguir los modos de foco soportados por la cámara
*/
function clickCameraPreviewGetSupportedFocusModes () { document.addEventListener("deviceready", cameraPreviewGetSupportedFocusModes, false); }
/*
Click en la opción de aplicar un modo de foco
*/
function clickCameraPreviewSetFocusMode () { document.addEventListener("deviceready", cameraPreviewSetFocusMode, false); }
/*
Click en la opción de conseguir el modo de foco actual
*/
function clickCameraPreviewGetFocusMode () { document.addEventListener("deviceready", cameraPreviewGetFocusMode, false); }
/*
Click en la opción de conseguir los modos de flash soportados por la cámara
*/
function clickCameraPreviewGetSupportedFlashModes () { document.addEventListener("deviceready", cameraPreviewGetSupportedFlashModes, false); }
/*
Click en la opción de aplicar un modo de flash
*/
function clickCameraPreviewSetFlashMode () { document.addEventListener("deviceready", cameraPreviewSetFlashMode, false); }
/*
Click en la opción de conseguir el modo de flash actual
*/
function clickCameraPreviewGetFlashMode () { document.addEventListener("deviceready", cameraPreviewGetFlashMode, false); }
/*
Click en la opción de conseguir el ancho del FOV
*/
function clickCameraPreviewGetHorizontalFOV () { document.addEventListener("deviceready", cameraPreviewGetHorizontalFOV, false); }
/*
Click en la opción de conseguir los efectos de colores soportados
*/
function clickCameraPreviewGetSupportedColorEffects () { document.addEventListener("deviceready", cameraPreviewGetSupportedColorEffects, false); }
/*
Click en la opción de aplicar un efecto de color
*/
function clickCameraPreviewSetColorEffect () { document.addEventListener("deviceready", cameraPreviewSetColorEffect, false); }
/*
Click en la opción de aplicar un zoom
*/
function clickCameraPreviewSetZoom () { document.addEventListener("deviceready", cameraPreviewSetZoom, false); }
/*
Click en la opción de recuperar el nivel máximo de zoom
*/
function clickCameraPreviewGetMaxZoom () { document.addEventListener("deviceready", cameraPreviewGetMaxZoom, false); }
/*
Click en la opción de recuperar los modos de balanceo del blanco disponibles
*/
function clickCameraPreviewGetSupportedWhiteBalanceModes () { document.addEventListener("deviceready", cameraPreviewGetSupportedWhiteBalanceModes, false); }
/*
Click en la opción de recuperar el modo de balanceo de blanco actual
*/
function clickCameraPreviewGetWhiteBalanceMode () { document.addEventListener("deviceready", cameraPreviewGetWhiteBalanceMode, false); }
/*
Click en la opción de aplicar un modo de balanceo de blanco
*/
function clickCameraPreviewSetWhiteBalanceMode () { document.addEventListener("deviceready", cameraPreviewSetWhiteBalanceMode, false); }
/*
Click en la opción de recuperar los modos de exposición de la cámara
*/
function clickCameraPreviewGetExposureModes () { document.addEventListener("deviceready", cameraPreviewGetExposureModes, false); }
/*
Click en la opción de recuperar el modo de exposición actual
*/
function clickCameraPreviewGetExposureMode () { document.addEventListener("deviceready", cameraPreviewGetExposureMode, false); }
/*
Click en la opción de aplicar un modo de exposición
*/
function clickCameraPreviewSetExposureMode () { document.addEventListener("deviceready", cameraPreviewSetExposureMode, false); }
/*
Click en la opción de recuperar el rango de la compensación de la exposición actual
*/
function clickCameraPreviewGetExposureCompensationRange () { document.addEventListener("deviceready", cameraPreviewGetExposureCompensationRange, false); }
/*
Click en la opción de recuperar la compensación de la exposición actual
*/
function clickCameraPreviewGetExposureCompensation () { document.addEventListener("deviceready", cameraPreviewGetExposureCompensation, false); }
/*
Click en la opción de aplicar la compensación de la exposición
*/
function clickCameraPreviewSetExposureCompensation () { document.addEventListener("deviceready", cameraPreviewSetExposureCompensation, false); }
/*
Click en la opción de cambiar las domensiones de la ventana de previsualización
*/
function clickCameraPreviewSetPreviewSize () { document.addEventListener("deviceready", cameraPreviewSetPreviewSize, false); }
/*
Click en la opción de recuperar las dimensiones soportadas de la imagen
*/
function clickCameraPreviewGetSupportedPictureSizes () { document.addEventListener("deviceready", cameraPreviewGetSupportedPictureSizes, false); }
/*
Click en la opción de recuperar las características de la cámara
*/
function clickCameraPreviewGetCameraCharacteristics () { document.addEventListener("deviceready", cameraPreviewGetCameraCharacteristics, false); }
/*
Click en la opción de definir un punto de foco específico
*/
function clickCameraPreviewTapToFocus () { document.addEventListener("deviceready", cameraPreviewTapToFocus, false); }
/*
Click en la opción de definir el evento al presionar el botón de atrás
*/
function clickCameraPreviewOnBackButton () { document.addEventListener("deviceready", cameraPreviewOnBackButton, false); }
/*
Click en la opción de conseguir un Blob a partir de una ruta file://
*/
function clickCameraPreviewGetBlob () { document.addEventListener("deviceready", cameraPreviewGetBlob, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de mostrar una instancia de la cámara
*/
function cameraPreviewStart () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert("Cámara iniciada correctamente");
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.width = 200;
  config.height = 200;

  // Iniciar la instancia de la cámara
  CPLUGIN.cameraPreview.startCamera(config);
}

/*
Click en la opción de detener la instancia de la cámara
*/
function cameraPreviewStop () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert("Cámara detenida correctamente");
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Detener la instancia de la cámara
  CPLUGIN.cameraPreview.stopCamera(config);
}

/*
Click en la opción de cambiar la cámara de frontal a trasera y viceversa
*/
function cameraPreviewSwitchCamera () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert("Cámara cambiada correctamente");
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Cambiar la cámara
  CPLUGIN.cameraPreview.switchCamera(config);
}

/*
Click en la opción de ocultar la cámara
*/
function cameraPreviewHide () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert("Cámara ocultada correctamente");
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Ocultar la cámara
  CPLUGIN.cameraPreview.hide(config);
}

/*
Click en la opción de mostrar la cámara
*/
function cameraPreviewShow () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert("Cámara mostrada correctamente");
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Mostrar la cámara
  CPLUGIN.cameraPreview.show(config);
}

/*
Click en la opción de capturar una imagen
*/
function cameraPreviewTakePicture () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (base64OrFilePath) {
    alert(base64OrFilePath);
    var image = document.getElementById('getPictureExample001');
    image.src = "data:image/jpeg;base64," + base64OrFilePath;    
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Capturar una imagen de la cámara
  CPLUGIN.cameraPreview.takePicture(config);
}

/*
Click en la opción de capturar una instantánea
*/
function cameraPreviewTakeSnapshot () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (base64OrFilePath) {
    alert(base64OrFilePath);
    var image = document.getElementById('getPictureExample002');
    image.src = "data:image/jpeg;base64," + base64OrFilePath;    
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Capturar una instantánea de la cámara
  CPLUGIN.cameraPreview.takeSnapshot(config);
}

/*
Click en la opción de conseguir los modos de foco soportados por la cámara
*/
function cameraPreviewGetSupportedFocusModes () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (focusModes) {
    alert(focusModes);    
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Conseguir los modos de foco soportados
  CPLUGIN.cameraPreview.getSupportedFocusModes(config);
}

/*
Click en la opción de aplicar un modo de foco
*/
function cameraPreviewSetFocusMode () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Modo de foco aplicado correctamente');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.focusMode = CameraPreview.FOCUS_MODE.FIXED;

  // Aplicar un modo de foco
  CPLUGIN.cameraPreview.setFocusMode(config);
}

/*
Click en la opción de conseguir el modo de foco actual
*/
function cameraPreviewGetFocusMode () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (currentFocusMode) {
    alert(currentFocusMode);    
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Conseguir el modos de foco actual
  CPLUGIN.cameraPreview.getFocusMode(config);
}

/*
Click en la opción de conseguir los modos de flash soportados por la cámara
*/
function cameraPreviewGetSupportedFlashModes () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (flashModes) {
    alert(flashModes);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Conseguir el modos de foco actual
  CPLUGIN.cameraPreview.getSupportedFlashModes(config);
}

/*
Click en la opción de aplicar un modo de flash
*/
function cameraPreviewSetFlashMode () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Modo de flash aplicado correctamente');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.flashMode = CameraPreview.FLASH_MODE.OFF;

  // Aplicar un modo de flash
  CPLUGIN.cameraPreview.setFlashMode(config);
}

/*
Click en la opción de conseguir el modo de flash actual
*/
function cameraPreviewGetFlashMode () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (currentFlashMode) {
    alert(currentFlashMode);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Conseguir el modo de flash actual
  CPLUGIN.cameraPreview.getFlashMode(config);
}

/*
Click en la opción de conseguir el ancho del FOV
*/
function cameraPreviewGetHorizontalFOV () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (horizontalFOV) {
    alert(horizontalFOV);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Conseguir el ancho del FOV
  CPLUGIN.cameraPreview.getHorizontalFOV(config);
}

/*
Click en la opción de conseguir los efectos de colores soportados
*/
function cameraPreviewGetSupportedColorEffects () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (colorEffects) {
    alert(colorEffects);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Conseguir los efectos de colores soportados
  CPLUGIN.cameraPreview.getSupportedColorEffects(config);
}

/*
Click en la opción de aplicar un efecto de color
*/
function cameraPreviewSetColorEffect () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Efecto de color aplicado correctamente');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.colorEffect = CameraPreview.COLOR_EFFECT.MONO;

  // Aplicar efecto de color
  CPLUGIN.cameraPreview.setColorEffect(config);
}

/*
Click en la opción de aplicar un nivel de zoom
*/
function cameraPreviewSetZoom () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Nivel de zoom aplicado correctamente');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.zoom = 20;

  // Aplicar nivel de zoom
  CPLUGIN.cameraPreview.setZoom(config);
}

/*
Click en la opción de recuperar el nivel máximo de zoom
*/
function cameraPreviewGetMaxZoom () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (maxZoom) {
    alert(maxZoom);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar nivel máximo de zoom
  CPLUGIN.cameraPreview.getMaxZoom(config);
}

/*
Click en la opción de recuperar los modos de balanceo del blanco disponibles
*/
function cameraPreviewGetSupportedWhiteBalanceModes () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (whiteBalanceModes) {
    alert(whiteBalanceModes);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar los modos de balanceo del blanco disponibles
  CPLUGIN.cameraPreview.getSupportedWhiteBalanceModes(config);
}

/*
Click en la opción de recuperar el modo de balanceo de blanco actual
*/
function cameraPreviewGetWhiteBalanceMode () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (whiteBalanceMode) {
    alert(whiteBalanceMode);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar el modo de balanceo del blanco actual
  CPLUGIN.cameraPreview.getWhiteBalanceMode(config);
}

/*
Click en la opción de aplicar un modo de balanceo de blanco
*/
function cameraPreviewSetWhiteBalanceMode () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Modo de balanceo de blanco aplicado correctamente');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.whiteBalanceMode = CameraPreview.WHITE_BALANCE_MODE.CLOUDY_DAYLIGHT;

  // Aplicar un modo de balanceo del blanco
  CPLUGIN.cameraPreview.setWhiteBalanceMode(config);
}

/*
Click en la opción de recuperar los modos de exposición de la cámara
*/
function cameraPreviewGetExposureModes () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (exposureMode) {
    alert(exposureMode);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar los modos de exposición soportados
  CPLUGIN.cameraPreview.getExposureModes(config);
}

/*
Click en la opción de recuperar el modo de exposición actual
*/
function cameraPreviewGetExposureMode () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (exposureMode) {
    alert(exposureMode);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar el modo de exposición actual
  CPLUGIN.cameraPreview.getExposureMode(config);
}

/*
Click en la opción de aplicar un modo de exposición
*/
function cameraPreviewSetExposureMode () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Modo de exposición aplicado correctamente');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.exposureMode = CameraPreview.EXPOSURE_MODE.CONTINUOUS;

  // Aplicar un modo de exposición
  CPLUGIN.cameraPreview.setExposureMode(config);
}

/*
Click en la opción de recuperar el rango de la compensación de la exposición actual de la cámara
*/
function cameraPreviewGetExposureCompensationRange () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (expoxureRange) {
    alert(JSON.stringify(expoxureRange));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar el rango de la compensación de la exposición
  CPLUGIN.cameraPreview.getExposureCompensationRange(config);
}

/*
Click en la opción de recuperar la compensación de la exposición actual de la cámara
*/
function cameraPreviewGetExposureCompensation () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (expoxureCompensation) {
    alert(JSON.stringify(expoxureCompensation));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar la compensación de la exposición
  CPLUGIN.cameraPreview.getExposureCompensation(config);
}

/*
Click en la opción de aplicar la compensación de la exposición
*/
function cameraPreviewSetExposureCompensation () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (expoxureCompensation) {
    alert(expoxureCompensation);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.exposureCompensation = 3;

  // Aplicar una compensación de la exposición
  CPLUGIN.cameraPreview.setExposureCompensation(config);
}

/*
Click en la opción de cambiar las domensiones de la ventana de previsualización
*/
function cameraPreviewSetPreviewSize () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert("Dimensiones de la ventana de previsualización cambiadas correctamente.");
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.width = 320;
  config.height = 240;

  // Aplicar las dimensiones de la ventana de previsualización
  CPLUGIN.cameraPreview.setPreviewSize(config);
}

/*
Click en la opción de recuperar las dimensiones soportadas de la imagen
*/
function cameraPreviewGetSupportedPictureSizes () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (dimensions) {
    alert(JSON.stringify(dimensions));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar las dimensiones soportadas
  CPLUGIN.cameraPreview.getSupportedPictureSizes(config);
}

/*
Click en la opción de recuperar las características de la cámara
*/
function cameraPreviewGetCameraCharacteristics () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (characteristics) {
    alert(JSON.stringify(characteristics));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar las dimensiones soportadas
  CPLUGIN.cameraPreview.getCameraCharacteristics(config);
}

/*
Click en la opción de definir un punto de foco específico
*/
function cameraPreviewTapToFocus () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Punto de foco definido');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.x = 100
  config.y = 100

  // Recuperar las dimensiones soportadas
  CPLUGIN.cameraPreview.tapToFocus(config);
}

/*
Click en la opción de definir el evento al presionar el botón de atrás
*/
function cameraPreviewOnBackButton () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback () {
    alert('Evento del botón atrás funcionando correctamente');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Recuperar las dimensiones soportadas
  CPLUGIN.cameraPreview.onBackButton(config);
}

/*
Click en la opción de conseguir un Blob a partir de una ruta file://
*/
function cameraPreviewGetBlob () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (image) {
    alert(JSON.stringify(image));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.url = 'file://';

  // Recuperar las dimensiones soportadas
  CPLUGIN.cameraPreview.getBlob(config);
}