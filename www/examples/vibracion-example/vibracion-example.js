(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de hacer vibrar el dispositivo 2 segundos
*/
function clickVibrateTwoSencods () { document.addEventListener("deviceready", vibrateTwoSencods, false); }

/*
Click en la opción de hacer vibrar el dispositivo siguiendo un patrón
*/
function clickVibratePattern () { document.addEventListener("deviceready", vibratePattern, false); }

/*
Click en la opción de hacer vibrar el dispositivo un tiempo por defecto largo
*/
function clickVibrateLong () { document.addEventListener("deviceready", vibrateLong, false); }

/*
Click en la opción de hacer vibrar el dispositivo un tiempo por defecto corto
*/
function clickVibrateShort () { document.addEventListener("deviceready", vibrateShort, false); }

/*
Click en la opción de hacer vibrar el dispositivo en modo alerta
*/
function clickVibrateAlert () { document.addEventListener("deviceready", vibrateAlert, false); }

/*
Click en la opción de hacer vibrar el dispositivo en modo alarma
*/
function clickVibrateAlarm () { document.addEventListener("deviceready", vibrateAlarm, false); }

/*
Click en la opción de detener la vibración en curso
*/
function clickVibrateStop () { document.addEventListener("deviceready", vibrateStop, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de hacer vibrar el dispositivo 2 segundos
*/
function vibrateTwoSencods () {

  CPLUGIN.vibration.vibrate(2000);
}

/*
Click en la opción de hacer vibrar el dispositivo siguiendo un patrón
*/
function vibratePattern () {

  CPLUGIN.vibration.vibrate([500,50,1000,50,1500,1000,200]);
}

/*
Click en la opción de hacer vibrar el dispositivo un tiempo por defecto largo
*/
function vibrateLong () {

  CPLUGIN.vibration.vibrateLong();
}

/*
Click en la opción de hacer vibrar el dispositivo un tiempo por defecto corto
*/
function vibrateShort () {

  CPLUGIN.vibration.vibrateShort();
}

/*
Click en la opción de hacer vibrar el dispositivo en modo alerta
*/
function vibrateAlert () {

  CPLUGIN.vibration.vibrateAlert();
}

/*
Click en la opción de hacer vibrar el dispositivo en modo alarma
*/
function vibrateAlarm () {

  CPLUGIN.vibration.vibrateAlarm();
}

/*
Click en la opción de detener la vibración en curso
*/
function vibrateStop () {

  CPLUGIN.vibration.vibrateStop();
}
