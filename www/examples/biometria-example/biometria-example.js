(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción para mostrar un diálogo de autenticación biométrica
*/
function clickBiometricsShow () { document.addEventListener("deviceready", biometricsShow, false); }

/*
Click en la opción de mostrar los sistemas de autenticación disponibles para el plugin AIO
*/
function clickBiometricsAIOIsAvailable () { document.addEventListener("deviceready", biometricsAIOIsAvailable, false); }

/*
Click en la opción de mostrar un diálogo de autenticación biométrica
*/
function clickBiometricsAIOShow () { document.addEventListener("deviceready", biometricsAIOShow, false); }

/*
Click en la opción de mostrar los sistemas de autenticación disponibles para el plugin de android
*/
function clickBiometricsAndroidIsAvailable () { document.addEventListener("deviceready", biometricsAndroidIsAvailable, false); }

/*
Click en la opción de mostrar un diálogo de autenticación biométrica por encrypt
*/
function clickBiometricsAndroidEncrypt () { document.addEventListener("deviceready", biometricsAndroidEncrypt, false); }

/*
Click en la opción de mostrar un diálogo de autenticación biométrica por decrypt
*/
function clickBiometricsAndroidDecrypt () { document.addEventListener("deviceready", biometricsAndroidDecrypt, false); }

/*
Click en la opción de eliminar 'cipher'
*/
function clickBiometricsAndroidDelete () { document.addEventListener("deviceready", biometricsAndroidDelete, false); }

/*
Click en la opción de rechazar un nuevo diálogo de autenticación
*/
function clickBiometricsAndroidDismiss () { document.addEventListener("deviceready", biometricsAndroidDismiss, false); }

/*
Click en la opción de mostrar los sistemas de autenticación disponibles para el plugin de iOS
*/
function clickBiometricsIosIsAvailable () { document.addEventListener("deviceready", biometricsIosIsAvailable, false); }

/*
Click en la opción de mostrar un diálogo de autenticación biométrica
*/
function clickBiometricsIosVerifyFingerprint () { document.addEventListener("deviceready", biometricsIosVerifyFingerprint, false); }

/*
Click en la opción de mostrar un diálogo de autenticación biométrica y PIN en caso de error
*/
function clickBiometricsIosVerifyFingerprintWithCustomPasswordFallback () { document.addEventListener("deviceready", biometricsIosVerifyFingerprintWithCustomPasswordFallback, false); }

/*
Click en la opción de mostrar un diálogo de autenticación biométrica y PIN en caso de error con un texto customizado
*/
function clickBiometricsIosVerifyFingerprintWithCustomPasswordFallbackAndEnterPasswordLabel () { document.addEventListener("deviceready", biometricsIosVerifyFingerprintWithCustomPasswordFallbackAndEnterPasswordLabel, false); }

/*
Click en la opción de comprobar si la BBDD de firmas ha cambiado
*/
function clickBiometricsIosDidFingerprintDatabaseChange () { document.addEventListener("deviceready", biometricsIosDidFingerprintDatabaseChange, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción para mostrar un diálogo de autenticación biométrica
*/
function biometricsShow () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.show(config);
}

/*
Click en la opción de mostrar los sistemas de autenticación disponibles para el plugin AIO
*/
function biometricsAIOIsAvailable () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(result);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometricsAIO.isAvailable(config);
}

/*
Click en la opción de mostrar un diálogo de autenticación biométrica
*/
function biometricsAIOShow () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert('Acceso permitido');
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Acceso denegado, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometricsAIO.show(config);
}

/*
Click en la opción de mostrar los sistemas de autenticación disponibles para el plugin de android
*/
function biometricsAndroidIsAvailable () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Acceso denegado, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.android.isAvailable(config);
}

/*
Click en la opción de mostrar un diálogo de autenticación biométrica por encrypt
*/
function biometricsAndroidEncrypt () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));

    localStorage.setItem('biometricsAndroidEncryptToken', result.token);
    localStorage.setItem('biometricsAndroidEncryptClientId', config.clientId);
    localStorage.setItem('biometricsAndroidEncryptUsername', config.username);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Acceso denegado, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.clientId = 'myAppName';
  config.username = 'currentUser';
  config.password = 'currentUserPassword';

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.android.encrypt(config);  
}

/*
Click en la opción de mostrar un diálogo de autenticación biométrica por decrypt
*/
function biometricsAndroidDecrypt () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Acceso denegado, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.clientId = localStorage.getItem('biometricsAndroidEncryptClientId');
  config.userName = localStorage.getItem('biometricsAndroidEncryptUsername');
  config.token = localStorage.getItem('biometricsAndroidEncryptToken');

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.android.decrypt(config);  
}

/*
Click en la opción de eliminar 'cipher'
*/
function  biometricsAndroidDelete () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('No se ha podido eliminar el \'cipher\', detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  config.clientId = localStorage.getItem('biometricsAndroidEncryptClientId');
  config.username = localStorage.getItem('biometricsAndroidEncryptUsername');

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.android.delete(config);
}

/*
Click en la opción de rechazar un nuevo diálogo de autenticación
*/
function biometricsAndroidDismiss () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('No se ha podido rechazar el nuevo diálogo de autenticaciuón, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.android.dismiss(config);
}

/*
Click en la opción de mostrr los sistemas de autenticación disponibles para el plugin de iOS
*/
function biometricsIosIsAvailable () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.ios.isAvailable(config);
}

/*
Click en la opción de mostrar un diálogo de autenticación biométrica
*/
function  biometricsIosVerifyFingerprint () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Acceso denegado, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.ios.verifyFingerprint(config);
}

/*
Click en la opción de mostrar un diálogo de autenticación biométrica y PIN en caso de error
*/
function biometricsIosVerifyFingerprintWithCustomPasswordFallback () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Acceso denegado, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.ios.verifyFingerprintWithCustomPasswordFallback(config);
}

/*
Click en la opción de mostrar un diálogo de autenticación biométrica y PIN en caso de error con un texto customizado
*/
function biometricsIosVerifyFingerprintWithCustomPasswordFallbackAndEnterPasswordLabel () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(JSON.stringify(result));
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Acceso denegado, detalle del error en la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.ios.verifyFingerprintWithCustomPasswordFallbackAndEnterPasswordLabel(config);
}

/*
Click en la opción de comprobar si la BBDD de firmas ha cambiado
*/
function biometricsIosDidFingerprintDatabaseChange () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function callback (result) {
    alert(JSON.stringify(result));
  }

  // Definir la configuración para el plugin
  var config = {};

  config.callback = callback;

  // Abrir la cámara para conseguir una imagen
  CPLUGIN.biometrics.ios.didFingerprintDatabaseChange(config);  
}