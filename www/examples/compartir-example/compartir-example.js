(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de compartir
*/
function clickShare () { document.addEventListener("deviceready", socialSharingShare, false); }

/*
Click en la opción de compartir por email
*/
function clickShareViaEmail () { document.addEventListener("deviceready", socialSharingShareViaEmail, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de compartir
*/
function socialSharingShare () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(
      'Compartir finalizado\n' +
      'Completado: ' + result.completed + '\n' +
      'App: ' + result.app);
  }

  // Función a ejecutar en caso de error
  function errorCallback (msg) {
    alert('Error: ' + msg);
  }

  // Definir la configuración para el plugin
  var config = {};
  // Información a compartir en este ejemplo
  config.message = 'Mensaje de ejemplo';
  config.subject = 'Asunto de ejemplo';
  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Compartir
  CPLUGIN.socialSharing.share(config);
}

/*
Click en la opción de compartir por email
*/
function socialSharingShareViaEmail () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (result) {
    alert(
      'Compartir finalizado\n' +
      'Completado: ' + result.completed + '\n' +
      'App: ' + result.app);
  }

  // Función a ejecutar en caso de error
  function errorCallback (msg) {
    alert('Error: ' + msg);
  }

  // Definir la configuración para el plugin
  var config = {};
  // Compartir de forma predeterminada como email
  config.shareWith = 'email';
  // Información a compartir en este ejemplo
  config.message = 'Mensaje de ejemplo';
  config.subject = 'Asunto de ejemplo';
  config.successCallback = successCallback;
  config.errorCallback = errorCallback;
  // Datos específicos para compartir por email
  config.emailTo = ['ejemplo.email@gemail.com'];
  config.emailCC = ['ejemplo.cc.email@jotmail.com', 'ejemplo002.cc.email@jotmail.com', 'ejemplo003.cc.email@jotmail.com'];
  config.emailBCC = ['ejemplo.bcc.email@terra.es', 'ejemplo002.bcc.email@terra.es'];

  // Compartir
  CPLUGIN.socialSharing.share(config);

}