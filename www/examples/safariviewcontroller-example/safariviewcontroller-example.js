(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de abrir una URL
*/
function clickSafariViewControllerOpen () { document.addEventListener("deviceready", safariViewControllerOpen, false); }

/*
Click en la opción de abrir una URL en el navegador del sistema
*/
function clickSafariViewControllerClose () { document.addEventListener("deviceready", safariViewControllerClose, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de abrir una URL
*/
function safariViewControllerOpen () {

  // Url que se quiere abrir
  var url = 'https://www.google.com/';

  // Función a ejecutar cuando se abre de la URL
  function openedCallback (result) {
    console.log(result);
  }

  // Función a ejecutar cuando termina la carga de la URL
  function loadedCallback (result) {
    console.log(result);
  }

  // Función a ejecutar cuando se cierra el navegador
  function closedCallback (result) {
    console.log(result);
  }

  // Función a ejecutar cuando da error la carga de la URL
  function errorCallback (msg) {
    console.log(msg);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.openedCallback = openedCallback;
  config.loadedCallback = loadedCallback;
  config.closedCallback = closedCallback;
  config.errorCallback = errorCallback;

  // Abrir la URL en el navegador
  CPLUGIN.safariViewController.open(url, config);
};

/*
Click en la opción de cerrar SafariViewController
*/
function safariViewControllerClose () {

  // Cerrar SafariViewController
  CPLUGIN.safariViewController.close();

}