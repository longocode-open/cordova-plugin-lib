(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de obtener el tipo de conexión actual
*/
function clickGetConnectionType () { document.addEventListener("deviceready", getConnectionType, false); }

/*
Click en la opción de incluir una función a ejecutar cuando el dispositivo consigue una conexión a la red
*/
function clickAddOnlineFunction () { document.addEventListener("deviceready", addOnlineFunction, false); }

/*
Click en la opción de eliminar una función a ejecutar cuando el dispositivo consigue una conexión a la red
*/
function clickRemoveOnlineFunction () { document.addEventListener("deviceready", removeOnlineFunction, false); }

/*
Click en la opción de incluir una función a ejecutar cuando el dispositivo pierde la conexión a la red
*/
function clickAddOfflineFunction () { document.addEventListener("deviceready", addOfflineFunction, false); }
/*
Click en la opción de eliminar una función a ejecutar cuando el dispositivo pierde la conexión a la red
*/
function clickRemoveOfflineFunction () { document.addEventListener("deviceready", removeOfflineFunction, false); }

/*
Click en la opción de comprobar si el wifi esta disponible
*/
function clickCheckIsWifiAvailable () { document.addEventListener("deviceready", checkIsWifiAvailable, false); }

/*
Click en la opción de comprobar la configuración del wifi esta activada
*/
function clickCheckIsWifiEnabled () { document.addEventListener("deviceready", checkIfWifiEnabled, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de incluir una función a ejecutar cuando el dispositivo consigue una conexión a la red
*/
function getConnectionType () {

  alert(CPLUGIN.connection.getType());
}

/*
Click en la opción de incluir una función a ejecutar cuando el dispositivo consigue una conexión a la red
*/
function addOnlineFunction () {

  // Función a ejecutar cuando se consigue la conexión a la red
  var whenOnlineFunction = function () {
    alert('Dispositivo conectado');
  }

  // Asignar la función
  CPLUGIN.connection.doWhenOnline = whenOnlineFunction;
}

/*
Click en la opción de eliminar una función a ejecutar cuando el dispositivo consigue una conexión a la red
*/
function removeOnlineFunction () {

  // Asignar el valor undefined
  CPLUGIN.connection.doWhenOnline = undefined;
}

/*
Click en la opción de incluir una función a ejecutar cuando el dispositivo pierde la conexión a la red
*/
function addOfflineFunction () {

  // Función a ejecutar cuando se pierde la conexión con la red
  var whenOfflineFunction = function () {
    alert('Dispositivo desconectado');
  }

  // Asignar la función
  CPLUGIN.connection.doWhenOffline = whenOfflineFunction;
}

/*
Click en la opción de eliminar una función a ejecutar cuando el dispositivo pierde la conexión a la red
*/
function removeOfflineFunction () {

  // Asignar el valor undefined
  CPLUGIN.connection.doWhenOffline = undefined;
}

/*
Click en la opción de comprobar si el wifi esta disponible
*/
function checkIsWifiAvailable () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (available) {
    alert(available);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.connection.isWifiAvailable(config);
}

/*
Click en la opción de comprobar la configuración del wifi esta activada
*/
function checkIfWifiEnabled () {

  // Función a ejecutar cuando se obtiene un resultado correcto
  function successCallback (enabled) {
    alert(enabled);
  }

  // Función a ejecutar en caso de error
  function errorCallback (error) {
    alert('Ha ocurrido un error, consulte la consola');
    console.log(error);
  }

  // Definir la configuración para el plugin
  var config = {};

  config.successCallback = successCallback;
  config.errorCallback = errorCallback;

  // Comprobar si la cámara esta disponible
  CPLUGIN.connection.isWifiEnabled(config);
}
