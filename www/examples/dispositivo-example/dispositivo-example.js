(function() {

  // Ejecutar cuando el DOM sea creado

})();

/****************************
 * Funciones del formulario *
 ****************************/

/*
Click en la opción de mostrar la versión de Cordova
*/
function clickGetCordovaVersion () { document.addEventListener("deviceready", getCordovaVersion, false); }

/*
Click en la opción de mostrar el modelo o producto del dispositivo
*/
function clickGetModel () { document.addEventListener("deviceready", getModel, false); }

/*
Click en la opción de mostrar la plataforma (sistema operativo) del dispositivo
*/
function clickGetPlatform () { document.addEventListener("deviceready", getPlatform, false); }

/*
Click en la opción de mostrar el identificador único de dispositivo (uuid) del dispositivo
*/
function clickGetUuid () { document.addEventListener("deviceready", getUuid, false); }

/*
Click en la opción de mostrar la versión de la plataforma del dispositivo
*/
function clickGetVersion () { document.addEventListener("deviceready", getVersion, false); }

/*
Click en la opción de mostrar el fabricante del dispositivo
*/
function clickGetManufacturer () { document.addEventListener("deviceready", getManufacturer, false); }

/*
Click en la opción de mostrar si estamos en un emulador
*/
function clickGetIsVirtual () { document.addEventListener("deviceready", getIsVirtual, false); }

/*
Click en la opción de mostrar el número de serie del dispositivo
*/
function clickGetSerial () { document.addEventListener("deviceready", getSerial, false); }

/***********************************************************************
 * Funciones a ejecutar cuando el dispositivo esta listo (deviceready) *
 ***********************************************************************/

/*
Click en la opción de mostrar la versión de Cordova
*/
function getCordovaVersion () {

  // Mostrar el valor
  alert(CPLUGIN.device.cordova);
}

/*
Click en la opción de mostrar el modelo o producto del dispositivo
*/
function getModel () {

  // Mostrar el valor
  alert(CPLUGIN.device.model);
}

/*
Click en la opción de mostrar la plataforma (sistema operativo) del dispositivo
*/
function getPlatform () {

  // Mostrar el valor
  alert(CPLUGIN.device.platform);
}

/*
Click en la opción de mostrar el identificador único de dispositivo (uuid) del dispositivo
*/
function getUuid () {

  // Mostrar el valor
  alert(CPLUGIN.device.uuid);
}

/*
Click en la opción de mostrar la versión de la plataforma del dispositivo
*/
function getVersion () {

  // Mostrar el valor
  alert(CPLUGIN.device.version);
}

/*
Click en la opción de mostrar el fabricante del dispositivo
*/
function getManufacturer () {

  // Mostrar el valor
  alert(CPLUGIN.device.manufacturer);
}

/*
Click en la opción de mostrar si estamos en un emulador
*/
function getIsVirtual () {

  // Mostrar el valor
  alert(CPLUGIN.device.isVirtual);
}

/*
Click en la opción de mostrar el número de serie del dispositivo
*/
function getSerial () {

  // Mostrar el valor
  alert(CPLUGIN.device.serial);
}
