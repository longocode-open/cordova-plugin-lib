var CPLUGIN = {};

/*******************
 CORE de la librería
 *******************/

CPLUGIN.core = {};

// Textos de la librería
CPLUGIN.core.text = {};
CPLUGIN.core.text.apiBatteryStatusError = 'No existe la Battery Status API.';
CPLUGIN.core.text.apiVibrationError = 'No existe la Vibration API.';
CPLUGIN.core.text.libraryStatusError = 'Errores con cordova-plugin-lib:';
CPLUGIN.core.text.cordovaError = 'No existe Cordova.';
CPLUGIN.core.text.cordovaPluginBiometricsAIOError = 'No existe el plugin cordova-plugin-fingerprint-aio.';
CPLUGIN.core.text.cordovaPluginBiometricsAndroidError = 'No existe el plugin cordova-plugin-android-fingerprint-auth.';
CPLUGIN.core.text.cordovaPluginBiometricsIosError = 'No existe el plugin cordova-plugin-touch-id.';
CPLUGIN.core.text.cordovaPluginCameraError = 'No existe el plugin cordova-plugin-camera.';
CPLUGIN.core.text.cordovaPluginCameraPreviewError = 'No existe el plugin cordova-plugin-camera-preview.';
CPLUGIN.core.text.cordovaPluginDiagnosticError = 'No existe el plugin cordova.plugins.diagnostic.';
CPLUGIN.core.text.cordovaPluginDialogsError = 'No existe el plugin cordova-plugin-dialogs.';
CPLUGIN.core.text.cordovaPluginFileError = 'No existe el plugin cordova-plugin-file.';
CPLUGIN.core.text.cordovaPluginFileOpener2Error = 'No existe el plugin cordova-plugin-file-opener2.';
CPLUGIN.core.text.cordovaPluginGeolocationError = 'No existe el plugin cordova-plugin-geolocation.';
CPLUGIN.core.text.cordovaPluginNetworkInformationError = 'No existe el plugin cordova-plugin-network-information.';
CPLUGIN.core.text.cordovaPluginPassbookError = 'No existe el plugin cordova-plugin-passbook.';
CPLUGIN.core.text.cordovaPluginSafariViewControllerError = 'No existe el plugin cordova-plugin-safariviewcontroller.';
CPLUGIN.core.text.cordovaPluginScreenOrientationError = 'No existe el plugin cordova-plugin-statusbar.';
CPLUGIN.core.text.cordovaPluginStatusbarError = 'No existe el plugin cordova-plugin-statusbar.';
CPLUGIN.core.text.cordovaPluginXSocialsharingError = 'No existe el plugin cordova-plugin-x-socialsharing.';
CPLUGIN.core.text.cordovaPluginXToastError = 'No existe el plugin cordova-plugin-x-toast.';
CPLUGIN.core.text.phonegapPluginBarcodescannerError = 'No existe el plugin phonegap-plugin-barcodescanner.';
CPLUGIN.core.text.phonegapPluginPushError = 'No existe el plugin phonegap-plugin-push.';

// Variables generales para consultar la disponibilidad de cordova, los plugins y las APIs
CPLUGIN.core.hasApiBatteryStatus = false;
CPLUGIN.core.hasApiVibration = false;
CPLUGIN.core.hasCordova = !!window.cordova;
CPLUGIN.core.hasCordovaPluginBiometricsAIO = false;
CPLUGIN.core.hasCordovaPluginBiometricsAndroid = false;
CPLUGIN.core.hasCordovaPluginBiometricsIos = false;
CPLUGIN.core.hasCordovaPluginCamera = false;
CPLUGIN.core.hasCordovaPluginCameraPreview = false;
CPLUGIN.core.hasCordovaPluginDiagnostic = false;
CPLUGIN.core.hasCordovaPluginDialogs = false;
CPLUGIN.core.hasCordovaPluginFile = false;
CPLUGIN.core.hasCordovaPluginFileOpener2 = false;
CPLUGIN.core.hasCordovaPluginGeolocation = false;
CPLUGIN.core.hasCordovaPluginNetworkInformation = false;
CPLUGIN.core.hasCordovaPluginPassbook = false;
CPLUGIN.core.hasCordovaPluginSafariViewController = false;
CPLUGIN.core.hasCordovaPluginScreenOrientation = false;
CPLUGIN.core.hasCordovaPluginStatusbar = false;
CPLUGIN.core.hasCordovaPluginXSocialsharing = false;
CPLUGIN.core.hasCordovaPluginXToast = false;
CPLUGIN.core.hasPhonegapPluginBarcodescanner = false;
CPLUGIN.core.hasPhonegapPluginPush = false;
CPLUGIN.core.hasStatusError = false;

// Centralizar la comprobación de la disponibilidad de un plugin
CPLUGIN.core._checkPlugin = function (jsonProperty) {

	var hasPlugin = !!CPLUGIN.core[jsonProperty];

	if (!hasPlugin) {

		var errorMessage = '';

		if (jsonProperty.localeCompare('hasApiBatteryStatus') == 0) errorMessage = CPLUGIN.core.text.apiBatteryStatusError;		
		if (jsonProperty.localeCompare('hasApiVibration') == 0) errorMessage = CPLUGIN.core.text.apiVibrationError;	
		if (jsonProperty.localeCompare('hasCordovaPluginBiometricsAIO') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginBiometricsAIOError;	
		if (jsonProperty.localeCompare('hasCordovaPluginBiometricsAndroid') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginBiometricsAndroidError;	
		if (jsonProperty.localeCompare('hasCordovaPluginBiometricsIos') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginBiometricsIosError;	
		if (jsonProperty.localeCompare('hasCordovaPluginCamera') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginBiometricsError;
		if (jsonProperty.localeCompare('hasCordovaPluginCameraPreview') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginCameraPreviewError;
		if (jsonProperty.localeCompare('hasCordovaPluginDiagnostic') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginDiagnosticError;
		if (jsonProperty.localeCompare('hasCordovaPluginDialogs') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginDialogsError;
		if (jsonProperty.localeCompare('hasCordovaPluginFile') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginFileError;
		if (jsonProperty.localeCompare('hasCordovaPluginFileOpener2') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginFileOpener2Error;
		if (jsonProperty.localeCompare('hasCordovaPluginGeolocation') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginGeolocationError;
		if (jsonProperty.localeCompare('hasCordovaPluginNetworkInformation') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginNetworkInformationError;
		if (jsonProperty.localeCompare('hasCordovaPluginPassbook') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginPassbookError;
		if (jsonProperty.localeCompare('hasCordovaPluginSafariViewController') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginSafariViewControllerError;
		if (jsonProperty.localeCompare('hasCordovaPluginScreenOrientation') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginScreenOrientationError;
		if (jsonProperty.localeCompare('hasCordovaPluginStatusbar') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginStatusbarError;
		if (jsonProperty.localeCompare('hasCordovaPluginXSocialsharing') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginXSocialsharingError;
		if (jsonProperty.localeCompare('hasCordovaPluginXToast') == 0) errorMessage = CPLUGIN.core.text.cordovaPluginXToastError;
		if (jsonProperty.localeCompare('hasPhonegapPluginBarcodescanner') == 0) errorMessage = CPLUGIN.core.text.phonegapPluginBarcodescannerError;
		if (jsonProperty.localeCompare('hasPhonegapPluginPush') == 0) errorMessage = CPLUGIN.core.text.phonegapPluginPushError;

		// Mostrar mensaje de error
		console.error(errorMessage);

	}

	return hasPlugin
}

// Devuelve un objeto con la información del estado de la librería
CPLUGIN.core._checkLibraryStatus = function () {

	var oReturn = {};
	oReturn.status = true;
	oReturn.errorMessage = '';

	// Comprobar si existe Cordova
	if (!CPLUGIN.core.hasCordova) {
		oReturn.status = false;
		oReturn.errorMessage += CPLUGIN.core.text.libraryStatusError + '\n - ' + CPLUGIN.core.text.cordovaError;
	}

	// Comprobar los plugins
	if (oReturn.status) {
  		document.addEventListener("deviceready", function() {

  			/*
  			 * navigator.
  			 */

			// cordova-plugin-camera
			oReturn.hasCordovaPluginCamera = !!navigator.camera;
			CPLUGIN.core.hasCordovaPluginCamera = oReturn.hasCordovaPluginCamera;

			// cordova-plugin-dialogs
			oReturn.hasCordovaPluginDialogs = !!navigator.notification;
			CPLUGIN.core.hasCordovaPluginDialogs = oReturn.hasCordovaPluginDialogs;

			// cordova-plugin-geolocation
			oReturn.hasCordovaPluginGeolocation = !!navigator.geolocation;
			CPLUGIN.core.hasCordovaPluginGeolocation = oReturn.hasCordovaPluginGeolocation;
			
			// cordova-plugin-network-information
			oReturn.hasCordovaPluginNetworkInformation = !!navigator.connection;
			CPLUGIN.core.hasCordovaPluginNetworkInformation = oReturn.hasCordovaPluginNetworkInformation;

			// API Battery Status 
			oReturn.hasApiBatteryStatus = !!navigator.getBattery
			CPLUGIN.core.hasApiBatteryStatus = oReturn.hasApiBatteryStatus;

			// API Vibration
			oReturn.hasApiVibration = !!navigator.vibrate
			CPLUGIN.core.hasApiVibration = oReturn.hasApiVibration;

			/*
			 * cordova.
			 */

  			// cordova-plugin-file
			oReturn.hasCordovaPluginFile = !!cordova.file;
			CPLUGIN.core.hasCordovaPluginFile = oReturn.hasCordovaPluginFile;

			/*
			 * cordova.plugins.
			 */

			if (typeof cordova.plugins !== 'undefined') {
				// cordova-plugin-diagnostic
				oReturn.hasCordovaPluginDiagnostic = !!cordova.plugins.diagnostic;
				CPLUGIN.core.hasCordovaPluginDiagnostic = oReturn.hasCordovaPluginDiagnostic;

				// cordova-plugin-fileOpener2
				oReturn.hasCordovaPluginFileOpener2 = !!cordova.plugins.fileOpener2;
				CPLUGIN.core.hasCordovaPluginFileOpener2 = oReturn.hasCordovaPluginFileOpener2;

				// phonegap-plugin-barcodescanner
				oReturn.hasPhonegapPluginBarcodescanner = !!cordova.plugins.barcodeScanner;
				CPLUGIN.core.hasPhonegapPluginBarcodescanner = oReturn.hasPhonegapPluginBarcodescanner;
			}

			/*
			 * window.plugins.
			 */

			if (typeof window.plugins !== 'undefined') {
				// cordova-plugin-x-socialsharing
				oReturn.hasCordovaPluginXSocialsharing = !!window.plugins.socialsharing;
				CPLUGIN.core.hasCordovaPluginXSocialsharing = oReturn.hasCordovaPluginXSocialsharing;	

				// cordova-plugin-x-toast
				oReturn.hasCordovaPluginXToast = !!window.plugins.toast;
				CPLUGIN.core.hasCordovaPluginXToast = oReturn.hasCordovaPluginXToast;


				// cordova-plugin-touch-id
				oReturn.hasCordovaPluginBiometricsIos = !!window.plugins.touchid;
				CPLUGIN.core.hasCordovaPluginBiometricsIos = oReturn.hasCordovaPluginBiometricsIos;	
			}

			/*
			 * window.screen.
			 */
			if (typeof window.screen !== 'undefined') {
				// cordova-plugin-screen-orientation
				oReturn.hasCordovaPluginScreenOrientation = !!window.screen.orientation;
				CPLUGIN.core.hasCordovaPluginScreenOrientation = oReturn.hasCordovaPluginScreenOrientation;	
			}

			/*
			 * CameraPreview.
			 */
			if (typeof CameraPreview !== 'undefined') {
				oReturn.hasCordovaPluginCameraPreview = !!CameraPreview;
				CPLUGIN.core.hasCordovaPluginCameraPreview = oReturn.hasCordovaPluginCameraPreview;
			}

			/*
			 * Fingerprint.
			 */
			if (typeof Fingerprint !== 'undefined') {
				oReturn.hasCordovaPluginBiometricsAIO = !!Fingerprint;
				CPLUGIN.core.hasCordovaPluginBiometricsAIO = oReturn.hasCordovaPluginBiometricsAIO;
			}

			/*
			 * FingerprintAuth.
			 */
			if (typeof FingerprintAuth !== 'undefined') {
				oReturn.hasCordovaPluginBiometricsAndroid = !!FingerprintAuth;
				CPLUGIN.core.hasCordovaPluginBiometricsAndroid = oReturn.hasCordovaPluginBiometricsAndroid;
			}

			/*
			 * Passbook.
			 */

			// cordova-plugin-passbook
			if (typeof Passbook !== 'undefined') {
				oReturn.hasCordovaPluginPassbook = !!Passbook;
				CPLUGIN.core.hasCordovaPluginPassbook = oReturn.hasCordovaPluginPassbook;
			}

			/*
			 * PushNotification.
			 */

			// phonegap-plugin-push
			if (typeof PushNotification !== 'undefined') {
				oReturn.hasPhonegapPluginPush = !!PushNotification;
				CPLUGIN.core.hasPhonegapPluginPush = oReturn.hasPhonegapPluginPush;
			}

			/*
			 * SafariViewController.
			 */

			// cordova-plugin-safariviewcontroller
			if (typeof SafariViewController !== 'undefined') {
				oReturn.hasCordovaPluginSafariViewController = !!SafariViewController;
				CPLUGIN.core.hasCordovaPluginSafariViewController = oReturn.hasCordovaPluginSafariViewController;
			}

			/*
			 * StatusBar.
			 */

			// cordova-plugin-statusbar
			if (typeof StatusBar !== 'undefined') {
				oReturn.hasCordovaPluginStatusbar = !!StatusBar;
				CPLUGIN.core.hasCordovaPluginStatusbar = oReturn.hasCordovaPluginStatusbar;
			}

  		}, false);
	}

	// Mostrar mensaje de error
	if (!oReturn.status) {
		console.warn(oReturn.errorMessage);
	}

	// Modificar la variable de estado de la librería
	CPLUGIN.core.hasStatusError = oReturn.status;

	// Devolver el objeto final
	return oReturn;
}
CPLUGIN.core._checkLibraryStatus(); // Comprobar estado de la librería

/*******************************************************************************
 PLUGIN: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-file

 Android / iOS
 *******************************************************************************/

CPLUGIN.file = {};

// Conseguir el array de directorios introducidos
CPLUGIN.file._generateFileStoreDirectories = function (oConfig) {
	
	var directories = [];

	/*
	Valores por defecto
	--8<-- El orden importa ya que si hay varios solo se usa el primero según el orden de esta lista
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	if (typeof oConfig.applicationDirectory !== 'undefined') directories.push(cordova.file.applicationDirectory);
	if (typeof oConfig.applicationStorageDirectory !== 'undefined') directories.push(cordova.file.applicationStorageDirectory);
	if (typeof oConfig.dataDirectory !== 'undefined') directories.push(cordova.file.dataDirectory);
	if (typeof oConfig.cacheDirectory !== 'undefined') directories.push(cordova.file.cacheDirectory);
	if (typeof oConfig.externalApplicationStorageDirectory !== 'undefined') directories.push(cordova.file.externalApplicationStorageDirectory);
	if (typeof oConfig.externalDataDirectory !== 'undefined') directories.push(cordova.file.externalDataDirectory);
	if (typeof oConfig.externalCacheDirectory !== 'undefined') directories.push(cordova.file.externalCacheDirectory);
	if (typeof oConfig.externalRootDirectory !== 'undefined') directories.push(cordova.file.externalRootDirectory);
	if (typeof oConfig.tempDirectory !== 'undefined') directories.push(cordova.file.tempDirectory);
	if (typeof oConfig.syncedDataDirectory !== 'undefined') directories.push(cordova.file.syncedDataDirectory);
	if (typeof oConfig.documentsDirectory !== 'undefined') directories.push(cordova.file.documentsDirectory);
	if (typeof oConfig.sharedDirectory !== 'undefined') directories.push(cordova.file.sharedDirectory);

	return directories;
}

// Conseguir el array de directorios de destino introducidos
CPLUGIN.file._generateFileStoreDestinyDirectories = function (oConfig) {
	
	var directories = [];

	/*
	Valores por defecto
	--8<-- El orden importa ya que si hay varios solo se usa el primero según el orden de esta lista
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	if (typeof oConfig.destinyApplicationDirectory !== 'undefined') directories.push(cordova.file.applicationDirectory);
	if (typeof oConfig.destinyApplicationStorageDirectory !== 'undefined') directories.push(cordova.file.applicationStorageDirectory);
	if (typeof oConfig.destinyDataDirectory !== 'undefined') directories.push(cordova.file.dataDirectory);
	if (typeof oConfig.destinyCacheDirectory !== 'undefined') directories.push(cordova.file.cacheDirectory);
	if (typeof oConfig.destinyExternalApplicationStorageDirectory !== 'undefined') directories.push(cordova.file.externalApplicationStorageDirectory);
	if (typeof oConfig.destinyExternalDataDirectory !== 'undefined') directories.push(cordova.file.externalDataDirectory);
	if (typeof oConfig.destinyExternalCacheDirectory !== 'undefined') directories.push(cordova.file.externalCacheDirectory);
	if (typeof oConfig.destinyExternalRootDirectory !== 'undefined') directories.push(cordova.file.externalRootDirectory);
	if (typeof oConfig.destinyTempDirectory !== 'undefined') directories.push(cordova.file.tempDirectory);
	if (typeof oConfig.destinySyncedDataDirectory !== 'undefined') directories.push(cordova.file.syncedDataDirectory);
	if (typeof oConfig.destinyDocumentsDirectory !== 'undefined') directories.push(cordova.file.documentsDirectory);
	if (typeof oConfig.destinySharedDirectory !== 'undefined') directories.push(cordova.file.sharedDirectory);

	return directories;
}

// Transformar un BASE64 a BLOB
CPLUGIN.file.b64toBlob = function (b64Data, contentType, sliceSize) {

  contentType = contentType || '';
  sliceSize = sliceSize || 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

// Generar un id Random
function getRandomId(length) {

	length = (!length) ? 10 : length;

	var result           = '';
	var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for ( var i = 0; i < length; i++ ) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

/*
Función para conseguir o crear el fileEntry y poder realizar sobre él acciones de fichero.
*/
CPLUGIN.file.getFile = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

	// Comprobar estado de la librería
	if (!CPLUGIN.core._checkLibraryStatus()) return false;

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos el nombre del fichero no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.fileName == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (fileEntry) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.directoryPath = (typeof oConfig.directoryPath !== 'undefined') ? oConfig.directoryPath : '';
	oConfig.create = (typeof oConfig.create !== 'undefined') ? oConfig.create : false;
	oConfig.exclusive = (typeof oConfig.exclusive !== 'undefined') ? oConfig.exclusive : false;
	oConfig.sizeAllocated = (typeof oConfig.sizeAllocated !== 'undefined') ? oConfig.sizeAllocated : 0;
	oConfig.storagePersistent = (typeof oConfig.storagePersistent !== 'undefined') ? oConfig.storagePersistent : true;
	oConfig.storageTemporary = (typeof oConfig.storageTemporary !== 'undefined') ? oConfig.storageTemporary : false;

	var storageType = LocalFileSystem.PERSISTENT; // Por defecto persistente

	// Elegir el tipo de persistencia
	if (oConfig.storagePersistent) {
		storageType = LocalFileSystem.PERSISTENT;
	} else if (oConfig.storageTemporary) {
		storageType = window.TEMPORARY;	
	}

	// Traducir los directorios del objeto de configuración
	var directories = CPLUGIN.file._generateFileStoreDirectories(oConfig);

	// Path donde va a crearse el fichero de forma local
	var path = directories[0] + oConfig.directoryPath; // --8<-- Aunque en el array puede haber varios, solo se utiliza el primer directorio detectado

	// Solicitar el sistema de archivos
    window.requestFileSystem(storageType, oConfig.sizeAllocated, function (fileSystem) {

    	// Solicitar el directorio del sistema de archivos
    	window.resolveLocalFileSystemURL(path, function (directoryEntry) {

    		// Acceder al fichero, no crearlo si no existe
	    	directoryEntry.getFile(oConfig.fileName, {create: oConfig.create, exclusive: oConfig.exclusive}, function(fileEntry) {

	    		// Ejecutar el callback, fichero creado correctamente. Se pasa el fileEntry como resultado
	    		oConfig.successCallback(fileEntry);

	    	}, function fail(error) {

			    oConfig.errorCallback(error);
			});

    	}, function fail(error) {

		    oConfig.errorCallback(error);
		});

    }, function fail(error) {

	    oConfig.errorCallback(error);
	});
}

/*
Función para leer un fichero
*/
CPLUGIN.file.readFile = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos el fileEntry
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.fileName == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (result) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.readAsArrayBuffer = (typeof oConfig.readAsArrayBuffer !== 'undefined') ? oConfig.readAsArrayBuffer : true;
	oConfig.readAsBinaryString = (typeof oConfig.readAsBinaryString !== 'undefined') ? oConfig.readAsBinaryString : false;
	oConfig.readAsDataURL = (typeof oConfig.readAsDataURL !== 'undefined') ? oConfig.readAsDataURL : false;
	oConfig.readAsText = (typeof oConfig.readAsText !== 'undefined') ? oConfig.readAsText : false;

	// Configuración para getFile
	var oGetFileConfig = Object.assign({}, oConfig);
	oGetFileConfig.create = false;

	// Fichero conseguido correctamente
	oGetFileConfig.successCallback = function (fileEntry) {

		// Leer el contenido del fileEntry
		fileEntry.file(function (file) {

	        var reader = new FileReader();

	        reader.onloadend = function() {

	        	oConfig.successCallback(this.result);
	        };

	        // Elegir el modo de lectura configurado
	        if (oConfig.readAsArrayBuffer) {

	        	reader.readAsArrayBuffer(file);

	        } else if (oConfig.readAsBinaryString) {

	        	reader.readAsBinaryString(file);

	        } else if (oConfig.readAsDataURL) {

	        	reader.readAsDataURL(file);

	        } else if (oConfig.readAsText) {

	        	reader.readAsText(file);

	        } else {

	        	reader.readAsArrayBuffer(file); // Por defecto si no se especifica ninguno  	
	        }

	    }, oConfig.errorCallback);

	}

	// Error al conseguir el fichero
	oGetFileConfig.errorCallback = function () {

		oConfig.errorCallback();
	}

	// Conseguir el fichero
	oGetFileConfig.fileName = oConfig.fileName;
	CPLUGIN.file.getFile(oGetFileConfig);
}

/*
Función para escrbir un Blob o fichero binario
*/
CPLUGIN.file.writeFile = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos el fileEntry
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.fileName == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (fileEntry) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.isAppend = (typeof oConfig.readAsText !== 'undefined') ? oConfig.isAppend : false;

	// Configuración para getFile
	var oGetFileConfig = Object.assign({}, oConfig);
	oGetFileConfig.create = true;

	// Fichero conseguido correctamente
	oGetFileConfig.successCallback = function (fileEntry) {

		// Para devolverlo en caso correcto
		var fileEntryResult = fileEntry;

		// Crear un objeto fileWriter para el fileEntry
	    fileEntry.createWriter(function (fileWriter) {

	        fileWriter.onwriteend = function() {
	            
	            // Devolver el fileEntry sobre el que acabamos de realizar la escritura
	            oConfig.successCallback(fileEntryResult);
	        };

	        fileWriter.onerror = function (error) {

	            oConfig.errorCallback(error);
	        };

	        // Si no hay objeto de datos creamos uno nuevo
	        if (!oConfig.oData) {
	            oConfig.oData = new Blob([''], { type: '' });
	        }

	        // Si se usa append se escribe al final del contenido existente.
	        if (oConfig.isAppend) {
	            try {
	                fileWriter.seek(fileWriter.length);
	            }
	            catch (e) {
	                oConfig.errorCallback(e);
	            }
	        }

	        // Escribir los datos del objeto de datos en el fichero
	        fileWriter.write(oConfig.oData);
	    });

	}

	// Error al conseguir el fichero
	oGetFileConfig.errorCallback = function (error) {

		oConfig.errorCallback(error);
	}

	// Conseguir el fichero
	oGetFileConfig.fileName = oConfig.fileName;
	CPLUGIN.file.getFile(oGetFileConfig);
}


/*
Función para eliminar un fichero local
*/
CPLUGIN.file.remove = function (oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos el nombre del fichero no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.fileName == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (response) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	//oConfig.directoryPath = (typeof oConfig.directoryPath !== 'undefined') ? oConfig.directoryPath : ''; // --8<-- Borrar si al final lo hace getFile

	// Configuración para getFile
	var oGetFileConfig = Object.assign({}, oConfig);
	oGetFileConfig.create = false;

	// Fichero conseguido correctamente
	oGetFileConfig.successCallback = function (fileEntry) {

		// Eliminar el fichero
	    fileEntry.remove(oConfig.successCallback, oConfig.errorCallback);
	}

	// Error al conseguir el fichero
	oGetFileConfig.errorCallback = function (error) {

		oConfig.errorCallback(error);
	}

	// Conseguir el fichero
	oGetFileConfig.fileName = oConfig.fileName;
	CPLUGIN.file.getFile(oGetFileConfig);
}



/*
Función para mover un fichero local a otro destino local
*/
CPLUGIN.file.moveTo = function (oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos el nombre del fichero no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.fileName == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (entry) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.directoryPath = (typeof oConfig.directoryPath !== 'undefined') ? oConfig.directoryPath : '';
	oConfig.destinyDirectoryPath = (typeof oConfig.destinyDirectoryPath !== 'undefined') ? oConfig.destinyDirectoryPath : '';
	oConfig.newName = (typeof oConfig.newName !== 'undefined') ? oConfig.newName : oConfig.fileName; // Si no se especifica será el mismo que el del fichero a copiar

	// Traducir los directorios del objeto de configuración
	var directoriesOrigin = CPLUGIN.file._generateFileStoreDirectories(oConfig);
	var directoriesDestiny = CPLUGIN.file._generateFileStoreDestinyDirectories(oConfig);

	// Si no se especifica destino se entiende que el origen es el destino
	if (directoriesDestiny) {
		if (directoriesDestiny.length == 0) directoriesDestiny = directoriesOrigin;
	} 

	// Path de origen del fichero local
	var pathOrigin = directoriesOrigin[0] + oConfig.directoryPath; // --8<-- Aunque en el array puede haber varios, solo se utiliza el primer directorio detectado.
	// Path de destino del fichero local
	var pathDestiny = directoriesDestiny[0] + oConfig.destinyDirectoryPath; // --8<-- Aunque en el array puede haber varios, solo se utiliza el primer directorio detectado.

	var originEntry;

	// Solicitar el sistema de archivos
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {

    	// Solicitar el directorio del sistema de archivos
    	window.resolveLocalFileSystemURL(pathOrigin, function (originDirectoryEntry) {

    		// Acceder al fichero, no crearlo si no existe
	    	originDirectoryEntry.getFile(oConfig.fileName, {create: false, exclusive: false}, function(fileEntry) {

	    		originEntry = fileEntry;

	    		// Solicitar el directorio del sistema de archivos
    			window.resolveLocalFileSystemURL(pathDestiny, function (destinyDirectoryEntry) {

		    		// Mover el fichero con el nombre especificado en oConfig.newName
		    		originEntry.moveTo(destinyDirectoryEntry, oConfig.newName, oConfig.successCallback, oConfig.errorCallback);
		    		
		    	}, function fail(error) {

				    oConfig.errorCallback(error);
				});

	    	}, function fail(error) {

			    oConfig.errorCallback(error);
			});

    	}, function fail(error) {

		    oConfig.errorCallback(error);
		});

    }, function fail(error) {

	    oConfig.errorCallback(error);
	});
}

/*
Función para copiar un fichero local a otro destino local
*/
CPLUGIN.file.copyTo = function (oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos el nombre del fichero no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.fileName == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (entry) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.directoryPath = (typeof oConfig.directoryPath !== 'undefined') ? oConfig.directoryPath : '';
	oConfig.destinyDirectoryPath = (typeof oConfig.destinyDirectoryPath !== 'undefined') ? oConfig.destinyDirectoryPath : '';
	oConfig.newName = (typeof oConfig.newName !== 'undefined') ? oConfig.newName : oConfig.fileName; // Si no se especifica será el mismo que el del fichero a copiar

	// Traducir los directorios del objeto de configuración
	var directoriesOrigin = CPLUGIN.file._generateFileStoreDirectories(oConfig);
	var directoriesDestiny = CPLUGIN.file._generateFileStoreDestinyDirectories(oConfig);

	// Si no se especifica destino se entiende que el origen es el destino
	if (directoriesDestiny) {
		if (directoriesDestiny.length == 0) directoriesDestiny = directoriesOrigin;
	}

	// Path de origen del fichero local
	var pathOrigin = directoriesOrigin[0] + oConfig.directoryPath; // --8<-- Aunque en el array puede haber varios, solo se utiliza el primer directorio detectado.
	// Path de destino del fichero local
	var pathDestiny = directoriesDestiny[0] + oConfig.destinyDirectoryPath; // --8<-- Aunque en el array puede haber varios, solo se utiliza el primer directorio detectado.

	var originEntry;

	// Solicitar el sistema de archivos
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {

    	// Solicitar el directorio del sistema de archivos
    	window.resolveLocalFileSystemURL(pathOrigin, function (originDirectoryEntry) {

    		// Acceder al fichero, no crearlo si no existe
	    	originDirectoryEntry.getFile(oConfig.fileName, {create: false, exclusive: false}, function(fileEntry) {

	    		originEntry = fileEntry;

	    		// Solicitar el directorio del sistema de archivos
    			window.resolveLocalFileSystemURL(pathDestiny, function (destinyDirectoryEntry) {

		    		// Copiar el fichero con el nombre especificado en oConfig.newName
		    		originEntry.copyTo(destinyDirectoryEntry, oConfig.newName, oConfig.successCallback, oConfig.errorCallback);
		    		
		    	}, function fail(error) {

				    oConfig.errorCallback(error);
				});

	    	}, function fail(error) {

			    oConfig.errorCallback(error);
			});

    	}, function fail(error) {

		    oConfig.errorCallback(error);
		});

    }, function fail(error) {

	    oConfig.errorCallback(error);
	});
}

/*
Función para descargar un fichero
*/
CPLUGIN.file.downloadFile = function (url, oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos url no puede funcionar
	if (typeof url == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (entry) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	//oConfig.directoryPath = (typeof oConfig.directoryPath !== 'undefined') ? oConfig.directoryPath : ''; // --8<-- Borrar si al final lo hace getFile
	oConfig.fileRename = (typeof oConfig.fileRename !== 'undefined') ? oConfig.fileRename : undefined;

	// Objeto para manejar los ficheros
	var fileTransfer = new FileTransfer();

	// URL codificada desde la que descargar el PDF
	var uri = encodeURI(url);

	// Nombre del fichero que va a crearse de forma local
	var fileName = url.substring(url.lastIndexOf('/')+1);

    // Variable global a nivel de función para poder mantener en todo momento los datos del entry final del fichero
    var finalEntry;

    // Configuración para getFile
	var oGetFileConfig = Object.assign({}, oConfig);
	oGetFileConfig.create = true;

	// Fichero conseguido correctamente
	oGetFileConfig.successCallback = function (fileEntry) {

		// Ruta local del fichero al que se ha accedido
		var localPath = fileEntry.toURL();

		// Limpiar la ruta en caso de ser Android
		if (device.platform === 'Android' && localPath.indexOf('file://') === 0) {
            localPath = localPath.substring(7);
        }

        fileTransfer.download(
        	uri,
            localPath, 
        function(entry) {

        	// Entry final del fichero descargado
        	finalEntry = entry;

        	// Si se marca la opción de renombrar el fichero descargado se copia con otro nombre y se elimina el descargado
        	if (typeof oConfig.fileRename != 'undefined') {

        		// No queremos hacer referencia a oConfig, necesitamos que oCopyToConfig sea independiente para modificar su successCallback de forma independiente
        		var oCopyToConfig = Object.assign({}, oConfig);

        		// Fichero copiado correctamente
        		oCopyToConfig.successCallback = function (entry) {

        			// Entry final del fichero copiado para ser renombrado
        			finalEntry = entry;

        			// No queremos hacer referencia a oConfig, necesitamos que oRemoveConfig sea independiente para modificar su successCallback de forma independiente
        			var oRemoveConfig = Object.assign({}, oConfig);

        			// Fichero original eliminado correctamente
        			oRemoveConfig.successCallback = function (response) {

        				oConfig.successCallback(finalEntry);
        			}

        			// Error al eleiminar el fichero original
        			oRemoveConfig.errorCallback = function (error) {

        				oConfig.errorCallback(error);
        			}

        			// Eliminar el fichero original descargado
        			oRemoveConfig.fileName = fileName;
        			CPLUGIN.file.remove(oRemoveConfig);
        		}

        		// Error al copiar el fichero
        		oCopyToConfig.errorCallback = function (error) {

        			oConfig.errorCallback(error);
        		}

            	oCopyToConfig.fileName = fileName;
            	oCopyToConfig.newName = oConfig.fileRename;
            	CPLUGIN.file.copyTo(oCopyToConfig);

        	} else {

        		oConfig.successCallback(finalEntry);
        	}

        }, function fail(error) {

			oConfig.errorCallback(error);
		});
	}

	// Error al conseguir el fichero
	oGetFileConfig.errorCallback = function (error) {

		oConfig.errorCallback(error);
	}

	// Conseguir el fichero
	oGetFileConfig.fileName = fileName;
	CPLUGIN.file.getFile(oGetFileConfig);
}

/***********************************************************
 PLUGIN: https://github.com/EddyVerbruggen/Custom-URL-scheme

 Android / iOS
 ***********************************************************/

CPLUGIN.customUrlScheme = {};

// Método privado que inicializa la función a ejecutar cuando se navega a la App desde un deep-link
CPLUGIN.customUrlScheme._handleOpenURL = function () {

	window.handleOpenURL = function handleOpenURL(url) {

		// Necesario para que alertas u otros comandos se ejecuten correctaemte en iOS
  		setTimeout(function() {

			// Ejecutar código de la App pasándo la url desde la que se viene
			CPLUGIN.customUrlScheme.handleOpenURL(url);
  		}, 0);
    }
}
CPLUGIN.customUrlScheme._handleOpenURL();

// Método ofrecido para sobrescribir con código propio
CPLUGIN.customUrlScheme.handleOpenURL = function (url) { return true };

///////////////////////////////

/**********************************************************************************************
 PLUGIN: https://cordova.apache.org/docs/en/1.6.1/cordova/camera/camera.getPicture.html
 OPCIONES: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-camera/index.html

 Android / iOS
 **********************************************************************************************/

// - En getPicture no se ha realizado la integración con CameraPopoverOptions ya que es solo para iOS y no se le ve utilidad.

CPLUGIN.camera = {};

/*
Función para capturar una imagen de la cámara
*/
CPLUGIN.camera.getPicture = function (oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCamera')) return false; // Salir si no esta disponible

	let options = {};

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.quality = (typeof oConfig.quality !== 'undefined') ? oConfig.quality : 50;
	oConfig.dataUrl = (typeof oConfig.dataUrl !== 'undefined') ? oConfig.dataUrl : false;
	oConfig.fileUri = (typeof oConfig.dataUrl !== 'undefined') ? oConfig.dataUrl : false;
	oConfig.nativeUri = (typeof oConfig.dataUrl !== 'undefined') ? oConfig.dataUrl : false;
	oConfig.photolibrary = (typeof oConfig.photolibrary !== 'undefined') ? oConfig.photolibrary : false;
	oConfig.camera = (typeof oConfig.camera !== 'undefined') ? oConfig.camera : false;
	oConfig.savedPhotoAlbum = (typeof oConfig.savedPhotoAlbum !== 'undefined') ? oConfig.savedPhotoAlbum : false;
	oConfig.allowEdit = (typeof oConfig.allowEdit !== 'undefined') ? oConfig.allowEdit : false;
	oConfig.jpg = (typeof oConfig.jpg !== 'undefined') ? oConfig.jpg : false;
	oConfig.png = (typeof oConfig.png !== 'undefined') ? oConfig.png : false;
	oConfig.targetWidth = (typeof oConfig.targetWidth !== 'undefined') ? oConfig.targetWidth : undefined;
	oConfig.targetHeight = (typeof oConfig.targetHeight !== 'undefined') ? oConfig.targetHeight : undefined;
	oConfig.picture = (typeof oConfig.picture !== 'undefined') ? oConfig.picture : false;
	oConfig.video = (typeof oConfig.video !== 'undefined') ? oConfig.video : false;
	oConfig.allMedia = (typeof oConfig.allMedia !== 'undefined') ? oConfig.allMedia : false;
	oConfig.correctOrientation = (typeof oConfig.correctOrientation !== 'undefined') ? oConfig.correctOrientation : false;
	oConfig.saveToPhotoAlbum = (typeof oConfig.saveToPhotoAlbum !== 'undefined') ? oConfig.saveToPhotoAlbum : false;
	oConfig.cameraBack = (typeof oConfig.cameraBack !== 'undefined') ? oConfig.cameraBack : false;
	oConfig.cameraFront = (typeof oConfig.cameraFront !== 'undefined') ? oConfig.cameraFront : false;

	if (typeof oConfig.quality !== "undefined")options.quality = oConfig.quality; // Calidad de la imagen salvada, rango de 0 a 100.
	// Decidir el formato de destino de la imagen tomada
	if (!oConfig.dataUrl && !oConfig.fileUri && !oConfig.nativeUri) {
		options.destinationType = Camera.DestinationType.DATA_URL; // Base64
	} else if (oConfig.dataUrl) {
		options.destinationType = Camera.DestinationType.DATA_URL; // Base64
	} else if (oConfig.fileUri) {
		options.destinationType = Camera.DestinationType.FILE_URI; // File Uri (content://media/...)
	} else if (oConfig.nativeUri) {
		options.destinationType = Camera.DestinationType.NATIVE_URI; // native Uri (asset-library://...)
	}
	// Decidir de donde escoger la imagen, origen de la imagen
	if (!oConfig.photolibrary && !oConfig.camera && !oConfig.savedPhotoAlbum) {
		options.sourceType = Camera.PictureSourceType.CAMERA; // Cámara
	} else if (oConfig.photolibrary) {
		options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY; // Librería de imágenes
	} else if (oConfig.camera) {
		options.sourceType = Camera.PictureSourceType.CAMERA; // Librería de imágenes
	} else if (oConfig.savedPhotoAlbum) {
		options.sourceType = Camera.PictureSourceType.SAVEDPHOTOALBUM; // Imágenes solo capturadas por la cámara
	}
	if (typeof oConfig.allowEdit !== "undefined") options.allowEdit = oConfig.allowEdit; // Permitir edición de la imagen
	// Decidir el tipo de codificación, JPG o PNG
	if (!oConfig.jpg && !oConfig.png) {
		options.encodingType = Camera.EncodingType.JPEG; // JPG
	} else if (oConfig.jpg) {
		options.encodingType = Camera.EncodingType.JPEG; // JPG
	} else if (oConfig.png) {
		options.encodingType = Camera.EncodingType.PNG; // PNG
	}
	if (typeof oConfig.targetWidth !== "undefined") options.targetWidth = oConfig.targetWidth; // Ancho para el escalado de la imagen
	if (typeof oConfig.targetHeight !== "undefined") options.targetHeight = oConfig.targetHeight; // Alto para el escalado de la imagen
	// Decidir el tipo de recurso multimedia
	if (!oConfig.picture && !oConfig.video && !oConfig.allMedia) {
		options.mediaType = Camera.MediaType.PICTURE ; // PICTURE
	} else if (oConfig.picture) {
		options.mediaType = Camera.MediaType.PICTURE ; // PICTURE
	} else if (oConfig.video) {
		options.mediaType = Camera.MediaType.VIDEO ; // VIDEO (Solo devuelve URL)
	} else if (oConfig.allMedia) {
		options.mediaType = Camera.MediaType.ALLMEDIA ; // ALLMEDIA
	}
	if (typeof oConfig.correctOrientation !== "undefined") options.correctOrientation = oConfig.correctOrientation; // Rotar la imagen a la orientación correcta cuando sea tomada la imágen
	if (typeof oConfig.saveToPhotoAlbum !== "undefined") options.saveToPhotoAlbum = oConfig.saveToPhotoAlbum; // Guardar la imagen en el albun de fotos al terminar la captura
	// Decidir que camara usar, la delantera o trasera
	if (!oConfig.cameraBack && !oConfig.cameraFront) {
		options.cameraDirection = Camera.Direction.BACK ; // BACK
	} else if (oConfig.cameraBack) {
		options.cameraDirection = Camera.Direction.BACK ; // BACK
	} else if (oConfig.cameraFront) {
		options.cameraDirection = Camera.Direction.FRONT ; // FRONT
	}

	navigator.camera.getPicture( oConfig.successCallback, oConfig.errorCallback, options );
}

/**************************************************************************************
 PLUGIN: https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview

 Android / iOS
 **************************************************************************************/

CPLUGIN.cameraPreview = {};

/*
Función para iniciar la cámara
*/
CPLUGIN.cameraPreview.startCamera = function (oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	let options = {};

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.x = (typeof oConfig.x !== 'undefined') ? oConfig.x : 0;
	oConfig.y = (typeof oConfig.y !== 'undefined') ? oConfig.y : 0;
	oConfig.width = (typeof oConfig.width !== 'undefined') ? oConfig.width : window.screen.width;
	oConfig.height = (typeof oConfig.height !== 'undefined') ? oConfig.height : window.screen.height;
	oConfig.cameraBack = (typeof oConfig.cameraBack !== 'undefined') ? oConfig.cameraBack : CameraPreview.CAMERA_DIRECTION.BACK;
	oConfig.cameraFront = (typeof oConfig.cameraFront !== 'undefined') ? oConfig.cameraFront : CameraPreview.CAMERA_DIRECTION.FRONT;
	oConfig.toBack = (typeof oConfig.toBack !== 'undefined') ? oConfig.toBack : false;
	oConfig.tapPhoto = (typeof oConfig.tapPhoto !== 'undefined') ? oConfig.tapPhoto : true;
	oConfig.tapFocus = (typeof oConfig.tapFocus !== 'undefined') ? oConfig.tapFocus : false;
	oConfig.previewDrag = (typeof oConfig.previewDrag !== 'undefined') ? oConfig.previewDrag : false;
	oConfig.storeToFile = (typeof oConfig.storeToFile !== 'undefined') ? oConfig.storeToFile : false;
	oConfig.disableExifHeaderStripping = (typeof oConfig.disableExifHeaderStripping !== 'undefined') ? oConfig.disableExifHeaderStripping : false; // Android

	options.x = oConfig.x;
	options.y = oConfig.y;
	options.width = oConfig.width;
	options.height = oConfig.height;
	options.toBack = oConfig.toBack;
	options.tapPhoto = oConfig.tapPhoto;
	options.tapFocus = oConfig.tapFocus;
	options.previewDrag = oConfig.previewDrag;
	options.storeToFile = oConfig.storeToFile;
	options.disableExifHeaderStripping = oConfig.disableExifHeaderStripping;
	// Decidir si usar la cámara trasera o frontal
	if (!oConfig.cameraBack && !oConfig.cameraFront) {
		options.camera = CameraPreview.CAMERA_DIRECTION.BACK;
	} else if (oConfig.cameraBack) {
		options.camera = CameraPreview.CAMERA_DIRECTION.BACK;
	} else if (oConfig.cameraFront) {
		options.camera = CameraPreview.CAMERA_DIRECTION.FRONT;
	}

	CameraPreview.startCamera(options, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para detener el inicio de la cámara
*/
CPLUGIN.cameraPreview.stopCamera = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.stopCamera(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para cambiar de cámara frontal a trasera y viceversa
*/
CPLUGIN.cameraPreview.switchCamera = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.switchCamera(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para mostrar el contenedor de la cámara
*/
CPLUGIN.cameraPreview.show = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.show(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para ocultar el contenedor de la cámara
*/
CPLUGIN.cameraPreview.hide = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.hide(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para capturar la imagen de la cámara
*/
CPLUGIN.cameraPreview.takePicture = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	let options = {};

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (base64OrFilePath) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.width = (typeof oConfig.width !== 'undefined') ? oConfig.width : undefined;
	oConfig.height = (typeof oConfig.height !== 'undefined') ? oConfig.height : undefined;
	oConfig.quality = (typeof oConfig.quality !== 'undefined') ? oConfig.quality : undefined;

	if (oConfig.width) options.width = oConfig.width;
	if (oConfig.height) options.height = oConfig.height;
	if (oConfig.quality) options.quality = oConfig.quality;

	CameraPreview.takePicture(options, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para capturar una instantanea de la cámara
*/
CPLUGIN.cameraPreview.takeSnapshot = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	let options = {};

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (base64OrFilePath) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.quality = (typeof oConfig.quality !== 'undefined') ? oConfig.quality : undefined;

	if (oConfig.quality) options.quality = oConfig.quality;

	CameraPreview.takeSnapshot(options, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para devolver los modos del foco de la camara soportados
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.FocusMode
*/
CPLUGIN.cameraPreview.getSupportedFocusModes = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (focusModes) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getSupportedFocusModes(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir un modo del foco
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.FocusMode
*/
CPLUGIN.cameraPreview.setFocusMode = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (focusModes) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.focusMode = (typeof oConfig.focusMode !== 'undefined') ? oConfig.focusMode : undefined;

	if (!oConfig.focusMode) return false; // Salir si no hay un modo del foco

	CameraPreview.setFocusMode(oConfig.focusMode, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar el modo del foco actual
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.FocusMode
*/
CPLUGIN.cameraPreview.getFocusMode = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (currentFocusMode) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getFocusMode(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para devolver los modos del flash de la camara soportados
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.FlashMode
*/
CPLUGIN.cameraPreview.getSupportedFlashModes = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (flashModes) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getSupportedFlashModes(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir un modo del flash
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.FlashMode
*/
CPLUGIN.cameraPreview.setFlashMode = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (flashModes) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.flashMode = (typeof oConfig.flashMode !== 'undefined') ? oConfig.flashMode : undefined;

	if (!oConfig.flashMode) return false; // Salir si no hay un modo del flash

	CameraPreview.setFlashMode(oConfig.flashMode, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar el modo del flash actual
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.FlashMode
*/
CPLUGIN.cameraPreview.getFlashMode = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (currentFlashMode) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getFlashMode(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar el ancho del FOV
*/
CPLUGIN.cameraPreview.getHorizontalFOV = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (horizontalFOV) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getHorizontalFOV(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar los efectos de colores soportados
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.ColorEffect
*/
CPLUGIN.cameraPreview.getSupportedColorEffects = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (colorEffects) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getSupportedColorEffects(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir un efecto de color
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.ColorEffect
*/
CPLUGIN.cameraPreview.setColorEffect = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (colorEffects) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.colorEffect = (typeof oConfig.colorEffect !== 'undefined') ? oConfig.colorEffect : undefined;

	if (!oConfig.colorEffect) return false; // Salir si no hay un modo del flash

	CameraPreview.setColorEffect(oConfig.colorEffect, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir un zoom concreto
*/
CPLUGIN.cameraPreview.setZoom = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.zoom = (typeof oConfig.zoom !== 'undefined') ? oConfig.zoom : 1;

	CameraPreview.setZoom(oConfig.zoom, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar el máximo zoom posible de la cámara
*/
CPLUGIN.cameraPreview.getMaxZoom = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (maxZoom) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getMaxZoom(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar los modos de balanceo del blanco de la cámara
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.WhiteBalanceMode
*/
CPLUGIN.cameraPreview.getSupportedWhiteBalanceModes = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (whiteBalanceModes) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getSupportedWhiteBalanceModes(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar el modo de balanceo del blanco de la cámara actual
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.WhiteBalanceMode
*/
CPLUGIN.cameraPreview.getWhiteBalanceMode = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (whiteBalanceMode) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getWhiteBalanceMode(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir un modo de balanceo del blanco de la cámara
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.WhiteBalanceMode
*/
CPLUGIN.cameraPreview.setWhiteBalanceMode = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.whiteBalanceMode = (typeof oConfig.whiteBalanceMode !== 'undefined') ? oConfig.whiteBalanceMode : undefined;

	if (!oConfig.whiteBalanceMode) return false;

	CameraPreview.setWhiteBalanceMode(oConfig.whiteBalanceMode, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar los modos de exposición de la cámara
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.ExposureMode
*/
CPLUGIN.cameraPreview.getExposureModes = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (exposureModes) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getExposureModes(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar el modo de exposición actual de la cámara
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.ExposureMode
*/
CPLUGIN.cameraPreview.getExposureMode = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (exposureMode) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getExposureMode(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir un modo de exposición de la cámara
https://github.com/cordova-plugin-camera-preview/cordova-plugin-camera-preview#camera_Settings.ExposureMode
*/
CPLUGIN.cameraPreview.setExposureMode = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.exposureMode = (typeof oConfig.exposureMode !== 'undefined') ? oConfig.exposureMode : undefined;

	if (!oConfig.exposureMode) return false;

	CameraPreview.setExposureMode(oConfig.exposureMode, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar el rango de la compensación de la exposición actual de la cámara
*/
CPLUGIN.cameraPreview.getExposureCompensationRange = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (expoxureRange) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getExposureCompensationRange(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar la compensación de la exposición actual de la cámara
*/
CPLUGIN.cameraPreview.getExposureCompensation = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (expoxureCompensation) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getExposureCompensation(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir la compensación de la exposición de la cámara
*/
CPLUGIN.cameraPreview.setExposureCompensation = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.exposureCompensation = (typeof oConfig.exposureCompensation !== 'undefined') ? oConfig.exposureCompensation : undefined;

	if (!oConfig.exposureCompensation) return false;

	CameraPreview.setExposureCompensation(oConfig.exposureCompensation, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para cambiar las dimensiones de la ventana de previsualización
*/
CPLUGIN.cameraPreview.setPreviewSize = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	var dimensions = {};

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.width = (typeof oConfig.width !== 'undefined') ? oConfig.width : window.screen.width; // Si no se especifica nada se usa el tamaño máximo de la pantalla
	oConfig.height = (typeof oConfig.height !== 'undefined') ? oConfig.height : window.screen.height; // Si no se especifica nada se usa el tamaño máximo de la pantalla

	if (oConfig.width) dimensions.width = oConfig.width;
	if (oConfig.height) dimensions.height = oConfig.height;

	CameraPreview.setPreviewSize(dimensions, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar las dimensiones soportadas de la imagen
*/
CPLUGIN.cameraPreview.getSupportedPictureSizes = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (dimensions) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getSupportedPictureSizes(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para recuperar las características de la cámara
*/
CPLUGIN.cameraPreview.getCameraCharacteristics = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (characteristics) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.getCameraCharacteristics(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir un punto de foco específico. Esta función asume que la cámara esta en máximo tamaño.
*/
CPLUGIN.cameraPreview.tapToFocus = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.x = (typeof oConfig.x !== 'undefined') ? oConfig.x : 0;
	oConfig.y = (typeof oConfig.y !== 'undefined') ? oConfig.y : 0;

	CameraPreview.tapToFocus(oConfig.x, oConfig.y, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para definir el evento al presionar el botón de atrás
*/
CPLUGIN.cameraPreview.onBackButton = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	CameraPreview.onBackButton(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para conseguir un Blob a partir de una ruta file://, cuando se trabaja con ficheros locales.
Si la conversión es correcta el resultado se pasa como parámetro de la función de success (image).
*/
CPLUGIN.cameraPreview.getBlob = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginCameraPreview')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (image) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.url = (typeof oConfig.url !== 'undefined') ? oConfig.url : 0;

	if (!oConfig.url) return false;

	CameraPreview.getBlob(oConfig.url, oConfig.successCallback, oConfig.errorCallback);
}

/************************************************************
 PLUGIN: https://github.com/pwlin/cordova-plugin-file-opener2

 Android / iOS
 ************************************************************/

/*
Función para abrir un fichero
*/
CPLUGIN.file.openFile = function (oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFileOpener2')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.filePath = (typeof oConfig.filePath !== 'undefined') ? oConfig.filePath : ''; // Usar el valor de entry.toURL() devuelto por CPLUGIN.file.downloadFile
	oConfig.fileMIMEType = (typeof oConfig.fileMIMEType !== 'undefined') ? oConfig.fileMIMEType : '';
	oConfig.showOpenDialog = (typeof oConfig.showOpenDialog !== 'undefined') ? oConfig.showOpenDialog : false; // Mostrar diálogo con las apps disponibles para abrir el fichero

	if (oConfig.showOpenDialog) {

    	cordova.plugins.fileOpener2.showOpenWithDialog(
	    oConfig.filePath,
	    oConfig.fileMIMEType,
		    {
		        error : oConfig.errorCallback,
		        success : oConfig.successCallback
		    }
		);

	} else {

    	cordova.plugins.fileOpener2.open(
	    oConfig.filePath,
	    oConfig.fileMIMEType,
		    {
		        error : oConfig.errorCallback,
		        success : oConfig.successCallback
		    }
		);

	}
}

/********************************************************************
 PLUGIN: https://github.com/phonegap/phonegap-plugin-barcodescanner
 Generador de códigos de barra online: https://barcode.tec-it.com/en/

 Android / iOS
 ********************************************************************/

CPLUGIN.barcodeScanner = {};

/*
Función para escanear un código
*/
CPLUGIN.barcodeScanner.scan = function (oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginBarcodescanner')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;
	
	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.preferFrontCamera = (typeof oConfig.preferFrontCamera !== 'undefined') ? oConfig.preferFrontCamera : false;
	oConfig.showFlipCameraButton = (typeof oConfig.showFlipCameraButton !== 'undefined') ? oConfig.showFlipCameraButton : false;
	oConfig.showTorchButton = (typeof oConfig.showTorchButton !== 'undefined') ? oConfig.showTorchButton : false;
	oConfig.torchOn = (typeof oConfig.torchOn !== 'undefined') ? oConfig.torchOn : false;
	oConfig.saveHistory = (typeof oConfig.saveHistory !== 'undefined') ? oConfig.saveHistory : false;
	oConfig.prompt = (typeof oConfig.prompt !== 'undefined') ? oConfig.prompt : '';
	oConfig.resultDisplayDuration = (typeof oConfig.resultDisplayDuration !== 'undefined') ? oConfig.resultDisplayDuration : 0;
	oConfig.formats = (typeof oConfig.formats !== 'undefined') ? oConfig.formats : 'QR_CODE';
	oConfig.orientation = (typeof oConfig.orientation !== 'undefined') ? oConfig.orientation : 'portrait';
	oConfig.disableAnimations = (typeof oConfig.disableAnimations !== 'undefined') ? oConfig.disableAnimations : true;
	oConfig.disableSuccessBeep = (typeof oConfig.disableSuccessBeep !== 'undefined') ? oConfig.disableSuccessBeep : false;

	// Lanzar la interfaz de escaneo
	cordova.plugins.barcodeScanner.scan(
      oConfig.successCallback,
      oConfig.errorCallback,
      {
          preferFrontCamera : oConfig.preferFrontCamera, // iOS and Android
          showFlipCameraButton : oConfig.showFlipCameraButton, // iOS and Android
          showTorchButton : oConfig.showTorchButton, // iOS and Android
          torchOn: oConfig.torchOn, // Android, launch with the torch switched on (if available)
          saveHistory: oConfig.saveHistory, // Android, save scan history (default false)
          prompt : oConfig.prompt, // Android
          resultDisplayDuration: oConfig.resultDisplayDuration, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : oConfig.formats, // default: all but PDF_417 and RSS_EXPANDED
          orientation : oConfig.orientation, // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : oConfig.disableAnimations, // iOS
          disableSuccessBeep: oConfig.disableSuccessBeep // iOS and Android
      }
   );

}
CPLUGIN.barcodeScannerScan = CPLUGIN.barcodeScanner.scan; // Mantener integridad con método antiguo

/*************************************************************************************
 PLUGIN ANDROID: https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth
 PLUGIN IOS: https://github.com/EddyVerbruggen/cordova-plugin-touch-id

 Android / iOS
 *************************************************************************************/

CPLUGIN.biometrics = {};

CPLUGIN.biometrics.android = {}; // Objeto que auna la funcionalidad del plugin de Android
CPLUGIN.biometrics.ios = {}; // Objeto que auna la funcionalidad del plugin de iOS

/*
Función principal para mostrar la autenticación biométrica en Android e iOS
*/
CPLUGIN.biometrics.show = function (oConfig) {

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (error) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	// Código a ejecutar en Android
	var showWithAndroid = function (oConfig) {

		// El caso por defecto usará CPLUGIN.biometrics.android
		var oConfigBiometricsAndroidIsAvailable = {};

		oConfigBiometricsAndroidIsAvailable.successCallback = function () {

			oConfig.clientId = 'cpluginBiometricsAIOClientId';

			// Mostrar el diálogo de autenticación biométrica
			CPLUGIN.biometrics.android.encrypt(oConfig);
		}
		oConfigBiometricsAndroidIsAvailable.errorCallback = function (error) {

			// Ejecutar el callback de error de la configuración principal
			oConfig.errorCallback(error);
		}

		// Comprobar que el sistema de autenticación biométrica esta disponible
		CPLUGIN.biometrics.android.isAvailable(oConfigBiometricsAndroidIsAvailable);

	}

	// Código a ejecutar en iOS
	var showWithIos = function (oConfig) {

		// El caso por defecto usará CPLUGIN.biometricsAIO
		var oConfigBiometricsAioIsAvailable = {};

		oConfigBiometricsAioIsAvailable.successCallback = function () {

			// Mostrar el diálogo de autenticación biométrica
			CPLUGIN.biometricsAIO.show(oConfig);
		}
		oConfigBiometricsAioIsAvailable.errorCallback = function (error) {

			// Ejecutar el callback de error de la configuración principal
			oConfig.errorCallback(error);
		}

		// Comprobar que el sistema de autenticación biométrica esta disponible
		CPLUGIN.biometricsAIO.isAvailable(oConfigBiometricsAioIsAvailable);

	}

	// Ejecutar un código y otro en función de la plataforma
	var platform = CPLUGIN.device.platform

	switch(platform) {

		case "Android":

			showWithAndroid(oConfig);

			break;

		case "iOS":

			showWithIos(oConfig);

			break;
	}

}

/********************************************************************
 PLUGIN: https://github.com/niklasmerz/cordova-plugin-fingerprint-aio

 Android / iOS
 ********************************************************************/

CPLUGIN.biometricsAIO = {};

/*
Android usa este valor para encriptar
iOS usa este valor para el diálogo, si no encuentra valor usa 'localizedReason'
*/
CPLUGIN.biometricsAIO.clientId = 'cpluginBiometricsAIOClientId';
/*
Necesario para la encriptación de Android, usar una clave secreta Random
*/
CPLUGIN.biometricsAIO.clientSecret = '';

/*
Función para comprobar que sistema de autenticación por biometría estan disponibles
*/
CPLUGIN.biometricsAIO.isAvailable = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsAIO')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	Fingerprint.isAvailable(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para mostrar el diálogo de autenticación disponible
*/
CPLUGIN.biometricsAIO.show = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsAIO')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	/*
	Si un valor para clientSecret se genera uno Random de 10 caracteres
	*/
	CPLUGIN.biometricsAIO.clientSecret = (CPLUGIN.biometricsAIO.clientSecret.localeCompare('') === 0) ? getRandomId(10) : CPLUGIN.biometricsAIO.clientSecret;

	Fingerprint.show({
      clientId: CPLUGIN.biometricsAIO.clientId, //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
      clientSecret: CPLUGIN.biometricsAIO.clientSecret //Necessary for Android encrpytion of keys. Use random secret key.
    }, oConfig.successCallback, oConfig.errorCallback);

}

/*
Función para comprobar si la autenticación biométrica esta disponible y de que tipo es
URL: https://github.com/EddyVerbruggen/cordova-plugin-touch-id#usage
*/
CPLUGIN.biometrics.ios.isAvailable = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsIos')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	window.plugins.touchid.isAvailable(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para mostrar el diálogo de autenticación biométrica
URL: https://github.com/EddyVerbruggen/cordova-plugin-touch-id#usage
*/
CPLUGIN.biometrics.ios.verifyFingerprint = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsIos')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.text = (typeof oConfig.text !== 'undefined') ? oConfig.text : 'Escanea tu huella dactilar';

	window.plugins.touchid.verifyFingerprint(oConfig.text, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para mostrar el diálogo de autenticación biométrica y en caso de fallo el diálogo de introducir PIN
URL: https://github.com/EddyVerbruggen/cordova-plugin-touch-id#usage
*/
CPLUGIN.biometrics.ios.verifyFingerprintWithCustomPasswordFallback = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsIos')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.text = (typeof oConfig.text !== 'undefined') ? oConfig.text : 'Escanea tu huella dactilar';

	window.plugins.touchid.verifyFingerprintWithCustomPasswordFallback(oConfig.text, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para mostrar el diálogo de autenticación biométrica y en caso de fallo el diálogo de introducir PIN con un texto customizado
URL: https://github.com/EddyVerbruggen/cordova-plugin-touch-id#usage
*/
CPLUGIN.biometrics.ios.verifyFingerprintWithCustomPasswordFallbackAndEnterPasswordLabel = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsIos')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.text = (typeof oConfig.text !== 'undefined') ? oConfig.text : 'Escanea huella dactilar';
	oConfig.textPin = (typeof oConfig.text !== 'undefined') ? oConfig.text : 'Introduce PIN';

	window.plugins.touchid.verifyFingerprintWithCustomPasswordFallbackAndEnterPasswordLabel(oConfig.text, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función para comprobar si ha cambiado o actualizado la BBDD de huellas del sistema operativo y evitar accesos con huellas no originales o de atacantes
- Esta función esta pensada para ser llamada después de 'isAvailable' como capa extra de seguridad
- A partir de iOS
URL: https://github.com/EddyVerbruggen/cordova-plugin-touch-id#security
*/
CPLUGIN.biometrics.ios.didFingerprintDatabaseChange = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsIos')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.callback == 'undefined') return false;

	window.plugins.touchid.didFingerprintDatabaseChange(oConfig.callback);
}



/*
Función para comprobar si la autenticación biométrica esta disponible y en que condiciones.
URL: https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth#fingerprintauthisavailablesuccesscallback-errorcallback
*/
CPLUGIN.biometrics.android.isAvailable = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsAndroid')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	FingerprintAuth.isAvailable(oConfig.successCallback, oConfig.errorCallback);
}

/*
Función que muestra un diálogo nativo para autentificar de forma biométrica
Mediante un usuario y una contraseña devuelve un token
URL: https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth#fingerprintauth-config-object
URL: https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth#fingerprintauthencryptencryptconfig-encryptsuccesscallback-encrypterrorcallback
*/
CPLUGIN.biometrics.android.encrypt = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsAndroid')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.clientId = (typeof oConfig.clientId !== 'undefined') ? oConfig.clientId : getRandomId(10);
	oConfig.username = (typeof oConfig.username !== 'undefined') ? oConfig.username : undefined;
	oConfig.password = (typeof oConfig.password !== 'undefined') ? oConfig.password : undefined;
	oConfig.disableBackup = (typeof oConfig.token !== 'undefined') ? oConfig.token : false;
	oConfig.maxAttempts = (typeof oConfig.maxAttempts !== 'undefined') ? oConfig.token : 5;
	oConfig.locale = (typeof oConfig.locale !== 'undefined') ? oConfig.locale : 'es'; // Por defecto español
	oConfig.userAuthRequired = (typeof oConfig.userAuthRequired !== 'undefined') ? oConfig.userAuthRequired : false;
	oConfig.encryptNoAuth = (typeof oConfig.encryptNoAuth !== 'undefined') ? oConfig.encryptNoAuth : undefined;
	oConfig.dialogTitle = (typeof oConfig.dialogTitle !== 'undefined') ? oConfig.dialogTitle : undefined;
	oConfig.dialogMessage = (typeof oConfig.dialogMessage !== 'undefined') ? oConfig.dialogMessage : undefined;
	oConfig.dialogHint = (typeof oConfig.dialogHint !== 'undefined') ? oConfig.dialogHint : undefined;

	// Generar objeto de configuración
	var encryptConfig = {};
	encryptConfig.clientId = oConfig.clientId;
	if (oConfig.username) encryptConfig.username = oConfig.username;
	if (oConfig.password) encryptConfig.password = oConfig.password;
	encryptConfig.disableBackup = oConfig.disableBackup;
	if (oConfig.maxAttempts) encryptConfig.maxAttempts = oConfig.maxAttempts;
	if (oConfig.locale) encryptConfig.locale = oConfig.locale;
	encryptConfig.userAuthRequired = oConfig.userAuthRequired;
	if (oConfig.encryptNoAuth) encryptConfig.encryptNoAuth = oConfig.encryptNoAuth;
	if (oConfig.dialogTitle) encryptConfig.dialogTitle = oConfig.dialogTitle;
	if (oConfig.dialogMessage) encryptConfig.dialogMessage = oConfig.dialogMessage;
	if (oConfig.dialogHint) encryptConfig.dialogHint = oConfig.dialogHint;

	FingerprintAuth.encrypt(encryptConfig, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función que muestra un diálogo nativo para autentificar de forma biométrica
Mediante un usuario y un token devuelve la contraseña
URL: https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth#fingerprintauth-config-object
URL: https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth#fingerprintauthdecryptdecryptconfig-encryptsuccesscallback-encrypterrorcallback
*/
CPLUGIN.biometrics.android.decrypt = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsAndroid')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.successCallback == 'undefined') return false;
	if (typeof oConfig.token == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.clientId = (typeof oConfig.clientId !== 'undefined') ? oConfig.clientId : getRandomId(10);
	oConfig.username = (typeof oConfig.username !== 'undefined') ? oConfig.username : undefined;
	oConfig.disableBackup = (typeof oConfig.token !== 'undefined') ? oConfig.token : false;
	oConfig.maxAttempts = (typeof oConfig.maxAttempts !== 'undefined') ? oConfig.token : 5;
	oConfig.locale = (typeof oConfig.locale !== 'undefined') ? oConfig.locale : 'es'; // Por defecto español
	oConfig.userAuthRequired = (typeof oConfig.userAuthRequired !== 'undefined') ? oConfig.userAuthRequired : false;
	oConfig.encryptNoAuth = (typeof oConfig.encryptNoAuth !== 'undefined') ? oConfig.encryptNoAuth : undefined;
	oConfig.dialogTitle = (typeof oConfig.dialogTitle !== 'undefined') ? oConfig.dialogTitle : undefined;
	oConfig.dialogMessage = (typeof oConfig.dialogMessage !== 'undefined') ? oConfig.dialogMessage : undefined;
	oConfig.dialogHint = (typeof oConfig.dialogHint !== 'undefined') ? oConfig.dialogHint : undefined;

	// Generar objeto de configuración
	var encryptConfig = {};
	encryptConfig.clientId = oConfig.clientId;
	if (oConfig.username) encryptConfig.username = oConfig.username;
	if (oConfig.token) encryptConfig.token = oConfig.token;
	encryptConfig.disableBackup = oConfig.disableBackup;
	if (oConfig.maxAttempts) encryptConfig.maxAttempts = oConfig.maxAttempts;
	if (oConfig.locale) encryptConfig.locale = oConfig.locale;
	encryptConfig.userAuthRequired = oConfig.userAuthRequired;
	if (oConfig.encryptNoAuth) encryptConfig.encryptNoAuth = oConfig.encryptNoAuth;
	if (oConfig.dialogTitle) encryptConfig.dialogTitle = oConfig.dialogTitle;
	if (oConfig.dialogMessage) encryptConfig.dialogMessage = oConfig.dialogMessage;
	if (oConfig.dialogHint) encryptConfig.dialogHint = oConfig.dialogHint;

	FingerprintAuth.decrypt(decryptConfig, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función que elimina un 'cipher' o cifrado existente
URL: https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth#fingerprintauthdeleteconfig-successcallback-errorcallback
*/
CPLUGIN.biometrics.android.delete = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsAndroid')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos función de vuelta no puede funcionar
	if (typeof oConfig == 'undefined') return false;
	if (typeof oConfig.clientId == 'undefined') return false;
	if (typeof oConfig.username == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (result) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	var deleteConfig = {};
	deleteConfig.clientId = oConfig.clientId;
	deleteConfig.username = oConfig.username;

	FingerprintAuth.delete(deleteConfig, oConfig.successCallback, oConfig.errorCallback);
}

/*
Función que despide o rechaza un diálogo de autenticación si ya existe otro diálogo de autenticación en pantalla
URL: https://github.com/mjwheatley/cordova-plugin-android-fingerprint-auth#fingerprintauthdismisssuccesscallback-errorcallback
*/
CPLUGIN.biometrics.android.dismiss = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginBiometricsAndroid')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (result) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	FingerprintAuth.dismiss(oConfig.successCallback, oConfig.errorCallback);
}

/**********************************************************************************
 PLUGIN: http://docs.phonegap.com/en/edge/cordova_inappbrowser_inappbrowser.md.html

 Android / iOS
 **********************************************************************************/

CPLUGIN.inAppBrowser = {};

/*
Función para abrir una URL
*/
CPLUGIN.inAppBrowser.open = function (url, oConfig) {

	/*
	Valores obligatorios
	*/
	// Si no tenemos url no puede funcionar
	if (typeof url == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.target = (typeof oConfig.target !== 'undefined') ? oConfig.target : '';
	oConfig.params = (typeof oConfig.params !== 'undefined') ? oConfig.params : 'location=yes';
	oConfig.pdfDrive = (typeof oConfig.pdfDrive !== 'undefined') ? oConfig.pdfDrive : false;

	// Abrir una URL a un PDF mediante el permalink de drive
	if (oConfig.pdfDrive == true) {
		url = 'https://docs.google.com/viewer?url=' + url.trim() + '&embedded=true';
	}

	// Abrir la URL
	var ref = window.open(url, oConfig.target, oConfig.params);

	/*
	Definir los eventos en cada caso
	*/
	if (typeof oConfig.loadStart !== 'undefined') ref.addEventListener('loadstart', oConfig.loadStart);
	if (typeof oConfig.loadStop !== 'undefined') ref.addEventListener('loadstop', oConfig.loadStop);
	if (typeof oConfig.loadError !== 'undefined') ref.addEventListener('loaderror', oConfig.loadError);
	if (typeof oConfig.exit !== 'undefined') ref.addEventListener('exit', oConfig.exit);

	// Se devuelve ref por si se necesita trabajar directamente con la instancia
	return ref;
}
CPLUGIN.inAppBrowserOpen = CPLUGIN.inAppBrowser.open; // Mantener integridad con método antiguo

/*****************************************************************************
 PLUGIN: https://github.com/EddyVerbruggen/cordova-plugin-safariviewcontroller

 iOS
 *****************************************************************************/

CPLUGIN.safariViewController = {};

/*
Función para abrir una URL mediante SafariViewController
*/
CPLUGIN.safariViewController.open = function (url, oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginSafariViewController')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos url no puede funcionar
	if (typeof url == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.openedCallback = (typeof oConfig.openedCallback !== 'undefined') ? oConfig.openedCallback : (function () {});
	oConfig.loadedCallback = (typeof oConfig.loadedCallback !== 'undefined') ? oConfig.loadedCallback : (function () {});
	oConfig.closedCallback = (typeof oConfig.closedCallback !== 'undefined') ? oConfig.closedCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (msg) {});
	oConfig.hidden = (typeof oConfig.hidden !== 'undefined') ? oConfig.hidden : false;
	oConfig.animated = (typeof oConfig.animated !== 'undefined') ? oConfig.animated : false;
	oConfig.transition = (typeof oConfig.transition !== 'undefined') ? oConfig.transition : false; // Solo funciona en iOS 9.2 y anteriores con valores curl, flip, fade, slide, se recomienda dejar a false
	oConfig.tintColor = (typeof oConfig.tintColor !== 'undefined') ? oConfig.tintColor : undefined;
	oConfig.barColor = (typeof oConfig.barColor !== 'undefined') ? oConfig.barColor : undefined;
	oConfig.controlTintColor = (typeof oConfig.controlTintColor !== 'undefined') ? oConfig.controlTintColor : undefined;

	SafariViewController.show(
		{
			"url": url,
			"hidden": oConfig.hidden,
			"animated": oConfig.animated,
			"transition": oConfig.transition,
			"enterReaderModeIfAvailable": false, // Modo lectura siempre a false
			"tintColor": oConfig.tintColor, // Por defecto el color es azul
			"barColor": oConfig.barColor, // En iOS 10 o superiores se puede sobrescribir el color de fondo por defecto
			"controlTintColor": oConfig.controlTintColor // En iOS 10 o superiores se puede sobrescribir el color de tintado por defecto
		},
		function(result) {
			if (result.event === 'opened') {
				oConfig.openedCallback(result);
			} else if (result.event === 'loaded') {
				oConfig.loadedCallback(result);
			} else if (result.event === 'closed') {
				oConfig.closedCallback(result);
			}
		},
		function(msg) {
			errorCallback(msg);
		}
	);
}

/*
Función para cerrar SafariViewController
*/
CPLUGIN.safariViewController.close = function () {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginSafariViewController')) return false; // Salir si no esta disponible

	SafariViewController.hide();
}

/***************************************************************************
 https://www.raymondcamden.com/2014/07/15/Cordova-Sample-Reading-a-text-file
 https://github.com/cfjedimaster/Cordova-Examples/tree/master/readtextfile

 Android / iOS
 ***************************************************************************/

CPLUGIN.pdfViewer = {};

CPLUGIN.pdfViewer.openPdf = function (url, oConfig) {
	
	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos url no puede funcionar
	if (typeof url == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.showOpenDialog = (typeof oConfig.showOpenDialog !== 'undefined') ? oConfig.showOpenDialog : false; // Mostrar diálogo con las apps disponibles para abrir el PDF

	/*
	Configuración interna para realizar la descarga del PDF
	*/
	var oDownloadConfig = {};

	oDownloadConfig.dataDirectory = true;

	oDownloadConfig.successCallback = function (entry) {

		oConfig.filePath = entry.toURL();
		oConfig.fileMIMEType = 'application/pdf';

		CPLUGIN.file.openFile(oConfig);
	};

	oDownloadConfig.errorCallback = function (error) {

		oConfig.errorCallback(error);
	};

	// Descargamos el PDF
	CPLUGIN.file.downloadFile(url, oDownloadConfig);

}
CPLUGIN.pdfViewerOpenPdf = CPLUGIN.pdfViewer.openPdf; // Mantener integridad con método antiguo

/***********************************************************************
 PLUGIN: https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin

 Android / iOS
 ***********************************************************************/

CPLUGIN.socialSharing = {};

/*
Función para compartir información mediante otras apps
*/
CPLUGIN.socialSharing.share = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginXSocialsharing')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (result) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (msg) {});
	oConfig.message = (typeof oConfig.message !== 'undefined') ? oConfig.message : undefined;
	oConfig.subject = (typeof oConfig.subject !== 'undefined') ? oConfig.subject : undefined;
	oConfig.files = (typeof oConfig.files !== 'undefined') ? oConfig.files : undefined;
	oConfig.url = (typeof oConfig.url !== 'undefined') ? oConfig.url : undefined;
	oConfig.chooserTitle = (typeof oConfig.chooserTitle !== 'undefined') ? oConfig.chooserTitle : undefined;
	oConfig.appPackageName = (typeof oConfig.appPackageName !== 'undefined') ? oConfig.appPackageName : undefined;

	/*
	Compartir de forma directa
	¡¡ IMPORTANTE !! - Estos casos personalizados pueden variar en el tiempo es mejor usar el compartir por defecto siempre que sea posible
	*/
	if (typeof oConfig.shareWith !== 'undefined') {
		switch (oConfig.shareWith.trim().toLowerCase()) {
			case 'twitter' :
				window.plugins.socialsharing.shareViaTwitter(
					oConfig.message, null, oConfig.url);
				break;
			case 'facebook' :
				window.plugins.socialsharing.shareViaFacebook(
					oConfig.message, null, oConfig.url, oConfig.successCallback, oConfig.errorCallback);
				break;
			case 'instagram' :
				// Para instagram el parámetro URL debe ser la URL de la imagen.
				window.plugins.socialsharing.shareViaInstagram(
					oConfig.message, oConfig.url, oConfig.successCallback, oConfig.errorCallback);
				break;
			case 'whatsapp' :
				window.plugins.socialsharing.shareViaWhatsApp(
					oConfig.message, null, oConfig.url, oConfig.successCallback, oConfig.errorCallback);
				break;
			case 'sms' :
				window.plugins.socialsharing.shareViaSMS(
					oConfig.message, null,  oConfig.successCallback, oConfig.errorCallback);
				break;
			case 'email' :
				// Estos atributos son propios para compartir de forma específica por email
				oConfig.emailTo = (typeof oConfig.emailTo !== 'undefined') ? oConfig.emailTo : undefined; // Array de strings
				oConfig.emailCC = (typeof oConfig.emailTo !== 'undefined') ? oConfig.emailCC : undefined; // Array de strings
				oConfig.emailBCC = (typeof oConfig.emailTo !== 'undefined') ? oConfig.emailBCC : undefined; // Array de strings

				window.plugins.socialsharing.shareViaEmail(
					oConfig.message, oConfig.subject,  oConfig.emailTo, oConfig.emailCC, oConfig.emailBCC, oConfig.files, oConfig.successCallback, oConfig.errorCallback);
				break;
		}

	} else {
		// Lanzar la interfaz de compartir por defecto
		window.plugins.socialsharing.shareWithOptions(oConfig, oConfig.successCallback, oConfig.errorCallback);
	}
}
CPLUGIN.socialSharingShare = CPLUGIN.socialSharing.share; // Mantener integridad con método antiguo

/**********************************************************
 PLUGIN: https://github.com/apache/cordova-plugin-statusbar

 Android / iOS
 **********************************************************/

CPLUGIN.statusBar = {};

CPLUGIN.statusBar.setConfig = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginStatusbar')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.overlaysWebView = (typeof oConfig.overlaysWebView !== 'undefined') ? oConfig.overlaysWebView : undefined;
	oConfig.styleDefault = (typeof oConfig.styleDefault !== 'undefined') ? oConfig.styleDefault : undefined;
	oConfig.styleLightContent = (typeof oConfig.styleLightContent !== 'undefined') ? oConfig.styleLightContent : undefined;
	oConfig.styleBlackTranslucent = (typeof oConfig.styleBlackTranslucent !== 'undefined') ? oConfig.styleBlackTranslucent : undefined;
	oConfig.styleBlackOpaque = (typeof oConfig.styleBlackOpaque !== 'undefined') ? oConfig.styleBlackOpaque : undefined;
	oConfig.backgroundColorByName = (typeof oConfig.backgroundColorByName !== 'undefined') ? oConfig.backgroundColorByName : undefined;
	oConfig.backgroundColorByHexString = (typeof oConfig.backgroundColorByHexString !== 'undefined') ? oConfig.backgroundColorByHexString : undefined;
	oConfig.hide = (typeof oConfig.hide !== 'undefined') ? oConfig.hide : undefined;
	oConfig.show = (typeof oConfig.show !== 'undefined') ? oConfig.show : undefined;
	oConfig.isVisible = (typeof oConfig.isVisible !== 'undefined') ? oConfig.isVisible : undefined;

	if (typeof oConfig.overlaysWebView !== 'undefined') StatusBar.overlaysWebView(oConfig.overlaysWebView); // iOS 7+ y Android 5+
	if (typeof oConfig.styleDefault !== 'undefined') StatusBar.styleDefault(oConfig.styleDefault); // iOS y Android 6+
	if (typeof oConfig.styleLightContent !== 'undefined') StatusBar.styleLightContent(oConfig.styleLightContent); // iOS y Android 6+
	if (typeof oConfig.styleBlackTranslucent !== 'undefined') StatusBar.styleBlackTranslucent(oConfig.styleBlackTranslucent); // iOS y Android 6+
	if (typeof oConfig.styleBlackOpaque !== 'undefined') StatusBar.styleBlackOpaque(oConfig.styleBlackOpaque); // iOS y Android 6+
	if (typeof oConfig.backgroundColorByName !== 'undefined') StatusBar.backgroundColorByName(oConfig.backgroundColorByName); // iOS y Android 5+
	if (typeof oConfig.backgroundColorByHexString !== 'undefined') StatusBar.backgroundColorByHexString(oConfig.backgroundColorByHexString); // iOS y Android 5+
	if (typeof oConfig.hide !== 'undefined') StatusBar.hide(oConfig.hide); // iOS y Android
	if (typeof oConfig.show !== 'undefined') StatusBar.show(oConfig.show); // iOS y Android
	if (typeof oConfig.isVisible !== 'undefined') StatusBar.isVisible = oConfig.isVisible; // iOS y Android
	if (typeof oConfig.statusTap !== 'undefined') ref.addEventListener('statusTap', oConfig.statusTap); // iOS
}
CPLUGIN.statusBarSetConfig = CPLUGIN.statusBar.setConfig // Mantener integridad con método antiguo

/********************************************************************************************
 PLUGIN: https://github.com/dpa99c/cordova-diagnostic-plugin#permissionstatus-constants
 Permisos de Android: https://github.com/dpa99c/cordova-diagnostic-plugin#android-permissions

 Android / iOS
 ********************************************************************************************/

CPLUGIN.permissions = {};

// Conseguir el array de permisos introducidos
CPLUGIN.permissions._generateAndroidPermissions = function (oConfig) {
	
	var permissions = [];

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	if (typeof oConfig.READ_CALENDAR !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.READ_CALENDAR);
	if (typeof oConfig.WRITE_CALENDAR !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.WRITE_CALENDAR);
	if (typeof oConfig.CAMERA !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.CAMERA);
	if (typeof oConfig.READ_CONTACTS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.READ_CONTACTS);
	if (typeof oConfig.WRITE_CONTACTS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.WRITE_CONTACTS);
	if (typeof oConfig.GET_ACCOUNTS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.GET_ACCOUNTS);
	if (typeof oConfig.ACCESS_FINE_LOCATION !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.ACCESS_FINE_LOCATION);
	if (typeof oConfig.ACCESS_COARSE_LOCATION !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.ACCESS_COARSE_LOCATION);
	if (typeof oConfig.RECORD_AUDIO !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.RECORD_AUDIO);
	if (typeof oConfig.READ_PHONE_STATE !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.READ_PHONE_STATE);
	if (typeof oConfig.CALL_PHONE !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.CALL_PHONE);
	if (typeof oConfig.ADD_VOICEMAIL !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.ADD_VOICEMAIL);
	if (typeof oConfig.USE_SIP !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.USE_SIP);
	if (typeof oConfig.PROCESS_OUTGOING_CALLS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.PROCESS_OUTGOING_CALLS);
	if (typeof oConfig.READ_CALL_LOG !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.READ_CALL_LOG);
	if (typeof oConfig.WRITE_CALL_LOG !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.WRITE_CALL_LOG);
	if (typeof oConfig.SEND_SMS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.SEND_SMS);
	if (typeof oConfig.RECEIVE_SMS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.RECEIVE_SMS);
	if (typeof oConfig.READ_SMS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.READ_SMS);
	if (typeof oConfig.RECEIVE_WAP_PUSH !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.RECEIVE_WAP_PUSH);
	if (typeof oConfig.RECEIVE_MMS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.RECEIVE_MMS);
	if (typeof oConfig.WRITE_EXTERNAL_STORAGE !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.WRITE_EXTERNAL_STORAGE);
	if (typeof oConfig.READ_EXTERNAL_STORAGE !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.READ_EXTERNAL_STORAGE);
	if (typeof oConfig.BODY_SENSORS !== 'undefined') permissions.push(cordova.plugins.diagnostic.permission.BODY_SENSORS);

	return permissions;
}


// (Android) Comprueba si estan concedidos una lista de permisos
CPLUGIN.permissions.getPermissionsAuthorizationStatus = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (statuses) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	// Traducir los permisos del objeto de configuración
	var permissions = CPLUGIN.permissions._generateAndroidPermissions(oConfig);

	cordova.plugins.diagnostic.getPermissionsAuthorizationStatus(
		oConfig.successCallback, 
		oConfig.errorCallback,
		permissions
	);
}

// (Android) Solicita los permisos de una lista de permisos
CPLUGIN.permissions.requestRuntimePermissions = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (statuses) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	// Traducir los permisos del objeto de configuración
	var permissions = CPLUGIN.permissions._generateAndroidPermissions(oConfig);

	cordova.plugins.diagnostic.requestRuntimePermissions(
		oConfig.successCallback, 
		oConfig.errorCallback,
		permissions
	);
}

// Comprueba si la cámara esta presente en el dispositivo
CPLUGIN.permissions.isCameraPresent = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (present) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isCameraPresent(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

// Comprueba si la cámara esta disponible
CPLUGIN.permissions.isCameraAvailable = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (available) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isCameraAvailable(
		oConfig.successCallback, 
		oConfig.errorCallback,
		{externalStorage: false}
	);
}

// Comprueba si la cámara esta autorizada para la App
CPLUGIN.permissions.isCameraAuthorized = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (authorized) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isCameraAvailable(
		oConfig.successCallback, 
		oConfig.errorCallback,
		{externalStorage: false}
	);
}

// Devuelve el estado actual de la autorización de la cámara
CPLUGIN.permissions.getCameraAuthorizationStatus = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (status) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.getCameraAuthorizationStatus(
		oConfig.successCallback, 
		oConfig.errorCallback,
		{externalStorage: false}
	);
}

// Requerir al usuario el permiso para usar la cámara
CPLUGIN.permissions.requestCameraAuthorization = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (status) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.requestCameraAuthorization(
		oConfig.successCallback, 
		oConfig.errorCallback,
		{externalStorage: false}
	);
}

// Comprueba si la localizacion del dispositivo es accesible por la App
CPLUGIN.permissions.isLocationAvailable = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (available) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isLocationAvailable(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

// Comprueba si la configuración de localización esta activada en el dispositivo
CPLUGIN.permissions.isLocationEnabled = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (enabled) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isLocationEnabled(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

// Comprueba si la App esta autorizada a usar la localización
CPLUGIN.permissions.isLocationAuthorized = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (authorized) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isLocationAuthorized(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

// Devuelve el estado actual de la autorización de la localización
CPLUGIN.permissions.getLocationAuthorizationStatus = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (status) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.getLocationAuthorizationStatus(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

// Requerir al usuario el permiso para usar la localización
CPLUGIN.permissions.requestLocationAuthorization = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (status) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.requestLocationAuthorization(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

// Registra una función a ejecutar cuando un cambio en el estado de la localización ocurre
CPLUGIN.permissions.registerLocationStateChangeHandler = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.callback = (typeof oConfig.callback !== 'undefined') ? oConfig.callback : (function (state) {});

	cordova.plugins.diagnostic.registerLocationStateChangeHandler(
		oConfig.callback
	);
}

// Comprueba si las notificaciones Push están activadas
CPLUGIN.permissions.isRemoteNotificationsEnabled = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (enabled) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isRemoteNotificationsEnabled(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

/************************************************************
 PLUGIN: https://github.com/passslot/cordova-plugin-passbook/
 Passbook online de ejemplo: https://d.pslot.io/cQY2f

 iOS
 ************************************************************/

CPLUGIN.passbook = {};

// Descarga un pass de la URL y lo muestra al usuario.
CPLUGIN.passbook.downloadPass = function (url, oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginPassbook')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos url no puede funcionar
	if (typeof url == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (pass, added) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.oHeaders = (typeof oConfig.oHeaders !== 'undefined') ? oConfig.oHeaders : undefined;

	oConfig.callData = undefined;
	if (typeof oConfig.oHeaders !== 'undefined')  {
		oConfig.callData = {};
		oConfig.callData.url = url;
		oConfig.callData.headers = oConfig.oHeaders;
	} else {
		oConfig.callData = url;
	}

	Passbook.downloadPass(oConfig.callData, oConfig.successCallback, oConfig.errorCallback);
}

// Ejecutar cada callback dependiendo de si Passbook esta disponible en el dispositivo.
CPLUGIN.passbook.available = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginPassbook')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function () {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function () {});

	Passbook.available(function(isAvailable) {
		if (isAvailable) {
			oConfig.successCallback();
		} else {
			oConfig.errorCallback();
		}
	});
}

// Muestra al usuario el pass del fichero local, cuando se muestra correctamente y el usuario cancela o incluye el pass se ejecuta el passCallback.
CPLUGIN.passbook.addPass = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginPassbook')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (pass, added) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.file = (typeof oConfig.file !== 'undefined') ? oConfig.file : undefined;

	var finalFile = cordova.file.applicationDirectory + oConfig.file;

	Passbook.addPass(finalFile, oConfig.successCallback, oConfig.errorCallback);
}

/*****************************************************
 Funcionalidad centralizada propia para manejar passes

 Android / iOS
 *****************************************************/

CPLUGIN.pass = {};

// Descarga un pass de la URL y lo muestra al usuario.
CPLUGIN.pass.downloadPass = function (url, oConfig) {

	/*
	Valores obligatorios
	*/
	// Si no tenemos al menos url no puede funcionar
	if (typeof url == 'undefined') return false;

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (pass, added) {}); // added es solo del plugin de Passbook de iOS
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.oHeaders = (typeof oConfig.oHeaders !== 'undefined') ? oConfig.oHeaders : undefined;

	// Caso Android
	var caseAndroid = function (url, oConfig) {

		var oDownloadConfig = {};

		oDownloadConfig.dataDirectory = true; // Descargar en memoria interna, en la carpeta de descargas

		oDownloadConfig.successCallback = function (entry) {

			
			oConfig.filePath = entry.toURL();
			oConfig.fileMIMEType = 'application/vnd.apple.pkpass';

			CPLUGIN.file.openFile(oConfig);
		};

		oDownloadConfig.errorCallback = function (error) {

			oConfig.errorCallback(error);
		};

		CPLUGIN.file.downloadFile(url, oDownloadConfig);
	}

	// Caso iOS
	var caseIOs = function (url, oConfig) {

		CPLUGIN.passbook.downloadPass(url, oConfig);
	}

	var platform = device.platform;

	switch(platform) {

		case "Android":

			if (!CPLUGIN.core._checkPlugin('hasCordovaPluginFile')) return false; // Salir si no esta disponible

			caseAndroid(url, oConfig);
			break;

		case "iOS":

			if (!CPLUGIN.core._checkPlugin('hasCordovaPluginPassbook')) return false; // Salir si no esta disponible

			caseIOs(url, oConfig)
			break;
	}
}

/***********************************************************************************************
 PLUGIN: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-dialogs/index.html

 Android / iOS
 ***********************************************************************************************/

CPLUGIN.dialog = {};

// Muestra una alerta nativa
CPLUGIN.dialog.alert = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDialogs')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.callback = (typeof oConfig.callback !== 'undefined') ? oConfig.callback : (function () {});
	oConfig.message = (typeof oConfig.message !== 'undefined') ? oConfig.message : '';
	oConfig.title = (typeof oConfig.title !== 'undefined') ? oConfig.title : undefined;
	oConfig.buttonText = (typeof oConfig.buttonText !== 'undefined') ? oConfig.buttonText : undefined;
	
	navigator.notification.alert(oConfig.message, oConfig.callback, oConfig.title, oConfig.buttonText);
}

// Muestra un diálogo de confirmación nativo
CPLUGIN.dialog.confirm = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDialogs')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.callback = (typeof oConfig.callback !== 'undefined') ? oConfig.callback : (function (button) {});
	oConfig.message = (typeof oConfig.message !== 'undefined') ? oConfig.message : '';
	oConfig.title = (typeof oConfig.title !== 'undefined') ? oConfig.title : undefined;
	oConfig.buttonOkText = (typeof oConfig.buttonOkText !== 'undefined') ? oConfig.buttonOkText : '';
	oConfig.buttonCancelText = (typeof oConfig.buttonCancelText !== 'undefined') ? oConfig.buttonCancelText : '';
	
	navigator.notification.confirm(oConfig.message, oConfig.callback, oConfig.title, oConfig.buttonOkText.trim()+','+oConfig.buttonCancelText.trim());
}

// El dispositivo reproduce un sonido un número de veces
CPLUGIN.dialog.beep = function (times) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDialogs')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	times = (typeof times !== 'undefined') ? times : 1;
	
	navigator.notification.beep(times);
}

// El dispositivo muestra un diálogo prompt
CPLUGIN.dialog.prompt = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDialogs')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.callback = (typeof oConfig.callback !== 'undefined') ? oConfig.callback : (function (results) {});
	oConfig.message = (typeof oConfig.message !== 'undefined') ? oConfig.message : '';
	oConfig.title = (typeof oConfig.title !== 'undefined') ? oConfig.title : undefined;
	oConfig.inputText = (typeof oConfig.inputText !== 'undefined') ? oConfig.inputText : '';
	oConfig.buttonOkText = (typeof oConfig.buttonOkText !== 'undefined') ? oConfig.buttonOkText : '';
	oConfig.buttonCancelText = (typeof oConfig.buttonCancelText !== 'undefined') ? oConfig.buttonCancelText : '';
	
	navigator.notification.prompt(oConfig.message, oConfig.callback, oConfig.title, oConfig.buttonOkText.trim()+','+oConfig.buttonCancelText.trim(), oConfig.inputText);
}

/************************************************************
 Lógica para conseguir información del la red del dispositivo

 Android / iOS
 ************************************************************/

CPLUGIN.connection = {};

document.addEventListener("deviceready", function() {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginNetworkInformation')) return false; // Salir si no esta disponible	

	// Constantes con los tipos de red
	CPLUGIN.connection.UNKNOWN = (!!Connection) ? Connection.UNKNOWN : undefined;
	CPLUGIN.connection.ETHERNET = (!!Connection) ? Connection.ETHERNET : undefined;
	CPLUGIN.connection.WIFI = (!!Connection) ? Connection.WIFI : undefined;
	CPLUGIN.connection.CELL_2G = (!!Connection) ? Connection.CELL_2G : undefined;
	CPLUGIN.connection.CELL_3G = (!!Connection) ? Connection.CELL_3G : undefined;
	CPLUGIN.connection.CELL_4G = (!!Connection) ? Connection.CELL_4G : undefined;
	CPLUGIN.connection.CELL = (!!Connection) ? Connection.CELL : undefined;
	CPLUGIN.connection.NONE = (!!Connection) ? Connection.NONE : undefined;

	// Eventos online y offline
	document.addEventListener("online", CPLUGIN.connection._doWhenOnline, false);
	document.addEventListener("offline", CPLUGIN.connection._doWhenOffline, false);
});

// Devuelve el tipo de conexión actual
CPLUGIN.connection.getType = function () {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginNetworkInformation')) return false; // Salir si no esta disponible	

	return navigator.connection.type;
}

// Método privado a ejecutar cuando la conexión se establece
CPLUGIN.connection._doWhenOnline = function () {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginNetworkInformation')) return false; // Salir si no esta disponible	

	// Ejecutar código definido
	if (typeof CPLUGIN.connection.doWhenOnline !== 'undefined') {
		CPLUGIN.connection.doWhenOnline();	
	}
}

// Método privado a ejecutar cuando la conexión se pierde
CPLUGIN.connection._doWhenOffline = function () {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginNetworkInformation')) return false; // Salir si no esta disponible	

	// Ejecutar código defeinido
	if (typeof CPLUGIN.connection.doWhenOffline !== 'undefined') {
		CPLUGIN.connection.doWhenOffline();
	}
}

// Método ofrecido para sobrescribir con código propio
CPLUGIN.connection.doWhenOnline = function () { return true };

// Método ofrecido para sobrescribir con código propio
CPLUGIN.connection.doWhenOffline = function () { return true };

// Comprueba si la cámara esta presente en el dispositivo
CPLUGIN.connection.isWifiAvailable = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (available) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isWifiAvailable(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

// Comprueba si la cámara esta presente en el dispositivo
CPLUGIN.connection.isWifiEnabled = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (enabled) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});

	cordova.plugins.diagnostic.isWifiEnabled(
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

/*************************************************
 Lógica para conseguir información del dispositivo

 Android / iOS
 *************************************************/

CPLUGIN.device = {};

document.addEventListener("deviceready", function() {
	// Versión de Cordova que usa la aplicación
	CPLUGIN.device.cordova = (!!device) ? device.cordova : undefined;
	// Nombre del modelo o producto del dispositivo
	CPLUGIN.device.model = (!!device) ? device.model : undefined;
	// Plataforma (sistema operativo) del dispositivo
	CPLUGIN.device.platform = (!!device) ? device.platform : undefined;
	// Identificador único universal (uuid) del dispositivo
	CPLUGIN.device.uuid = (!!device) ? device.uuid : undefined;
	// Version de la plataforma (sistema operativo) del dispositivo
	CPLUGIN.device.version = (!!device) ? device.version : undefined;
	// Fabricante del dispositivo
	CPLUGIN.device.manufacturer = (!!device) ? device.manufacturer : undefined;
	// True si el dispositivo es un simulador
	CPLUGIN.device.isVirtual = (!!device) ? device.isVirtual : undefined;
	// Número de serie del dispositivo
	CPLUGIN.device.serial = (!!device) ? device.serial : undefined;
});

/********************************************************
 Lógica para el uso de la geolocalización del dispositivo

 Android / iOS
 ********************************************************/

CPLUGIN.geolocation = {};

// Conseguir la posición del dispositivo
CPLUGIN.geolocation.getCurrentPosition = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginGeolocation')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (position) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.enableHighAccuracy = (typeof oConfig.enableHighAccuracy !== 'undefined') ? oConfig.enableHighAccuracy : false; // Máxima precision, por defecto usa posición de red, con true usa posición satélite
	oConfig.timeout = (typeof oConfig.timeout !== 'undefined') ? oConfig.timeout : 10000; // Por defecto 10 segundos
	oConfig.cacheTime = (typeof oConfig.cacheTime !== 'undefined') ? oConfig.cacheTime : 30000; // Por defecto 30 segundos

	var oOptions = {};

	oOptions.enableHighAccuracy = oConfig.enableHighAccuracy;
	oOptions.timeout = oConfig.timeout;
	oOptions.maximumAge = oConfig.cacheTime;

	navigator.geolocation.getCurrentPosition(oConfig.successCallback, oConfig.errorCallback, oOptions);
}

// Comienza una escucha para conseguir la posición del dispositivo cuando un cambio en la posición sea detectado.
CPLUGIN.geolocation.watchPosition = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginGeolocation')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (position) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.endTimeCallback = (typeof oConfig.endTimeCallback !== 'undefined') ? oConfig.endTimeCallback : (function () {});
	oConfig.enableHighAccuracy = (typeof oConfig.enableHighAccuracy !== 'undefined') ? oConfig.enableHighAccuracy : false; // Máxima precision, por defecto usa posición de red, con true usa posición satélite
	oConfig.timeout = (typeof oConfig.timeout !== 'undefined') ? oConfig.timeout : 10000; // Por defecto 10 segundos
	oConfig.cacheTime = (typeof oConfig.cacheTime !== 'undefined') ? oConfig.cacheTime : 30000; // Por defecto 30 segundos
	oConfig.endTime = (typeof oConfig.endTime !== 'undefined') ? oConfig.endTime : 0; // Por defecto no definido

	var oOptions = {};

	oOptions.enableHighAccuracy = oConfig.enableHighAccuracy;
	oOptions.timeout = oConfig.timeout;
	oOptions.maximumAge = oConfig.cacheTime;

	// Devuelve una cadena con un identificador de referencia para la escucha creada, para poder cerrarlo cuando se necesite con CPLUGIN.geolocation.clearWatch.
	var watchID = navigator.geolocation.watchPosition(oConfig.successCallback, oConfig.errorCallback, oOptions);

	// Si existe un tiempo de finalización lanzamos el CPLUGIN.geolocation.clearWatch con un timeout con el valor de oConfig.endTime
	if (oConfig.endTime > 0) {

		setTimeout( function() { 

			// Detener la escucha
			CPLUGIN.geolocation.clearWatch(watchID);

			// Lanzar el callback de finalización de la escucha
			oConfig.endTimeCallback();

		}, oConfig.endTime);
	}

	return watchID;
}

// Finalizar la escucha de cambio de posición del dispositivo cuando un cambio en la posición se detecta.
CPLUGIN.geolocation.clearWatch = function (watchID) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginGeolocation')) return false; // Salir si no esta disponible

	/*
	Valores obligatorios
	*/
	// Si no tenemos al el identificador de una escucha url no puede funcionar
	if (typeof watchID == 'undefined') return false;

	navigator.geolocation.clearWatch(watchID);
}

// Método privado que registra una función a ejecutar cuando un cambio en el estado de la localización ocurre
CPLUGIN.geolocation._registerLocationStateChangeHandler = function () {

	// Evento deviceready
	document.addEventListener("deviceready", function() {

		// No hace falta comprobar el plugin degeolocalización ya que aquí solo se comprueba la tecnología no el uso de la misma por el plugin de geolocalización
		if (!CPLUGIN.core._checkPlugin('hasCordovaPluginDiagnostic')) return false; // Salir si no esta disponible

		// Ejecutar la función privada ante el evento
		cordova.plugins.diagnostic.registerLocationStateChangeHandler(CPLUGIN.geolocation._doOnLocationStateChange);
	});
}
CPLUGIN.geolocation._registerLocationStateChangeHandler(); // Lanzar el evento

// Método privado a ejecutar cuando el estado de la funcionalidad de localización cambia
CPLUGIN.geolocation._doOnLocationStateChange = function (state) {

	// Ejecutar el código de la App ante el evento
	CPLUGIN.geolocation.doOnLocationStateChange(state)
}

// Método para ser sobrescrito con el codigo de la App ante el evento de un cambio en el estado de la funcionalidad de localización
CPLUGIN.geolocation.doOnLocationStateChange = function (state) {};

/*************************************
 API: https://www.w3.org/TR/vibration/

 Android / iOS
 *************************************/

CPLUGIN.vibration = {};

// Método para hacer vibrar el dispositivo
CPLUGIN.vibration.vibrate = function (pattern) {

	if (!CPLUGIN.core._checkPlugin('hasApiVibration')) return false; // Salir si no esta disponible

	var patternArray = [];

	if (typeof pattern === 'number') {
		patternArray =  [pattern];
	} else {
		patternArray = pattern;
	}

	return navigator.vibrate(patternArray);
}

// Método para hacer vibrar el dispositivo de forma prolongada
CPLUGIN.vibration.vibrateLong = function () {

	return CPLUGIN.vibration.vibrate(1100);
}

// Método para hacer vibrar el dispositivo de forma corta
CPLUGIN.vibration.vibrateShort = function () {

	return CPLUGIN.vibration.vibrate(200);
}

// Método para hacer vibrar el dispositivo con repeticiones cortas
CPLUGIN.vibration.vibrateAlert = function () {

	return CPLUGIN.vibration.vibrate([200,50,200,50,200]);
}

// Método para hacer vibrar el dispositivo con repeticiones largas, alarma
CPLUGIN.vibration.vibrateAlarm = function () {

	return CPLUGIN.vibration.vibrate([500, 250, 500, 250, 500, 250, 500, 250, 500]);
}

// Método para detener las vibraciones que esten sucediendo
CPLUGIN.vibration.vibrateStop = function () {

	return CPLUGIN.vibration.vibrate(0);
}

/******************************************
 API: https://www.w3.org/TR/battery-status/

 Android / iOS
 ******************************************/

CPLUGIN.battery = {};

CPLUGIN.battery.charging = undefined;
CPLUGIN.battery.chargingTime = undefined;
CPLUGIN.battery.dischargingTime = undefined;
CPLUGIN.battery.level = undefined;

// Método privado que centraliza la actualización de las variables con datos de la batería
CPLUGIN.battery._updateStatus = function (battery) {

	CPLUGIN.battery.charging = battery.charging;
	CPLUGIN.battery.chargingTime = battery.chargingTime;
	CPLUGIN.battery.dischargingTime = battery.dischargingTime;
	CPLUGIN.battery.level = battery.level;
}

// Método privado para conseguir el objeto gestor de la batería en CPLUGIN.battery._batteryManager
CPLUGIN.battery._getBatteryManager = function () {

	// Evento deviceready
	document.addEventListener("deviceready", function() {

		if (!CPLUGIN.core._checkPlugin('hasApiBatteryStatus')) return false; // Salir si no esta disponible

		// Promesa con la que conseguir el gestor de la batería
		navigator.getBattery().then(function(battery) {

			// Ejecutar la función privada con el código para el evento por primera vez
			CPLUGIN.battery._updateStatus(battery);

			// Evento ante un cambio en el estado de cargando
			battery.onchargingchange = function (event) {

				// Ejecutar la función privada con el código para el evento
				CPLUGIN.battery._doOnChargingChange(battery, event);
	        };

	        // Evento ante un cambio en el tiempo de cargado
	        battery.onchargingtimechange = function (event) {

	        	// Ejecutar la función privada con el código para el evento
	        	CPLUGIN.battery._doOnChargingTimeChange(battery, event);
	        };

	        // Evento ante un cambio en el tiempo de descargado
	        battery.ondischargingtimechange = function (event) {

	        	// Ejecutar la función privada con el código para el evento
	        	CPLUGIN.battery._doOnDischargingTimeChange(battery, event);
	        };

	        // Evento ante un cambio en el nivel de la batería
	        battery.onlevelchange = function (event) {

	        	// Ejecutar la función privada con el código para el evento
	        	CPLUGIN.battery._doOnLevelChange(battery, event);
	        };
		});
	});
}
CPLUGIN.battery._getBatteryManager(); // Cargar el gestor de la batería en CPLUGIN.battery.batteryManager

// Metodo privado a ejecutar ante un cambio en el estado de cargando
CPLUGIN.battery._doOnChargingChange = function (battery, event) {

	// Actualizar las variables con datos de la batería
	CPLUGIN.battery._updateStatus(battery);

	// Ejecutar el código de la App ante el evento
	CPLUGIN.battery.doOnChargingChange(event); 
}

// Metodo privado a ejecutar ante un cambio en el tiempo de cargado
CPLUGIN.battery._doOnChargingTimeChange = function (battery, event) {

	// Actualizar las variables con datos de la batería
	CPLUGIN.battery._updateStatus(battery);

	// Ejecutar el código de la App ante el evento
	CPLUGIN.battery.doOnChargingTimeChange(event); 
}

// Metodo privado a ejecutar ante un cambio en el tiempo de descargado
CPLUGIN.battery._doOnDischargingTimeChange = function (battery, event) {

	// Actualizar las variables con datos de la batería
	CPLUGIN.battery._updateStatus(battery);

	// Ejecutar el código de la App ante el evento
	CPLUGIN.battery.doOnDischargingTimeChange(event); 
}

// Metodo privado a ejecutar ante un cambio en el nivel de la batería
CPLUGIN.battery._doOnLevelChange = function (battery, event) {

	// Actualizar las variables con datos de la batería
	CPLUGIN.battery._updateStatus(battery);

	// Ejecutar el código de la App ante el evento
	CPLUGIN.battery.doOnLevelChange(event); 
}

// Método para ser sobrescrito con el codigo de la App ante el evento de un cambio en el estado de cargando
CPLUGIN.battery.doOnChargingChange = function () {};
// Método para ser sobrescrito con el codigo de la App ante el evento de un cambio en el tiempo de cargado
CPLUGIN.battery.doOnChargingTimeChange = function () {};
// Método para ser sobrescrito con el codigo de la App ante el evento de un cambio en el tiempo de descargado
CPLUGIN.battery.doOnDischargingTimeChange = function () {};
// Método para ser sobrescrito con el codigo de la App ante el evento de un cambio en el nivel de la batería
CPLUGIN.battery.doOnLevelChange = function () {};

// Método para comprobar si estamos en un estado crítico. La batería igual o por debajo del 25% y sin estar cargando el dispositivo
CPLUGIN.battery.isBatteryWarningStatus = function () {

	if (!CPLUGIN.core._checkPlugin('hasApiBatteryStatus')) return false; // Salir si no esta disponible

	return ((!CPLUGIN.battery.charging && CPLUGIN.battery.level <= 0.25) ? true : false);
}

// Método para comprobar si estamos en un estado crítico. La batería igual o por debajo del 10% y sin estar cargando el dispositivo
CPLUGIN.battery.isBatteryCriticalStatus = function () {

	if (!CPLUGIN.core._checkPlugin('hasApiBatteryStatus')) return false; // Salir si no esta disponible

	return ((!CPLUGIN.battery.charging && CPLUGIN.battery.level <= 0.10) ? true : false);
}

/***************************************************************
 PLUGIN: https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin

 Android / iOS
 ***************************************************************/

CPLUGIN.toast = {};

// Mostrar un Toast
CPLUGIN.toast.show = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginXToast')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (args) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (error) {});
	oConfig.message = (typeof oConfig.message !== 'undefined') ? oConfig.message : '';
	oConfig.duration = (typeof oConfig.duration !== 'undefined') ? oConfig.duration : 'short'; // Duración del Toast en milisegundos, también acepta el valor 'short' o 'long'
	oConfig.position = (typeof oConfig.position !== 'undefined') ? oConfig.position : 'bottom'; // Posición del Toast, acepta los valores 'top', 'center', 'bottom'
	oConfig.addPixelsY = (typeof oConfig.addPixelsY !== 'undefined') ? oConfig.addPixelsY : 0; // Mover el Toast un valor de píxeles en el eje Y respecto a la posición elegida, usar un valor negativo para subir el Toast hacia arriba
	oConfig.styling = (typeof oConfig.styling !== 'undefined') ? oConfig.styling : {}; // Objeto con propiedades CSS para dar un estilo personalizado al Toast

	var oOptions = {};

	oOptions.message = oConfig.message;
	oOptions.duration = oConfig.duration;
	oOptions.position = oConfig.position;
	oOptions.addPixelsY = oConfig.addPixelsY;
	oOptions.styling = oConfig.styling;

	window.plugins.toast.showWithOptions(
		oOptions,
		oConfig.successCallback, 
		oConfig.errorCallback
	);
}

// Ocultar un Toast existente, ejecutará el callback de success del Toast si ha sido definido
CPLUGIN.toast.hide = function () {

	if (!CPLUGIN.core._checkPlugin('hasCordovaPluginXToast')) return false; // Salir si no esta disponible	

	window.plugins.toast.hide();
}

/********************************************************
 PLUGIN: https://github.com/phonegap/phonegap-plugin-push

 Android / iOS
 ********************************************************/

CPLUGIN.push = {};

// Inicializa el plugin de forma nativa
CPLUGIN.push.init = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	https://github.com/phonegap/phonegap-plugin-push/blob/master/docs/API.md#pushnotificationinitoptions
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};

	// Plataformas
	oConfig.android = (typeof oConfig.android !== 'undefined') ? oConfig.android : {};
	oConfig.ios = (typeof oConfig.ios !== 'undefined') ? oConfig.ios : {};
	oConfig.browser = (typeof oConfig.browser !== 'undefined') ? oConfig.browser : {};

	// Plataforma Android
	if (typeof oConfig.android.sound !== 'undefined') oConfig.android.sound = oConfig.android.sound;
	if (typeof oConfig.android.vibrate !== 'undefined') oConfig.android.vibrate = oConfig.android.vibrate;
	if (typeof oConfig.android.clearNotifications !== 'undefined') oConfig.android.clearNotifications = oConfig.android.clearNotifications;
	if (typeof oConfig.android.forceShow !== 'undefined') oConfig.android.forceShow = oConfig.android.forceShow;
	if (typeof oConfig.android.icon !== 'undefined') oConfig.android.icon = oConfig.android.icon;
	if (typeof oConfig.android.iconColor !== 'undefined') oConfig.android.iconColor = oConfig.android.iconColor;
	if (typeof oConfig.android.clearBadge !== 'undefined') oConfig.android.clearBadge = oConfig.android.clearBadge;
	if (typeof oConfig.android.topics !== 'undefined') oConfig.android.topics = oConfig.android.topics;
	if (typeof oConfig.android.messageKey !== 'undefined') oConfig.android.messageKey = oConfig.android.messageKey;
	if (typeof oConfig.android.titleKey !== 'undefined') oConfig.android.titleKey = oConfig.android.titleKey;

	// Plataforma iOS
	if (typeof oConfig.ios.alert !== 'undefined') { oConfig.ios.alert = oConfig.ios.alert } else { oConfig.ios.alert = true };
	if (typeof oConfig.ios.badge !== 'undefined') { oConfig.ios.badge = oConfig.ios.badge } else { oConfig.ios.badge = true };
	if (typeof oConfig.ios.sound !== 'undefined') { oConfig.ios.sound = oConfig.ios.sound } else { oConfig.ios.sound = true };
	if (typeof oConfig.ios.voip !== 'undefined') oConfig.ios.voip = oConfig.ios.voip;
	if (typeof oConfig.ios.clearBadge !== 'undefined') oConfig.ios.clearBadge = oConfig.ios.clearBadge;
	if (typeof oConfig.ios.categories !== 'undefined') oConfig.ios.categories = oConfig.ios.categories;

	// Plataforma Browser
	oConfig.browser.pushServiceURL = 'http://push.api.phonegap.com/v1/push';

	const push = PushNotification.init({
		android: oConfig.android,
	    browser: oConfig.browser,
		ios: oConfig.ios,
		windows: {}
	});

	push.on('registration', function (data) {
		CPLUGIN.push._doOnRegistration(data);
	});

	push.on('notification', function (data) {
		CPLUGIN.push._doOnNotification(data);
	});

	push.on('error', function (error) {
		CPLUGIN.push._doOnError(error);
	});

}

/*
Funciones pensadas para ser sobreescritas
*/

// Función privada con la lógica a ejecutar al registrar el dispositivo
CPLUGIN.push._doOnRegistration = function (data) {
	//console.log(data);
}

// Función privada con la lógica a ejecutar al recibir una notificación
CPLUGIN.push._doOnNotification = function (data) {
	//console.log(data);
}

// Función privada con la lógica a ejecutar en caso de error al iniciar el plugin
CPLUGIN.push._doOnError = function (error) {
	//console.log(error);
}

/*********************************************************************
 Lógica para el uso de la API de notificaciones Push de Madrid Digital

 Android / iOS
 *********************************************************************/

CPLUGIN.pushMD = {};

// Objeto de error
CPLUGIN.pushMD.oError = {};

// Constantes
CPLUGIN.pushMD.DESARROLLO = 0;
CPLUGIN.pushMD.VALIDACION = 1;
CPLUGIN.pushMD.PRODUCCION = 2;
CPLUGIN.pushMD.DESARROLLO_URL_BASE = 'https://desarrollo.madrid.org/mova_rest_notificaciones/';
CPLUGIN.pushMD.VALIDACION_URL_BASE = 'https://valintranet.madrid.org/mova_rest_notificaciones/';
CPLUGIN.pushMD.PRODUCCION_URL_BASE = 'https://gestiona3.madrid.org/mova_rest_notificaciones/';
CPLUGIN.pushMD.NATIVE_TOKEN;
CPLUGIN.pushMD.DEVICE_ID;

// Códigos y textos de error
CPLUGIN.pushMD.ERROR_CODE_NO_DEVICE_ID = 1;
CPLUGIN.pushMD.ERROR_TEXT_NO_DEVICE_ID = 'No existe ID de dispositivo';
CPLUGIN.pushMD.ERROR_CODE_RETRIES_SPENT = 2;
CPLUGIN.pushMD.ERROR_TEXT_RETRIES_SPENT = 'Reintentos de registro agotados';
CPLUGIN.pushMD.ERROR_CODE_SET_DEVICE = 100;
CPLUGIN.pushMD.ERROR_TEXT_SET_DEVICE = 'Error CPLUGIN.pushMD.setDevice';
CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE = 101;
CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE = 'Error CPLUGIN.pushMD.getDevice';
CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_DELIVERIES = 102;
CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_DELIVERIES = 'Error CPLUGIN.pushMD.getDeviceDeliveries';
CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_USERS = 103;
CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_USERS = 'Error CPLUGIN.pushMD.getDeviceUsers';
CPLUGIN.pushMD.ERROR_CODE_SEND_DEVICE_PUSH = 104;
CPLUGIN.pushMD.ERROR_TEXT_SEND_DEVICE_PUSH = 'Error CPLUGIN.pushMD.sendDevicePush';
CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_ID = 105;
CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_ID = 'Error CPLUGIN.pushMD.getDeviceId';
CPLUGIN.pushMD.ERROR_CODE_ADD_DEVICE_USER = 106;
CPLUGIN.pushMD.ERROR_TEXT_ADD_DEVICE_USER = 'Error CPLUGIN.pushMD.addDeviceUser';
CPLUGIN.pushMD.ERROR_CODE_REMOVE_DEVICE_USER = 107;
CPLUGIN.pushMD.ERROR_TEXT_REMOVE_DEVICE_USER = 'Error CPLUGIN.pushMD.removeDeviceUser';
CPLUGIN.pushMD.ERROR_CODE_REMOVE_DEVICE_ALL_USERS = 108;
CPLUGIN.pushMD.ERROR_TEXT_REMOVE_DEVICE_ALL_USERS = 'Error CPLUGIN.pushMD.removeDeviceAllUsers';
CPLUGIN.pushMD.ERROR_CODE_SEND_DEVICE_USER_PUSH = 109;
CPLUGIN.pushMD.ERROR_TEXT_SEND_DEVICE_USER_PUSH = 'Error CPLUGIN.pushMD.sendDeviceUserPush';
CPLUGIN.pushMD.ERROR_CODE_ADD_DEVICE_TOPIC = 110;
CPLUGIN.pushMD.ERROR_TEXT_ADD_DEVICE_TOPIC = 'Error CPLUGIN.pushMD.addDeviceTopic';
CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_TOPIC = 111;
CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_TOPIC = 'Error CPLUGIN.pushMD.getDeviceTopic';
CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_ALL_TOPICS = 112;
CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_ALL_TOPICS = 'Error CPLUGIN.pushMD.getDeviceAllTopics';
CPLUGIN.pushMD.ERROR_CODE_REMOVE_DEVICE_TOPIC = 113;
CPLUGIN.pushMD.ERROR_TEXT_REMOVE_DEVICE_TOPIC = 'Error CPLUGIN.pushMD.removeDeviceTopic';
CPLUGIN.pushMD.ERROR_CODE_SEND_DEVICE_TOPIC_PUSH = 114;
CPLUGIN.pushMD.ERROR_TEXT_SEND_DEVICE_TOPIC_PUSH = 'Error CPLUGIN.pushMD.sendDeviceTopicPush';


// Configuración de la API de Madrid Digital, debe sobreescribirse con los datos propios de cada App
CPLUGIN.pushMD.application = '';
CPLUGIN.pushMD.client = '';
CPLUGIN.pushMD.desarrolloPassword = '';
CPLUGIN.pushMD.validacionPassword = '';
CPLUGIN.pushMD.produccionPassword = '';
CPLUGIN.pushMD.version = 'v4'; // valor por defecto si no se sobreescribe
CPLUGIN.pushMD.retries = 5; // Número de reintentos

// Variables privadas
CPLUGIN.pushMD._retriesCount = -1;
CPLUGIN.pushMD._retryConfig = {}; // Objeto con la configuración del reintento
CPLUGIN.pushMD._oError = {}; // Objeto de error para devolver en los callbacks

// Realiza un reintento mientras el conteo de reintentos sea mayor que 0
CPLUGIN.pushMD._doRetry = function () {

	// Inicializar o descontar reintentos
	CPLUGIN.pushMD._retriesCount = (CPLUGIN.pushMD._retriesCount === -1) ? CPLUGIN.pushMD.retries : CPLUGIN.pushMD._retriesCount - 1;

	// Comprobar si se ha llegado al último reintento
	if (CPLUGIN.pushMD._retriesCount === 0) {

		var errorMessage = 'Se han agotado todos los reintentos posibles para "CPLUGIN.pushMD.init".\n Aunque pueden no ser válidos, los datos de registro actuales son:';

		var errorNativeToken = (CPLUGIN.pushMD.NATIVE_TOKEN) ? CPLUGIN.pushMD.NATIVE_TOKEN : 'Ninguno';
		var errorDeviceId = (CPLUGIN.pushMD.DEVICE_ID) ? CPLUGIN.pushMD.DEVICE_ID : 'Ninguno';

		errorMessage += '\n - Token nativo: ' + errorNativeToken;
		errorMessage += '\n - ID de dispositivo: ' + errorDeviceId;

		// Mostrar mensaje de error
		console.error(errorMessage);

		// Reinicializar el valor de la variable de recuento de reintentos
		CPLUGIN.pushMD._retriesCount = -1;

	} else {

		var warningMessage = 'Reintento agotado. ( ' + CPLUGIN.pushMD._retriesCount  + ' / ' + CPLUGIN.pushMD.retries + ' )';

		// Mostrar mensaje de aviso
		console.warn(warningMessage);

		// Hacer un reintento
		CPLUGIN.pushMD.init(CPLUGIN.pushMD._retryConfig);
	}

	return CPLUGIN.pushMD._retriesCount;
}

// Consigue los datos del entorno necesarios para la API Push de Madrid Digital en un objeto.
CPLUGIN.pushMD._apiEnvironmentObject = function (oConfig) {

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;

	var oData = {};
	switch(oConfig.environment) {
		case CPLUGIN.pushMD.DESARROLLO:
			oData.urlBase = CPLUGIN.pushMD.DESARROLLO_URL_BASE + CPLUGIN.pushMD.version + '/';
			oData.apiEnv = 'DES';
			oData.apiPass = CPLUGIN.pushMD.desarrolloPassword;
			break;
		case CPLUGIN.pushMD.VALIDACION:
			oData.urlBase = CPLUGIN.pushMD.VALIDACION_URL_BASE + CPLUGIN.pushMD.version + '/';
			oData.apiEnv = 'VAL';
			oData.apiPass = CPLUGIN.pushMD.validacionPassword;
			break;
		case CPLUGIN.pushMD.PRODUCCION:
			oData.urlBase = CPLUGIN.pushMD.PRODUCCION_URL_BASE + CPLUGIN.pushMD.version + '/';
			oData.apiEnv = 'PRO';
			oData.apiPass = CPLUGIN.pushMD.produccionPassword;
			break;
	}

	return oData;
}

// Registra un dispositivo en la API Push de Madrid Digital
CPLUGIN.pushMD.registerDevice = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.registrationId = (typeof oConfig.registrationId !== 'undefined') ? oConfig.registrationId : '';
	oConfig.deviceInfo = (typeof oConfig.deviceInfo !== 'undefined') ? oConfig.deviceInfo : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/registra';
	var url = urlBase + urlRest;

	var environment = oApiEnvironment.apiEnv;
	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.tokenDispositivo = oConfig.registrationId;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.plataforma = device.platform;
	oData.entorno = environment;
	oData.dsInformacionMovil = oConfig.deviceInfo;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Incluimos en la respuesta el token nativo como información adicional
	        data.registrationId = oConfig.registrationId;

			// Guardar el identificador del dispositivo de la API de Madrid Digital en la variable CPLUGIN.pushMD.DEVICE_ID
			CPLUGIN.pushMD.DEVICE_ID = data.idDispositivo;

			/*******************************
			 * Comprobar ID de dispositivo *
			 *******************************/

			// Si no tenemos identificador del dispositivo de la API de Madrid Digital realizamos un reintento si es posible.
			if (!CPLUGIN.pushMD.DEVICE_ID) {

				// Realizar un reintento y si se agotan los reintentos disponibles lanzar el callback de error
				if (CPLUGIN.pushMD._doRetry() === -1) {

			    	// Configurar objeto de error
			    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_NO_DEVICE_ID;
			    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_CODE_NO_DEVICE_TEXT;

					//Llamar al callback de error y pasar el objeto de errir
			    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
				}
			}

			/****************************************************************
			 * Registro en la API de Madrid Digital realizado correctamente *
			 ****************************************************************/

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	        
	    } else {

			// Realizar un reintento y si se agotan los reintentos disponibles lanzar el callback de error
			if (CPLUGIN.pushMD._doRetry() === -1) {

		    	// Configurar objeto de error
		    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_RETRIES_SPENT;
		    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_RETRIES_SPENT;

				//Llamar al callback de error y pasar el objeto de errir
		    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
			}
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Configura la aplicación para que pueda usar las notificaciones Push de Madrid Digital.
CPLUGIN.pushMD.init = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	// Guardar la configuración del reintento por si hiciese falta
	if (CPLUGIN.pushMD._retriesCount === -1) CPLUGIN.pushMD._retryConfig = oConfig;

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.registrationCallback = (typeof oConfig.registrationCallback !== 'undefined') ? oConfig.registrationCallback : (function (data) {});
	oConfig.registrationNativeCallback = (typeof oConfig.registrationNativeCallback !== 'undefined') ? oConfig.registrationNativeCallback : (function (data) {});
	oConfig.getNotificationCallback = (typeof oConfig.getNotificationCallback !== 'undefined') ? oConfig.getNotificationCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.pluginInitConfig = (typeof oConfig.pluginInitConfig !== 'undefined') ? oConfig.pluginInitConfig : {};
	oConfig.deviceInfo = (typeof oConfig.deviceInfo !== 'undefined') ? oConfig.deviceInfo : '';
	
	// Sobrescribe el método del plugin para realizar las acciones necesarias de la API Push de Madrid Digital
	CPLUGIN.push._doOnRegistration = function (data) {
		
		// Configuración para registrar el dispositivo en la API Push de Madrid Digital
		var oRegisterConfig = {};
		oRegisterConfig.successCallback = oConfig.registrationCallback; // Función a ejecutar en caso de registro correcto
		oRegisterConfig.errorCallback = oConfig.errorCallback;
		oRegisterConfig.registrationId = data.registrationId;
		oRegisterConfig.environment = oConfig.environment;
		oRegisterConfig.deviceInfo = oConfig.deviceInfo;

		// Guardar el token de registro en la variable CPLUGIN.pushMD.NATIVE_TOKEN
		CPLUGIN.pushMD.NATIVE_TOKEN = data.registrationId;

		// Llamar al callback de success del registro nativo correcto
		oConfig.registrationNativeCallback(data);

		// Registrar el dispositivo con la API de Madrid Digital
		CPLUGIN.pushMD.registerDevice(oRegisterConfig);
	}

	// Sobrescribe el método del plugin para realizar acciones al recibir una notificación
	CPLUGIN.push._doOnNotification = function (data) {

		// Al recibir una notificación ejecutar el método de callback
		oConfig.getNotificationCallback(data);
	}

	CPLUGIN.push._doOnError = function (error) {

		//Llamar al callback de error y pasar el objeto xhr
    	oConfig.errorCallback(error);
	}

    // Iniciar el plugin de notificaciones Push
    CPLUGIN.push.init(oConfig.pluginInitConfig);
}

// Cambia las propiedades de un dispositivo concreto
CPLUGIN.pushMD.setDevice = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';
	oConfig.deviceNativeToken = (typeof oConfig.deviceNativeToken !== 'undefined') ? oConfig.deviceNativeToken : '';
	oConfig.deviceAllowNotifications = (typeof oConfig.deviceAllowNotifications !== 'undefined') ? oConfig.deviceAllowNotifications : 1; // Por defecto se activan las notificaciones

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/set';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.tokenDispositivo = oConfig.deviceNativeToken;
	oData.itNotifica = oConfig.deviceAllowNotifications;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);

	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_SET_DEVICE;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_SET_DEVICE;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Devuelve la información de un dispositivo concreto
CPLUGIN.pushMD.getDevice = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/get';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Devuelve los topics a los que esta suscrito un dispositivo concreto
CPLUGIN.pushMD.getDeviceDeliveries = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';
	oConfig.limit = (typeof oConfig.limit !== 'undefined') ? oConfig.limit : 5; // Por defecto solo los 5 últimos envíos
	oConfig.dateLimit = (typeof oConfig.dateLimit !== 'undefined') ? oConfig.dateLimit : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/envios';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.limit = oConfig.limit;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;
	oData.fechaDesde = oConfig.dateLimit;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_DELIVERIES;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_DELIVERIES;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Devuelve los usuarios asociados a un dispositivo concreto
CPLUGIN.pushMD.getDeviceUsers = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/usuarios';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_USERS;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_USERS;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Envia un Push a un dispositivo
CPLUGIN.pushMD.sendDevicePush = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';
	oConfig.message = (typeof oConfig.message !== 'undefined') ? oConfig.message : '';
	oConfig.title = (typeof oConfig.title !== 'undefined') ? oConfig.title : '';
	oConfig.info = (typeof oConfig.info !== 'undefined') ? oConfig.info : {};

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/mensaje';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.mensaje = oConfig.message;
	oData.titulo = oConfig.title;
	oData.infoApp = oConfig.info;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_SEND_DEVICE_PUSH;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_SEND_DEVICE_PUSH;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Devuelve el identificador del dispositivo para la API de Madrid Digital mediante el token nativo de la plataforma
CPLUGIN.pushMD.getDeviceId = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceNativeToken = (typeof oConfig.deviceNativeToken !== 'undefined') ? oConfig.deviceNativeToken : '';
	oConfig.platform = (typeof oConfig.platform !== 'undefined') ? oConfig.platform : '';
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/confirmaId';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.tokenDispositivo = oConfig.deviceNativeToken;
	oData.plataforma = oConfig.environment;
	oData.entorno = oConfig.platform;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_ID;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_ID;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Asocia un usuario al dispositivo
CPLUGIN.pushMD.addDeviceUser = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';
	oConfig.userId = (typeof oConfig.userId !== 'undefined') ? oConfig.userId : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/usuarios/add';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.idUsuario = oConfig.userId;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_ADD_DEVICE_USER;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_ADD_DEVICE_USER;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}


// Elimina un usuario al dispositivo
CPLUGIN.pushMD.removeDeviceUser = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';
	oConfig.userId = (typeof oConfig.userId !== 'undefined') ? oConfig.userId : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/usuarios/delete';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.idUsuario = oConfig.userId;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_REMOVE_DEVICE_USER;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_REMOVE_DEVICE_USER;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Elimina tpdos los usuarios del dispositivo
CPLUGIN.pushMD.removeDeviceAllUsers = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/usuarios/deleteAll';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_REMOVE_DEVICE_ALL_USERS;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_REMOVE_DEVICE_ALL_USERS;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Envia un Push a un usuario
CPLUGIN.pushMD.sendDeviceUserPush = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.userId = (typeof oConfig.userId !== 'undefined') ? oConfig.userId : '';
	oConfig.message = (typeof oConfig.message !== 'undefined') ? oConfig.message : '';
	oConfig.title = (typeof oConfig.title !== 'undefined') ? oConfig.title : '';
	oConfig.info = (typeof oConfig.info !== 'undefined') ? oConfig.info : {};

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/usuarios/mensaje';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idUsuario = oConfig.userId;
	oData.mensaje = oConfig.message;
	oData.titulo = oConfig.title;
	oData.infoApp = oConfig.info;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_SEND_DEVICE_USER_PUSH;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_SEND_DEVICE_USER_PUSH;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Suscribe un dispositivo a un topic
CPLUGIN.pushMD.addDeviceTopic = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';
	oConfig.topicName = (typeof oConfig.topicName !== 'undefined') ? oConfig.topicName : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'topics/subscripcion/subscribe';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.nombreTopic = oConfig.topicName;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_ADD_DEVICE_TOPIC;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_ADD_DEVICE_TOPIC;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Devuelve los topics de un dispositivo
CPLUGIN.pushMD.getDeviceTopic = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';
	oConfig.topicName = (typeof oConfig.topicName !== 'undefined') ? oConfig.topicName : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'topics/subscripcion/subscrito';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.nombreTopic = oConfig.topicName;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_TOPIC;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_TOPIC;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Devuelve los topics a los que esta suscrito un dispositivo concreto
CPLUGIN.pushMD.getDeviceAllTopics = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'dispositivos/topics';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_GET_DEVICE_ALL_TOPICS;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_GET_DEVICE_ALL_TOPICS;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Elimina la suscripción de un dispositivo a un topic
CPLUGIN.pushMD.removeDeviceTopic = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.deviceId = (typeof oConfig.deviceId !== 'undefined') ? oConfig.deviceId : '';
	oConfig.topicName = (typeof oConfig.topicName !== 'undefined') ? oConfig.topicName : '';

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'topics/subscripcion/elimina';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.idDispositivo = oConfig.deviceId;
	oData.nombreTopic = oConfig.topicName;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_REMOVE_DEVICE_TOPIC;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_REMOVE_DEVICE_TOPIC;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}

// Envia un Push a un usuario
CPLUGIN.pushMD.sendDeviceTopicPush = function (oConfig) {

	if (!CPLUGIN.core._checkPlugin('hasPhonegapPluginPush')) return false; // Salir si no esta disponible

	/*
	Valores por defecto
	*/
	oConfig = (typeof oConfig !== 'undefined') ? oConfig : {};
	oConfig.environment = (typeof oConfig.environment !== 'undefined') ? oConfig.environment : CPLUGIN.pushMD._desarrollo;
	oConfig.successCallback = (typeof oConfig.successCallback !== 'undefined') ? oConfig.successCallback : (function (data) {});
	oConfig.errorCallback = (typeof oConfig.errorCallback !== 'undefined') ? oConfig.errorCallback : (function (xhr) {});
	oConfig.topicName = (typeof oConfig.topicName !== 'undefined') ? oConfig.topicName : '';
	oConfig.message = (typeof oConfig.message !== 'undefined') ? oConfig.message : '';
	oConfig.title = (typeof oConfig.title !== 'undefined') ? oConfig.title : '';
	oConfig.info = (typeof oConfig.info !== 'undefined') ? oConfig.info : {};

	// Conseguir la url dependiendo de la configuración
	var oApiEnvironment = CPLUGIN.pushMD._apiEnvironmentObject(oConfig);

	var urlBase = oApiEnvironment.urlBase;
	var urlRest = 'topics/mensaje';
	var url = urlBase + urlRest;

	var password = oApiEnvironment.apiPass; 

	// Objeto con la información a pasar mediante la llamada http
	var oData = {};
	oData.nombreTopic = oConfig.topicName;
	oData.mensaje = oConfig.message;
	oData.titulo = oConfig.title;
	oData.infoApp = oConfig.info;
	oData.nAplicacion = CPLUGIN.pushMD.application;
	oData.password = password;
	oData.dsCliente = CPLUGIN.pushMD.client;

	// Realizar la llamada http
	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
	    if (this.readyState != 4) return;

	    if (this.status == 200) {

	        var data = JSON.parse(this.responseText);

	        // Llamar al callback de success
	        oConfig.successCallback(data);
	    } else {

	    	// Configurar objeto de error
	    	CPLUGIN.pushMD.oError.code = CPLUGIN.pushMD.ERROR_CODE_SEND_DEVICE_TOPIC_PUSH;
	    	CPLUGIN.pushMD.oError.text = CPLUGIN.pushMD.ERROR_TEXT_SEND_DEVICE_TOPIC_PUSH;
	    	CPLUGIN.pushMD.oError.xhr = xhr;

			//Llamar al callback de error y pasar el objeto de errir
	    	oConfig.errorCallback(CPLUGIN.pushMD.oError);
	    }
	};

	xhr.open('POST', url, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(oData));
}