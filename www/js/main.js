(function() {

  // Ejecutar cuando el DOM sea creado y el dispositivo disponible
  document.addEventListener("deviceready", function() { 

  	// Ocultar la barra de estado
  	CPLUGIN.statusBar.setConfig({"hide":true}); 


  }, false);

})();