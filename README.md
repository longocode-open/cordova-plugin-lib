# Ejemplo de uso de cordova-plugin-lib

Ejemplo sencillo del uso de la librería cordova-plugin-lib preparado para ser compilado con Cordova o Phonegap.

### ¿Donde se encuentra la librería?

Esta se encuentra en la ruta www/js/ con el nombre _cordova-plugin-lib-x.x.x.js_.

### ¿Cómo funciona la librería?

Al incluir la librería disponemos de un objeto global llamado CPLUGIN que contiene toda la funcionalidad disponible.

> El propio proyecto es un ejemplo en si mismo.
> Para ver el uso de los métodos y aclarar dudas puedes ver ejemplos de uso en el directorio **www/examples/** que se encuentra en la ruta **www/**
